!!! Time-stamp: <2015-07-16 10:32:16 pbrowne>

!program to run the particle filter on the model HadCM3.
!this shall hopefully have minimal changes specific to the model.
!Simon Wilson and Philip Browne 2013
!----------------------------------------------------------------

program couple_pf
  use comms
  use pf_control
  use sizes
  implicit none
  include 'mpif.h'
  integer :: i,j,k
  integer :: mpi_err,particle,tag
  INTEGER, DIMENSION(:), ALLOCATABLE  :: requests
  INTEGER, DIMENSION(:,:), ALLOCATABLE  :: mpi_statuses
  logical :: mpi_flag
  logical, dimension(:), ALLOCATABLE :: received
  integer :: mpi_status(MPI_STATUS_SIZE)
  real(kind=kind(1.0d0)) :: start_t,end_t
  character(14) :: filename



  write(6,'(A)') 'PF: Starting PF code'
  call flush(6)
  call initialise_mpi
  print*,'PF: setting controls'
  call set_pf_controls
  print*,'PF: configuring model'
  call configure_model
  print*,'allocating pf'
  call allocate_pf
!  print*,'stopping' !for testing
!  stop              !for testing
  call random_seed_mpi(pfrank)
  write(6,*) 'PF: starting to receive from model'
! 1st call to model to get psi
!  do k =1,pf%count
!     call receive_from_model(pf%psi(:,k),pf%particles(k))

     !lets add some random noise to the initial conditions
!     call perturb_particle(pf%psi(:,k))

!  enddo
!  print*,'launching the receives'
 ! print*,pf%count
 ! print*,pf%particles
 ! print*,allocated(requests)
  tag = 1        
  allocate(requests(pf%count))
  allocate(mpi_statuses(mpi_status_size,pf%count))
  allocate(received(pf%count))

  !HADCM3 MODEL SPECIFIC...
  !let us spin the model up for one day, or 72 timesteps
  start_t = mpi_wtime() 
  do j = 1,72
     do k = 1,pf%count
        particle = pf%particles(k)
        call mpi_recv(pf%psi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     end do
     do k = 1,pf%count
        particle = pf%particles(k)
!        if(j .eq. 1 .or. j .eq. 2) then
        if( j .eq. 1 .or. j .eq. 2 .or. j .eq. 49 .or. j .eq. 50) then
           if(pf%gen_data) then
              write(filename,'(A,i2.2,A)') 'start/',32,'.state'
           else
              write(filename,'(A,i2.2,A)') 'start/',pfrank,'.state'
           end if
           print*,'particle ',pfrank,' loading file ',filename
           call get_state(pf%psi(:,k),filename)
        end if
        call mpi_send(pf%psi(:,k), state_dim , MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM, mpi_err)
     end do
  end do
  end_t = mpi_wtime()
  write(6,*) 'Time for initial day run = ',end_t-start_t,' seconds.'
  pf%time=mpi_wtime()

  if(.not. pf%gen_Q) then

  DO k = 1,pf%count
     particle = pf%particles(k)
     print*,'receiving  from ',particle
     CALL MPI_IRECV(pf%psi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM,requests(k), mpi_err)
  end DO
  print*,'receives launched'
  k = 0
  received = .false.
  do
     k = mod(k,pf%count)+1
!     print*,k
     if(.not. received(k)) then
        particle = pf%particles(k)
!        print*,particle ,'not received so testing it'
        call MPI_TEST(requests(k), mpi_flag, mpi_statuses(:,k), mpi_err)
        
        if(mpi_flag) then
!           PRINT*,'Particle filter ',pfrank,'has received initial state_v&
!                &ector over mpi from ensemble member ',particle
           received(k) = .true.
!           if(.not. pf%gen_data) call perturb_particle(pf%psi(:,k))
           call perturb_particle(pf%psi(:,k))
        end if
     end if
     if(all(received)) exit
  end do
  write(6,*) 'PF: All models received in pf couple' 
  call flush(6)


!  if(pf%gen_data) call save_truth(pf%psi(:,1))
  call output_from_pf
  if(pf%gen_data) call save_truth(pf%psi(:,1))
  if(pf%use_traj) call trajectories
  start_t = mpi_wtime()

  do j=1,pf%time_obs
     write(6,*) 'PF: observation counter = ',j
     do i = 1,pf%time_bwn_obs-1
        pf%timestep = pf%timestep + 1
        if(pf%type .eq. 'EW') then
           call proposal_filter
        elseif(pf%type .eq. 'SI') then
           call stochastic_model
        elseif(pf%type .eq. 'SE') then
           call stochastic_model
        else
           print*,'Error -555: Incorrect pf%type'
        end if
!        write(6,*) 'PF: timestep = ',pf%timestep, 'after proposal filter'
        call flush(6)
        if(pf%use_traj) call trajectories
        call output_from_pf
     end do
           
     pf%timestep = pf%timestep + 1
     write(6,*) 'starting the equal weight filter step'
     call flush(6)


     if(pf%type .eq. 'EW') then
           call equal_weight_filter
        elseif(pf%type .eq. 'EZ') then
           call equivalent_weights_filter_zhu
        elseif(pf%type .eq. 'SI') then
           call sir_particle_filter
        elseif(pf%type .eq. 'SE') then
           call stochastic_model
           call diagnostics
        else
           print*,'Error -556: Incorrect pf%type'
        end if
     write(6,*) 'PF: timestep = ',pf%timestep, 'after equal weight filter'
     call flush(6)

     if(pf%use_traj) call trajectories
     call output_from_pf

  end do
  call diagnostics
  write(6,*) 'PF: finished the loop - now to tidy up'
  end_t = mpi_wtime()
  call flush(6)


  tag = 1        
  DO k = 1,pf%count
     particle = pf%particles(k)
     CALL MPI_ISEND(pf%psi(:,k), state_dim , MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM, requests(k), mpi_err)
     PRINT*,'Particle filter ',pfrank,'has sent final state_vector over mpi &
          &to ensemble member ',particle
  END DO
  CALL MPI_WAITALL(pf%count,requests,mpi_statuses, mpi_err)


  else

     call genQ

  end if


  
  call deallocate_data

  write(6,*) 'PF: finished deallocate_data - off to mpi_finalize'
  call flush(6)


  call MPI_Finalize(mpi_err)
  deallocate(requests)
  deallocate(mpi_statuses)
  deallocate(received)
  write(*,*) 'Program couple_pf terminated successfully.'
  write(*,*) 'Time taken in running the model = ',end_t-start_t
  write(*,*) 'Which works out at ',(end_t-start_t)/real(pf%time_obs),'seconds per day (72 timesteps)'


end program couple_pf



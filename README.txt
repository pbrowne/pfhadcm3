This is the repo for the particle filter code working with HadCM3.

Written mainly by Philip Browne (email: p.browne@reading.ac.uk)
with major help from Simon Wilson and based on code supplied by
Peter Jan van Leeuwen and Melanie Ades.

Time-stamp: <2014-01-09 14:09:07 pbrowne>

!!! Time-stamp: <2014-11-04 10:12:28 pbrowne>

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!subroutine proposal_filter based on subroutine IntegrateModel
!given from the Particle filter code of Mel and Peter Jan
!PAB 04-02-2013

subroutine proposal_filter
  use pf_control
  use Sizes
  use comms

  IMPLICIT NONE
  include 'mpif.h'
  integer, parameter :: rk = kind(1.0D0)

  real(kind=rk) :: pWeight!, qWeight
  real(kind=rk), dimension(state_dim,pf%count) :: normaln     !vector to store uncorrelated random error
  real(kind=rk), dimension(state_dim,pf%count) :: betan       !vector to store sqrtQ correlated random error
  real(kind=rk), dimension(obs_dim) :: y             !y, the observations
  real(kind=rk), dimension(obs_dim,pf%count) :: Hpsi          !H(psi^(n-1))
  real(kind=rk), dimension(obs_dim,pf%count) :: y_Hpsin1      !y-H(psi^(n-1))
  real(kind=rk), dimension(state_dim,pf%count) :: fpsi        !f(psi^(n-1))
  real(kind=rk), dimension(state_dim,pf%count) :: kgain       !QH^T(HQH^T+R)^(-1)(y-H(psi^(n-1)))
  real(kind=rk), dimension(state_dim,pf%count) :: Qkgain
  real(kind=rk) :: t,dnrm2
  integer :: particle,k,tag,mpi_err
  integer :: mpi_status( MPI_STATUS_SIZE )
  real(kind=rk), dimension(7) :: ti
  logical, parameter :: time = .false.

  if(.not. pf%gen_data) call get_observation_data(y)

  if(.not. pf%gen_data) then
     if(time) t = mpi_wtime()
     if(.not. all(pf%psi .eq. pf%psi)) then
        print*,'vags and quims. pf%psi has NaNs'
        stop
     end if

     call H(pf%psi,Hpsi)
     if(.not. all(Hpsi .eq. Hpsi)) then
        print*,'cock and balls. Hpsi introduced NaNs'
        stop
     end if
     if(time) ti(1) = mpi_wtime()-t
     !$omp parallel do
     do k = 1,pf%count
        y_Hpsin1(:,k) = y - Hpsi(:,k)
     end do
     !$omp end parallel do
     if(time) ti(2) = mpi_wtime()-ti(1) -t
  else
     y_Hpsin1 = 0.0_rk
  end if

  do k =1,pf%count
     particle = pf%particles(k)
     tag = 1
     if(.not. all(pf%psi(:,k) .eq. pf%psi(:,k))) then
        print*,'on send, pf%psi had some NaNs ',particle
     end if
     call mpi_send(pf%psi(:,k),state_dim,MPI_DOUBLE_PRECISION&
          &,particle-1,tag,CPL_MPI_COMM,mpi_err)
  end do
  if(time) ti(3) = mpi_wtime()-ti(2)-t
  DO k = 1,pf%count
     particle = pf%particles(k)
     tag = 1
     CALL MPI_RECV(fpsi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     if(.not. all(fpsi(:,k) .eq. fpsi(:,k))) then
        print*,'on recv, fpsi had some NaNs ',particle
     end if
  END DO
  if(time) ti(4) = mpi_wtime()-ti(3) -t

  call NormalRandomNumbers2D(0.0D0,1.0D0,state_dim,pf%count,normaln)
  if(time) ti(5) = mpi_wtime()-ti(4) -t
  call Bprime(y_Hpsin1,kgain,Qkgain,normaln,betan)
  if(time) ti(6) = mpi_wtime()-ti(5) -t

  !$omp parallel do private(particle,pweight)
  DO k = 1,pf%count
     particle = pf%particles(k)
!     print*,'pf',pfrank,'|fpsi-psi|_2 = ',dnrm2(state_dim,(fpsi(:,k)-pf
     !     %psi(:,k)),1)
!     print*,'calling update state on timestep ',pf%timestep
     call update_state(pf%psi(:,k),fpsi(:,k),Qkgain(:,k),betan(:,k))
     pweight = sum(Qkgain(:,k)*kgain(:,k))+2.0D0*sum(betan(:,k)*kgain(:,k))
     if(pweight .ne. pweight) then
        print*,'pweight NOT EQUAL pweight ',pweight
        print*,'THIS IS PARTICLE ',particle
        print*,'COMPONENTS:',sum(Qkgain(:,k)*kgain(:,k)),2.0D0*sum(betan(:,k)&
             &*kgain(:,k))
        print*,'stopping'
        stop
     end if
     pf%weight(particle) = pf%weight(particle) + 0.5*pWeight
  end DO
  !$omp end parallel do
  if(time) then
     ti(7) = mpi_wtime()-ti(6) -t
     print*,ti
  end if
end subroutine proposal_filter

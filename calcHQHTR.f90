subroutine calc_HQHTR
  use sizes
  use Qdata
  use Rdata
  use hqht_plus_r
  use hsl_mc68_double
  use hsl_mc69_double
  use hsl_ma87_double
  
  implicit none
  include 'mpif.h'
  integer, parameter :: rk = kind(1.0d0)
  integer :: i,j,k,Hne,ne,sizebig!,a,cc,d
  integer :: nnz,ierr
!  logical, allocatable, dimension(:) :: diag
  integer, allocatable, dimension(:) :: hrow,hcol,bigrow,bigcol,bigrowt,bigcolt
  integer, allocatable, dimension(:) :: jao,iao,jc,ic,iw,ir,jaot,iaot
!  integer, allocatable, dimension(:) :: Srow,Scol
!  real(kind=kind(1.0d0)), allocatable, dimension(:) :: Sval
  real(kind=rk), allocatable, dimension(:) :: hval,bigval,ao,c,bigvalt,aot
!  real(kind=rk) :: eig
  real(kind=rk), dimension(obs_dim) :: a1,b1,b2
  real(kind=rk), dimension(state_dim) :: a2,a3,a4,b4
  real(kind=rk) :: scal
  logical :: values
  integer :: ii,ka,kb,jj,nrow,ncol,nzmax,jpos,jcol,len
  type(mc68_control) :: control68
  type(mc68_info) :: info68

  integer :: lmap, flag
  integer, dimension(:), allocatable  :: ptr, row, map
  real(kind=rk), dimension(:), allocatable :: val



  print*,'in calc_HQHTR'
  !compute the largest eigenvalue of Q.
!  call power(eig)
!  open(3,file='Qeig.dat',action='write',status='replace')
!  write(3,*) eig
!  close(3)

!  !now scale Q by sqrt(eig)
!  Qval = Qval/sqrt(eig)


  !now we reduce Q to HQ by picking the appropriate entries

  k = 0
!  print*,'size Qrow/col = ',size(Qrow),size(Qcol)
!  print*,'but Qne = ',Qne
!  print*,'allocated Qrow',allocated(Qrow)

  do j = 1,Qne
     if(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 .and. Qcol(j) .ge.&
          & 539617 .and. Qcol(j) .le. 566986) then
        if(Qrow(j) .eq. Qcol(j)) then
           k = k + 1
        else
           k = k + 2
        end if
     elseif(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 ) then
        k = k + 1
     elseif(Qcol(j) .ge. 539617 .and. Qcol(j) .le. 566986) then
        k = k + 1
     end if
  end do

!  print*,'after loop'
!  print*,'k = ',k
!  print*,'obs_dim = ',obs_dim
  Hne = k+obs_dim
  allocate(Hrow(Hne),Hcol(Hne),Hval(Hne))

!  print*,'after cheese allocations'

  k = 0
!  d = 0
  do j = 1,Qne
     if(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 .and. Qcol(j) .ge.&
          & 539617 .and. Qcol(j) .le. 566986) then
        k = k + 1
        Hrow(k) = Qrow(j) - 539616
        Hcol(k) = Qcol(j)
!        if(Hrow(k) .eq. Hcol(k)) d = d + 1
        Hval(k) = Qval(j)
        if(Qrow(j) .ne. Qcol(j)) then
           k = k + 1
           Hrow(k) = Qcol(j) - 539616
           Hcol(k) = Qrow(j)
           Hval(k) = Qval(j)
        end if
     elseif(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 ) then
        k = k + 1
        Hrow(k) = Qrow(j) - 539616
        Hcol(k) = Qcol(j)! - 539616
        Hval(k) = Qval(j)
     elseif(Qcol(j) .ge. 539617 .and. Qcol(j) .le. 566986) then
        k = k + 1
        Hrow(k) = Qcol(j) - 539616
        Hcol(k) = Qrow(j)
        Hval(k) = Qval(j)
     end if
  end do
!  print*,'after chesse computations'

  Hrow(k+1:k+obs_dim) = (/ (i, i=1,obs_dim) /)
  Hcol(k+1:k+obs_dim) = (/ (i+539616, i=1,obs_dim) /)
  Hval(k+1:k+obs_dim) = 1.0_rk


!  print*,'reduced to HQ k = ',k+obs_dim
!  print*,'and contains diagonals ',d
!  print*,'minimum row is :',minval(Hrow)
!  print*,'maximum row is :',maxval(Hrow)
!  print*,'minimum col is :',minval(Hcol)
!  print*,'maximum col is :',maxval(Hcol)
  

!  Qn = state_dim
  !changing the matrix now:
!  allocate(Qdiag(Qn))
!  allocate(Srow(Qne-Qn),Scol(Qne-Qn),Sval(Qne-Qn))
!  cc = 0
!  a = 0
!  do i = 1,Qne
!     if(Qrow(i) .eq. Qcol(i)) then
!        a = a + 1
!        Qdiag(a) = Qval(i)
!     else
!        cc = cc + 1
!        Sval(cc) = Qval(i)
!        Srow(cc) = Qrow(i)
!        Scol(cc) = Qcol(i)
!     end if
!  end do
!  print*,'cc = ',cc


!  open(2,file='Qdata.dat',action='write',status='replace',form='unformatted')
!  write(2) Qn
!  write(2) cc
!  do i = 1,Qn
!     write(2) Qdiag(i)
!  end do
!  do i = 1,cc
!     write(2) Sval(i)
!  end do
!  do i = 1,cc
!     write(2) Srow(i)
!  end do
!  do i = 1,cc
!     write(2) Scol(i)
!  end do
!  close(2)
!  deallocate(Qval,Qrow,Qcol)
!  allocate(Qval(cc),Qrow(cc),Qcol(cc))
!  Qval = Sval
!  Qrow = Srow
!  Qcol = Scol
!  Qne = cc




!!$  k=0
!!$  do i = 1,Hne
!!$     if(Hrow(i) .ne. Hcol(i)) then
!!$        if(Hcol(i) .ge. 539617 .and. Hcol(i) .le. 566986) then
!!$           k = k + 1
!!$           bigrow(Hne+k)  = Hcol(i)-539616
!!$           bigrowT(Hne+k) = Hrow(i)
!!$           bigcol(Hne+k)  = Hrow(i)
!!$           bigcolT(Hne+k) = Hcol(i)-539616
!!$           bigval(Hne+k)  = Hval(i)
!!$           bigvalT(Hne+k) = Hval(i)
!!$        end if
!!$     end if
!!$  end do
  
  
  sizebig = k+obs_dim








  allocate(bigrow(sizebig),bigcol(sizebig),bigval(sizebig))
  allocate(bigrowT(sizebig),bigcolT(sizebig),bigvalT(sizebig))
  allocate(ao(sizebig),  jao(sizebig), iao(obs_dim+1))
  allocate(aoT(sizebig),jaoT(sizebig),iaoT(state_dim+1))
  allocate(c(4*(sizebig)),jc(4*(sizebig)),ic(4*(sizebig)),ir(4*(sizebig))&
       &,iw(state_dim))



 
  bigrow = Hrow
  bigcol = Hcol
  bigval = Hval
  bigrowT = Hcol
  bigcolT = Hrow
  bigvalT = Hval


  !now we must scale the values by applying the diag(scaled variances) to
  !the rows and columns of the respective matrices

  !have to scale the columns of bigrow/col/val to get Q working...
  do i = 1,Hne
      bigval(i) =  bigval(i)*Qdiag( bigrow(i)+539616)
  end do
  do i = 1,Hne
     bigvalT(i) = bigvalT(i)*Qdiag(bigcolT(i)+539616)
  end do
  




!  print*,'expanded to nonsymmetric form'
!  print*,'minimum row of HQ',minval(bigrow)
!  print*,'maximum row of HQ',maxval(bigrow)
!  print*,'minimum col of HQ',minval(bigcol)
!  print*,'maximum col of HQ',maxval(bigcol)
!  print*,'minimum row of (HQ)^T',minval(bigrowT)
!  print*,'maximum row of (HQ)^T',maxval(bigrowT)
!  print*,'minimum col of (HQ)^T',minval(bigcolT)
!  print*,'maximum col of (HQ)^T',maxval(bigcolT)






  call coocsr(obs_dim  ,sizebig,bigval ,bigrow ,bigcol ,ao ,jao ,iao)
!  print*,'converted first matrix to csr'
!  print*,minval(bigrowT),maxval(bigrowT)
!  print*,minval(bigcolT),maxval(bigcolT)
  call coocsr(state_dim,sizebig,bigvalT,bigrowT,bigcolT,aoT,jaoT,iaoT)
!  print*,'converted to compressed storage'


!  print*,'going into amub...'
  !       subroutine amub (nrow,ncol,job,a,ja,ia,b,jb,ib,
  !   *                  c,jc,ic,nzmax,iw,ierr) 

!  call amub(obs_dim,obs_dim,1,ao,jao,iao,ao,jao,iao,c,jc,ic,4*(2*Hne-obs_dim),iw,ierr)
!  call amub(obs_dim,state_dim,1,ao,jao,iao,aoT,jaoT,iaoT,c,jc,ic,4*(sizebig)
  !  ,iw,ierr)

  nrow = obs_dim
  ncol = state_dim
  nzmax = 4*sizebig
  values = .true.
  len = 0
  ic(1) = 1 
  ierr = 0
  !     initialize array iw.
  do j=1, ncol
     iw(j) = 0
  end do

  do ii=1, nrow 
     !     row i 
     do ka=iao(ii), iao(ii+1)-1 
        if (values) scal = ao(ka)
        jj   = jao(ka)
        do kb=iaoT(jj),iaoT(jj+1)-1
           jcol = jaoT(kb)
           jpos = iw(jcol)
           if (jpos .eq. 0) then
              len = len+1
              if (len .gt. nzmax) then
                 ierr = ii
                 print*,'ierr in amub = ',ierr
                 stop
              end if
              jc(len) = jcol
              iw(jcol)= len
              if (values) c(len)  = scal*aoT(kb)
           else
              if (values) c(jpos) = c(jpos) + scal*aoT(kb)
           end if
        end do
     end do
     do  k=ic(ii), len
        iw(jc(k)) = 0
     end do
     ic(ii+1) = len+1
  end do
           
           
           
           
!  print*,'amub ierr = ',ierr
!  print*,'multiplied matrix'
  call csrcoo(obs_dim,3,4*(sizebig),c,jc,ic,nnz,c,ir,jc,ierr)
!  print*,'csrcoo ierr = ',ierr
!  print*,'csrcoo nnz = ',nnz
!  print*,'converted to coordinate storage'
  ic = ir

  deallocate(Hrow,Hcol,Hval)
  ne = 0
  do i = 1,nnz
     if(ic(i) .le. jc(i)) then
        ne = ne+1
     end if
  end do
  allocate(HQHTRval(ne),HQHTRrow(ne),HQHTRcol(ne),HQHTR_order(state_dim))
  k = 0
  do i = 1,nnz
     if(ic(i) .le. jc(i)) then
        k = k + 1
        HQHTRrow(k) = ic(i)
        HQHTRcol(k) = jc(i)
        HQHTRval(k) = c(i)
     end if
  end do
!  print*,'reduced to upper triangular form'

!  print*,'minimum row in H:',minval(HQHTRrow)
!  print*,'maximum row in H:',maxval(HQHTRrow)
!  print*,'minimum col in H:',minval(HQHTRcol)
!  print*,'maximum col in H:',maxval(HQHTRcol)


!  open(2,file='HQHTRdata.dat',action='write',status='replace',form='unfor&
!       &matted')
!  write(2) obs_dim
!  write(2) ne
!  write(2) HQHTRrow
!  write(2) HQHTRcol
!  write(2) HQHTRval
!  close(2)


!TEST THAT SPARSE MATRIX MULTIPLICATION WORKED:
!CHECK H*Q*Q*HT == HQQHT
  do i = 1,obs_dim
     a1(i) = real(i,rk)
  end do
!  print*,'sum(a1**2) = ',sum(a1**2)
  call HT(a1,a2)
!  print*,'sum(a2**2) = ',sum(a2**2)
  call Q(1,a2,a3)
  call Qhalf(1,a2,a4)
  
  b4 = 0.0_rk
  do i = 1,sizebig
     b4(bigrowT(i)) = b4(bigrowT(i)) + bigvalT(i)*a1(bigcolT(i))
  end do


!  print*,'check that bigrowT/colT/valT works: ',sum((b4-a4)**2)
!  print*,'that is sum(b4**2) [reduced shit] = ',sum(b4**2)
!  print*,'    and sum(a4**3) [operatorssss] = ',sum(a4**2)

!  do i = 1,state_dim
!     if(abs(b4(i)-a4(i)) .gt. 1.0d-15) print*,i,b4(i),a4(i)
!  end do


!  print*,'sum(a3**2) = ',sum(a3**2)
!  call Q(1,a3,a2)
  call H(a3,b1)
!  print*,'sum(b1**2) = ',sum(b1**2)

  b2 = 0.0_rk
  do i = 1,ne
     if(HQHTRrow(i) .ne. HQHTRcol(i)) then
        b2(HQHTRrow(i)) = b2(HQHTRrow(i)) + HQHTRval(i)*a1(HQHTRcol(i))
        b2(HQHTRcol(i)) = b2(HQHTRcol(i)) + HQHTRval(i)*a1(HQHTRrow(i))
     else
        b2(HQHTRrow(i)) = b2(HQHTRrow(i)) + HQHTRval(i)*a1(HQHTRcol(i))
     end if
  end do

  print*,'testing if sparse matrix multiplication worked:...'
  print*,'H*Q*Q*HT == HQQHT'
  print*,'sum((b1-b2)**2) = ',sum((b1-b2)**2)
  print*,'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
  print*,'#######################################'

!  do i = 1,200
!     print*,i,b1(i),b2(i)
!  end do

!  i = maxval(maxloc(abs(b1-b2)))
!  print*,i,b1(i),b2(i)
  


  call loadR
  do i = 1,ne
     if(HQHTRrow(i) .eq. HQHTRcol(i)) HQHTRval(i) = HQHTRval(i) + Rdiag(HQHTRcol(i))
  end do


!  call checkHQHTR_factor(obs_dim,ne,HQHTRrow,HQHTRcol,HQHTRval)
  HQHTRn = obs_dim
  HQHTRne = ne
  allocate(ptr(HQHTRn+1))
!  print*,'reconstructing matrix in mc69:'


  call mc69_coord_convert(HSL_MATRIX_REAL_SYM_PSDEF, HQHTRn, HQHTRn,&
       &HQHTRne, HQHTRrow, HQHTRcol, ptr, row, &
       &flag, val_in=HQHTRval, val_out=val, lmap=lmap, map=map)
  print*,'mc69 finished with flag (should read 0): ',flag

!  print*,'getting pivot order from mc68'
  call mc68_order(1, HQHTRn, ptr, row, HQHTR_order, control68, info68)
!  print*,'pivot order returned from mc68'
  PRINT*,'INFO68 = ',info68

!  open(2,file='HQHTRdata.dat',action='write',status='old',form='unfor&
!       &matted',position='append')
!  write(2) HQHTR_order
!  close(2)

!  print*,'analyse phase starting'
  ! Analyse
  call ma87_analyse(HQHTRn, ptr, row, HQHTR_order, HQHTR_keep,&
       &HQHTR_control, HQHTR_info)
!  print*,'analysis finished'
!  print*,'factorisation starting:'
  ! Factor
  call ma87_factor(HQHTRn, ptr, row, val, HQHTR_order, HQHTR_keep,HQHTR_control&
       &, HQHTR_info)
  print*,'factorisation complete...'

  do i = 1,obs_dim
     a1(i) = real(i,rk)
  end do
  a1 = a1/sqrt(sum(a1**2))

!  print*,'sum(a1**2) = ',sum(a1**2)
  call HT(a1,a2)
!  print*,'sum(a2**2) = ',sum(a2**2)
  call Q(1,a2,a3)
  call H(a3,b1)
  b2 = b1 + Rdiag(1)*a1

  b1 = 0.0_rk
  call solve_hqht_plus_r(b2,b1)
  
  print*,'UNIT TEST'
  print*,'result of ||(HQHT+R)^(-1)(HQHT+R)x-x||_2 = ',sqrt(sum((a1-b1)**2))



  deallocate(ptr)
  deallocate(bigrow)
  deallocate(bigcol)
  deallocate(bigval)
  deallocate(bigrowT)
  deallocate(bigcolT)
  deallocate(bigvalT)
  deallocate(ao)
  deallocate(jao)
  deallocate(iao)
  deallocate(aoT)
  deallocate(jaoT)
  deallocate(iaoT)
  deallocate(c)
  deallocate(jc)
  deallocate(ic)
  deallocate(ir)
  deallocate(iw)

end subroutine calc_HQHTR




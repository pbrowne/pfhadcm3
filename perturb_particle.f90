!!! Time-stamp: <2014-11-03 11:47:50 pbrowne>

subroutine perturb_particle(x)
use sizes
use pf_control
use comms
integer, parameter :: rk=kind(1.0D0)
real(kind=rk), dimension(state_dim), intent(inout) :: x
real(kind=rk), dimension(state_dim) :: rdom,y,kgain
character(14) :: filename

if(pf%init .eq. 'P') then
   call NormalRandomNumbers1D(0.0D0,1.0D0,state_dim,rdom)
   call Qhalf(1,rdom,y)
   y = y*1.0d2
   rdom = 0.0_rk
   kgain = 0.0_rk
   call update_state(rdom,x,kgain,y)
!!$x = x + y
   x = rdom
elseif(pf%init .eq. 'R') then
   !get ensemble member from the restart folder
   write(filename,'(A,i2.2,A)') 'rstrt/',pfrank,'.state'
   call get_state(x,filename)
elseif(pf%init .eq. 'S') then
   !get ensemble member from the start folder
   if(pf%gen_data) then
      write(filename,'(A,i2.2,A)') 'start/',32,'.state'
   else
      write(filename,'(A,i2.2,A)') 'start/',pfrank,'.state'
      print*,'pf #',pfrank,' starting from ',filename
   end if
   call get_state(x,filename)
else
   print*,'ERROR: incorrect pf%init selected in perturb_particle'
   stop
end if

   
end subroutine perturb_particle


subroutine update_state(state,fpsi,kgain,betan)
use pf_control
use dump_netcdf
use sizes
use hadcm3_config
use comms
integer, parameter :: rk=kind(1.0D0)
real(kind=rk), dimension(state_dim), intent(out) :: state
real(kind=rk), dimension(state_dim), intent(in) :: fpsi,kgain
real(kind=rk), dimension(state_dim), intent(inout) :: betan
real(kind=rk), dimension(state_dim) :: diff
integer :: k,start
real(kind=rk) :: dnrm2
logical, parameter :: debug=.false.
logical, parameter :: norms=.false.
logical, parameter :: netcdf=.false.
character(len=11) :: filename


if(.not. all(fpsi .eq. fpsi)) then
   print*,'in update_state, input state has NaNs...'
   stop
end if


if(.not. all(kgain .eq. kgain)) then
   print*,'in update_state, input kgain has NaNs...'
   stop
end if

if(.not. all(betan .eq. betan)) then
   print*,'in update_state, input betan has NaNs...'
   stop
end if

!atmosphere humidity = state_vector(406465:539616)
do k = 406465,539616
   ! fpsi(i)+kgain(i)+betan(i) has to lie between 0 and 1   
!!$   betan(k) = max(-fpsi(k)-kgain(k),min(betan(k),1.0D0-fpsi(k)-kgain(k)))
   betan(k) = max(-fpsi(k)-kgain(k),betan(k))
end do


!ocean salinity = state_vector(997128:1454638)
!!$do k = 997128,1454638
!!$   ! fpsi(i)+kgain(i)+betan(i) has to lie between 0 and 1
!!$   betan(k) = max(-fpsi(k)-kgain(k),min(betan(k),1.0D0-fpsi(k)-kgain(k)))
!!$end do
if(norms) call check_norms(state,fpsi,kgain,betan)


if(netcdf) then
   if(pfrank .eq. 0) then
      write(filename,'(A,I3.3,A)') 'detr',pf%timestep,'.nc'
      call open_netcdf(filename)
      call put_netcdf(1,fpsi)
      call close_netcdf(1)
      
      write(filename,'(A,I3.3,A)') 'nudg',pf%timestep,'.nc'
      call open_netcdf(filename)
      call put_netcdf(1,kgain)
      call close_netcdf(1)

      write(filename,'(A,I3.3,A)') 'rand',pf%timestep,'.nc'
      call open_netcdf(filename)
      call put_netcdf(1,betan)
      call close_netcdf(1)
   end if
end if


!do the addition
state = fpsi+kgain+betan
if(debug) print*,'|fpsi|=',dnrm2(state_dim,fpsi,1),' |kgain|= ',dnrm2(state_dim,kgain&
     &,1),' |betan| = ',dnrm2(state_dim,betan,1)
if(norms) print*,'pf',pfrank,'|kgain|= ',dnrm2(state_dim,kgain,1),' |betan| = ',dnrm2(state_dim,betan,1)


!now fix the atmospheric polar values:
! PSTAR     U     V     THETA    Q

!index = 0
!do k=1,levels
!  do j=1,columns
!    do i=1,rows
!       index = index + 1
!       data_out(index)=data(i,j,k) == data(longitude,latitude,level)
!    enddo
!  enddo
!enddo

!at the poles, all the longitude values are equal
!first lets do Pstar
state(1:a_nxn) = state(1)
state((a_nyn-1)*a_nxn+1:a_nyn*a_nxn) = state((a_nyn-1)*a_nxn+1)

start = a_nxn*a_nyn   !for U
do k = 1,a_levels
   !j = 1
   state( start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + a_nxn )  =&
        & state( start+(k-1)*a_nyn*a_nxn + 1 )
if(debug)   print*,'fpsi test U k:',k,&
        maxval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : &
        start+(k-1)*a_nyn*a_nxn + a_nxn))-&
        minval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : &
        start+(k-1)*a_nyn*a_nxn + a_nxn))
   !j = a_nyn
   state( start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn) = state(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)&
        &*a_nxn + 1)
if(debug)   print*,'fpsi test U k:',k,maxval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn))-minval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn))
end do
start = a_nxn*a_nyn + 1*a_levels*a_nyn*a_nxn !for V
do k = 1,a_levels
   !j = 1
   !        
   state( start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + a_nxn )  =&
        & state( start+(k-1)*a_nyn*a_nxn + 1 )
if(debug)   print*,'fpsi test V k:',k,&
        maxval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn +&
        & a_nxn))&
        -minval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + a_nxn))
   !j = a_nyn
   !        
   state( start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn) = state(start+(k-1)*a_nyn*a_nxn +&
        & (a_nyn-1)&
        &*a_nxn + 1)
if(debug)   print*,'fpsi test V k:',k,maxval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn))-minval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn))
end do
start = a_nxn*a_nyn + 2*a_levels*a_nyn*a_nxn !for Theta
do k = 1,a_levels
   !j = 1
   !        
   state( start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + a_nxn )  =&
        & state( start+(k-1)*a_nyn*a_nxn + 1 )
if(debug)   print*,'fpsi test Theta k:',k,&
        maxval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn +&
        & a_nxn))&
        -minval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + a_nxn))
   !j = a_nyn
   !        
   state( start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn) = state(start+(k-1)*a_nyn*a_nxn +&
        & (a_nyn-1)&
        &*a_nxn + 1)
if(debug)   print*,'fpsi test Theta k:',k,maxval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn))-minval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn))
end do
start = a_nxn*a_nyn + 3*a_levels*a_nyn*a_nxn !for Q
do k = 1,a_levels
   !j = 1
   !        
   state( start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + a_nxn )  =&
        & state( start+(k-1)*a_nyn*a_nxn + 1 )
if(debug)   print*,'fpsi test Q:',k,':',maxval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : start+(k-1)&
        &*a_nyn*a_nxn + a_nxn))-minval(fpsi(start+(k-1)*a_nyn*a_nxn + 1 : start+(k&
        &-1)*a_nyn*a_nxn + a_nxn ))
   !j = a_nyn
   !        
   state( start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + 1 : start+(k-1)*a_nyn&
        &*a_nxn +&
        & (a_nyn-1)*a_nxn + a_nxn) = state(start+(k-1)*a_nyn*a_nxn +&
        & (a_nyn-1)&
        &*a_nxn + 1)
if(debug)   print*,'fpsi test Q:',k,':',&
        maxval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)&
        &*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + a_nxn))&
        -minval(fpsi(start+(k-1)*a_nyn*a_nxn + (a_nyn-1)&
        &*a_nxn + 1 : start+(k-1)*a_nyn*a_nxn + (a_nyn-1)*a_nxn + a_nxn))
end do

if(debug)print*,'PSTAR: min fpsi= ',minval(fpsi(1:7008))
if(debug)print*,'PSTAR: max fpsi= ',maxval(fpsi(1:7008))
if(debug)print*,'PSTAR: min state= ',minval(state(1:7008))
if(debug)print*,'PSTAR: max state= ',maxval(state(1:7008))




if(debug)diff = abs(state-fpsi)
if(debug)print*,'max perturbation is ',maxval(diff),' at ',maxloc(diff),' of '&
     &,state(maxloc(diff))
if(debug)diff = diff/(max(abs(fpsi),1.0d-8))
if(debug)print*,'max prop perturb is ',maxval(diff),' at ',maxloc(diff),' of '&
     &,fpsi(maxloc(diff))



if(.not. all(state .eq. state)) then
   print*,'in update_state, output fpsi has NaNs...'
   stop
end if
end subroutine update_state

subroutine check_norms(state,fpsi,kgain,betan)
use sizes
use hadcm3_config
use comms
integer, parameter :: rk=kind(1.0D0)
real(kind=rk), dimension(state_dim), intent(in) :: state
real(kind=rk), dimension(state_dim), intent(in) :: fpsi,kgain
real(kind=rk), dimension(state_dim), intent(in) :: betan
integer :: i
integer, dimension(9) :: srt,end
real(kind=rk) :: dnrm2

real(kind=rk), dimension(9,3) :: norms
character(6), dimension(9) :: files


files = (/"ap.txt","au.txt","av.txt","at.txt","aq.txt","ot.txt","os.txt","ou.t&
     &xt","ov.txt"/)
end = (/7008,140160,273312,406464,539616,997127,1454638,1884534,2314430/)
srt(1) = 1
srt(2:9) = end(1:8)+1


do i = 1,9
   norms(i,1) = dnrm2(1+end(i)-srt(i),fpsi(srt(i):end(i))-state(srt(i):end(i)),1)
   norms(i,2) = dnrm2(1+end(i)-srt(i),kgain(srt(i):end(i)),1)
   norms(i,3) = dnrm2(1+end(i)-srt(i),betan(srt(i):end(i)),1)
end do

do i = 1,9
   open(36,file=files(i),action='write',position='append')
   write(36,*) norms(i,:)
   close(36)
end do


end subroutine check_norms

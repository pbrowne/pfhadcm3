!!! Time-stamp: <2014-02-11 18:08:49 pbrowne>

program make_histograms
  use histogram_data
  use pf_control
  implicit none
  integer, parameter :: state_dim = 2314430
  integer, parameter :: obs_dim = 27370
  integer, parameter :: rk = kind(1.0d0)


  real(kind=rk), dimension(:,:), allocatable :: HHpsi
  real(kind=rk), dimension(:),allocatable :: bin_marker
  real(kind=rk), dimension(state_dim) :: y
  integer :: particle,time,i,j,mpi_err,lb,npfs,timestep
  logical :: placed
  integer, dimension(:,:), allocatable :: talagrand,reduced_talagrand
  LOGICAL :: file_exists
  integer :: pfrank,nens,time_obs
  character(32) :: filename
  include 'mpif.h'

  CALL MPI_INIT (mpi_err)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,pfrank,mpi_err)
  call mpi_comm_size(mpi_comm_world,npfs,mpi_err)

  i = 0
  do
     i = i + 1
     write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',i,'particle',1
     !print*,filename
     INQUIRE(FILE=filename, EXIST=file_exists)
     if(.not. file_exists) then
        print*,filename
        time_obs = i-1
        exit
     end if
  end do
  print*,'time_obs=',time_obs
  pf%time_obs = time_obs - 1
  
  file_exists = .true.
  i = 0
  do 
     i = i + 1
     write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',1,'particle',i
     INQUIRE(FILE=filename, EXIST=file_exists)
     if(.not. file_exists) then
        print*,filename
        nens = i-1
        exit
     end if
  end do
 
  print*,'nens=',nens
  print*,'npfs = ',npfs
  call load_histogram_data
  
  allocate(HHpsi(rhl_n,nens),bin_marker(nens),reduced_talagrand(9,nens+1)&
       &,talagrand(9,nens+1))

  print*,'loaded histogram data'
  talagrand = 0
  !     call mpi_barrier(pf_mpi_comm,mpi_err)

  do time = 1,time_obs
     !              print*,'time = ',time
     if(mod(time,npfs) .eq. pfrank) then
        timestep = time
        call get_truth(y,timestep)
        !              print*,'got obs data at timestep ',timestep
        do particle = 1,nens
           write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',timestep,'&
                &particle',particle
           open(13, file=filename,action='read',status='old',form='un&
                &for&
                &matted')
           do i = 1,rhl_n
              read(13) HHpsi(i,particle)
           end do
           close(13)
        end do
        !             print*,'read HHpsi'
        

        do j = 1,rhn_n !for each histogram we want to make
           if( j .eq. 1) then
              lb = 1
           else
              lb = sum(rank_hist_nums(1:j-1)) + 1
              end if
              do i = lb,sum(rank_hist_nums(1:j))
                 
                 do particle = 1,nens
                    bin_marker(particle) = HHpsi(i,particle)
                 end do
                 
                 call quicksort_d(bin_marker,nens) 
                 !                    print*,'quicksorted'
                 placed = .false.
                 do particle  = 1,nens
                    if(y(rank_hist_list(i)) .lt. bin_marker(particle)) then
                       talagrand(j,particle) = talagrand(j,particle) + 1
                       placed = .true.
                       exit
                    end if
                 end do
                 !                    print*,'did we place?'
                 
                 if(.not. placed) then
                    if(y(rank_hist_list(i)) .ge. bin_marker(nens)) then
                       talagrand(j,nens+1) = talagrand(j,nens+1) + 1
                    else
                       stop 'There was an error in the calculation of the p&
                            &lacement &
                            &in the rank histogram. Bums.'
                    end if
                 end if
                 
              end do
           end do
           
        end if !end of mpi splitting by timestep
     end do !end of the timestep
     !           print*,'end of all the timesteps'
     
     !now let us reduce the information to the master processor:
     call mpi_reduce(talagrand,reduced_talagrand,rhn_n*(nens+1)&
          &,mpi_integer,mpi_sum,0,mpi_comm_world,mpi_err)
     if(mpi_err .ne. 0) then
        print*,mpi_err,pfrank
        stop
     end if
     
     !           print*,'some reduction just happened'
     
     if(pfrank .eq. 0) then
        do i = 1,rhn_n
           write(filename,'(A,i1.0)') 'histogram_',i
           open(17,file=filename,action='write',status='replace')
           do j = 1,nens+1
              write(17,'(i8.8)') reduced_talagrand(i,j)
           end do
           close(17)
        end do
        !now output the image
     end if
     
     call kill_histogram_data

     if(pfrank .eq. 0) then
        call convert_pf_mean
     end if
     if(pfrank .eq. 1 .or. npfs .eq. 1) then
        call convert_pf_var
     end if

     
     

     print*,'pfrank = ',pfrank,' calling mpi finalize...',mpi_err
     call mpi_finalize(mpi_err)
     print*,'pfrank = ',pfrank,' mpi_err = ',mpi_err
     deallocate(hhpsi,talagrand,reduced_talagrand,bin_marker)
     
end program make_histograms

subroutine get_truth(x,timestep)
  
  implicit none
  integer, parameter :: rk = kind(1.0D0)
  integer, parameter :: state_dim = 2314430
  integer, parameter :: obs_dim = 27370
  
  !  real(kind=rk), dimension(state_dim), intent(out) :: hx                                
  real(kind=rk), dimension(state_dim), intent(out) :: x
  INTEGER, INTENT(IN) :: timestep
  integer :: ios,length,rec
  inquire(iolength=length) x
  open(62,file='pf_truth',iostat=ios,action='read',form='unformatted',access='&
       &direct',recl=length)
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_truth to read'
     write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
     stop
  end if
!  do i = 0,pf%timestep
  rec = timestep + 1
  read(62,rec=rec) x
!  end do
  close(62)


  !  call H(1,x,hx)                                                                        

end subroutine get_truth

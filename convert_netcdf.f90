!!! Time-stamp: <2014-10-15 14:24:05 pbrowne>

module dump_netcdf
  integer :: ncid
  integer :: a_lat_y_blue_varid, a_lon_x_blue_varid
  integer :: a_lat_y_red_varid, a_lon_x_red_varid,a_ver_z_varid
  integer :: a_pstar_varid, a_u_varid
  integer :: a_v_varid
  integer :: a_theta_varid
  integer :: a_q_varid
  integer :: timestep_varid
  integer :: o_theta_varid
  integer :: o_sal_varid
  integer :: o_u_varid
  integer :: o_v_varid
  integer :: o_lat_y_blue_varid, o_lon_x_blue_varid
  integer :: o_lat_y_red_varid, o_lon_x_red_varid,o_ver_z_varid
end module dump_netcdf

subroutine convert_pf_mean
  use dump_netcdf
  use pf_control
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, parameter :: state_dim = 2314430
  integer :: ios
  integer :: time,length,rec
  character(len=11) :: filename
  real(kind=rk), dimension(state_dim) :: state_vector
  filename = 'pf_mean.nc'
  call open_netcdf(filename)
  time = 0
  inquire(iolength=length) state_vector
  open(88,file='pf_mean',action='read',status='old',iostat=ios,form='unformatt&
       &ed',access='direct',recl=length)
  if(ios .ne. 0) stop 'file pf_mean does not exist'
  do rec = 0,pf%time_obs
     print*,'mean rec = ',rec
     read(88,rec=rec+1) state_vector
     time = time + 1
     call put_netcdf(time,state_vector)
  end do
!  120 continue
  close(88)
  call close_netcdf(time)
end subroutine convert_pf_mean

subroutine convert_pf_var
  use dump_netcdf
  use pf_control
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, parameter :: state_dim = 2314430
  integer :: ios
  integer :: time,length,rec
  character(len=11) :: filename
  real(kind=rk), dimension(state_dim) :: state_vector
  filename = 'pf_var.nc'
  print*,'opening netcdf'
  call open_netcdf(filename)
  print*,'after opening netcdf'
  time = 0
  inquire(iolength=length) state_vector
  open(88,file='pf_var',action='read',status='old',iostat=ios,form='unformatte&
       &d',access='direct',recl=length)
  if(ios .ne. 0) stop 'file pf_mean does not exist'
  do rec = 0,pf%time_obs
     print*,'var rec = ',rec
     read(88,rec=rec+1) state_vector
     time = time + 1
     call put_netcdf(time,state_vector)
  end do
!  120 continue
  close(88)
  call close_netcdf(time)
end subroutine convert_pf_var

subroutine convert_pf_truth
  use dump_netcdf
  use pf_control
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, parameter :: state_dim = 2314430
  integer :: ios
  integer :: time,length,rec
  character(len=11) :: filename
  real(kind=rk), dimension(state_dim) :: state_vector
  filename = 'pf_truth.nc'
  call open_netcdf(filename)
  time = 0
  inquire(iolength=length) state_vector
  open(88,file='pf_truth',action='read',status='old',iostat=ios,form='unformat&
       &ted',access='direct',recl=length)
  if(ios .ne. 0) stop 'file pf_truth does not exist'
  do rec = 0,pf%time_obs
     read(88,rec=rec+1) state_vector
     time = time + 1
     call put_netcdf(time,state_vector)
  end do
!  120 continue
  close(88)
  call close_netcdf(time)
end subroutine convert_pf_truth

!!$program dump
!!$  use dump_netcdf
!!$  implicit none
!!$  integer, parameter :: rk = kind(1.0d0)
!!$  integer, parameter :: state_dim = 2314430
!!$  integer :: ios
!!$  integer :: time
!!$  character(len=11) :: filename
!!$  real(kind=rk), dimension(state_dim) :: state_vector
!!$
!!$  filename = 'pf_mean.nc'
!!$  call open_netcdf(filename)
!!$
!!$  time = -1
!!$  open(88,file='pf_mean',action='read',status='old',iostat=ios)
!!$  if(ios .ne. 0) stop 'file pf_mean does not exist'
!!$  do 
!!$     read(88,*,end=120) state_vector
!!$     time = time + 1
!!$     call put_netcdf(time,state_vector)
!!$  
!!$  end do
!!$  120 continue
!!$  close(88)
!!$  call close_netcdf(time)
!!$
!!$end program dump

subroutine put_netcdf(time,state_vector)
  use dump_netcdf
  use netcdf
  use hadcm3_config
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, parameter :: state_dim = 2314430
  
  real(kind=rk), dimension(state_dim), intent(in) :: state_vector
  integer, intent(in) :: time

!!$  integer, parameter :: a_levels = 19,a_nyn=73,a_nxn=96
!!$  integer, parameter :: o_nxn=290,o_nyn=144,o_levels=20
  real(kind=rk) :: NaN  

  real(kind=rk), dimension(a_nxn,a_nyn,a_levels) :: a_u,a_v&
       &,a_theta,a_q
  real(kind=rk), dimension(a_nxn,a_nyn) :: a_pstar
  real(kind=rk), dimension(o_nxn,o_nyn,o_levels) :: o_u,o_v&
       &,o_theta,o_sal

!!$  integer, dimension(o_nxn,o_nyn) :: q_depth,p_depth

  integer,allocatable :: counts(:),start(:)
  integer :: count,i,j,k

  NaN = -1.0d20

!!$  call load_bathimitry(q_depth,p_depth)

  !let us calculate the position vectors: put everything in its place:
  count = 0
  !first is PSTAR (2d)
  do j = 1,a_nyn
     do i = 1,a_nxn
        count = count + 1
        a_pstar(i,j) = state_vector(count)
     end do
  end do
!  print*,'a_pstar finishes on the ',count,' element'
  !second is  U
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_u(i,j,k) = state_vector(count)
        end do
     end do
  end do
!  print*,'a_u finishes on the ',count,' element'
  !third is V
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_v(i,j,k) = state_vector(count)
        end do
     end do
  end do
!  print*,'a_v finishes on the ',count,' element'
  !fouth is THETA
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_theta(i,j,k) = state_vector(count)
        end do
     end do
  end do
!  print*,'a_theta finishes on the ',count,' element'
  !fifth is Q
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_q(i,j,k) = state_vector(count)
        end do
     end do
  end do
!  print*,'a_q finishes on the ',count,' element'
  !now we are onto the ocean:
  !sixth is THETA
!  print*,'first entry of SST is then:', count+1
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_theta(i,j,k) = state_vector(count)
           else
              o_theta(i,j,k) = NaN
           end if
        end do
     end do
!     if(k .eq. 1) print*,'final entry of SST is then:',count
  end do
!  print*,'o_theta finished on the ',count,' element'
  !seventh is SALINITY
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_sal(i,j,k) = state_vector(count)
           else
              o_sal(i,j,k) = NaN
           end if
        end do
     end do
  end do
!  print*,'o_sal finished on the ',count,' element'
  !eighth is U
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_u(i,j,k) = state_vector(count)
           else
              o_u(i,j,k) = NaN
           end if
        end do
     end do
  end do
!  print*,'o_u finished on the ',count,' element'
  !ninth is V
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_v(i,j,k) = state_vector(count)
           else
              o_v(i,j,k) = NaN
           end if
        end do
     end do
  end do
!  print*,'o_v finished on the ',count,' element'
  !FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT
!  print*,'FINISHED PUTTING THE STATE VECTOR INTO EACH VARIABLE'


  allocate(counts(3),start(3))
  counts = (/a_nxn,a_nyn,1/)
  start = (/1,1,time/)
!  print*,'before dumping a_pstar'
  call check( nf90_put_var(ncid, a_pstar_varid, a_pstar,start = start&
       &,count=counts) )
  deallocate(counts,start)
  allocate(counts(4),start(4))
  counts = (/a_nxn,a_nyn,a_levels,1/)
  start = (/1,1,1,time/)
!  print*,'before dumping a_u'
  call check( nf90_put_var(ncid, a_u_varid, a_u,start = start&
       &,count=counts) )
!  print*,'before dumping a_v'
  call check( nf90_put_var(ncid, a_v_varid, a_v,start = start&
       &,count=counts) )
!  print*,'before dumping a_theta'
  call check( nf90_put_var(ncid, a_theta_varid, a_theta,start = start&
       &,count=counts) )
!  print*,'before dumping a_q'
  call check( nf90_put_var(ncid, a_q_varid, a_q,start = start&
       &,count=counts) )

  counts = (/o_nxn,o_nyn,o_levels,1/)
  call check( nf90_put_var(ncid, o_u_varid, o_u,start = start&
       &,count=counts) )
!  print*,'before dumping o_u'
  call check( nf90_put_var(ncid, o_v_varid, o_v,start = start&
       &,count=counts) )
!  print*,'before dumping o_v'
  call check( nf90_put_var(ncid, o_theta_varid, o_theta,start = start&
       &,count=counts) )
!  print*,'before dumping o_theta'
  call check( nf90_put_var(ncid, o_Sal_varid, o_sal,start = start&
       &,count=counts) )
!  print*,'before dumping o_sal'
  deallocate(counts,start)


end subroutine put_netcdf

subroutine open_netcdf(filename)
  use dump_netcdf
  use netcdf
  use hadcm3_config
  implicit none

  character(len=11), intent(in) :: filename
  integer, parameter :: rk = kind(1.0d0)
!!$  integer, parameter :: a_levels = 19,a_nyn=73,a_nxn=96
!!$  integer, parameter :: o_nxn=290,o_nyn=144,o_levels=20
  real(kind=rk) :: NaN

  real(kind=rk), dimension(a_nyn) :: a_lat_y_blue,a_lat_y_red
  real(kind=rk), dimension(a_nxn) :: a_lon_x_blue,a_lon_x_red
  real(kind=rk), dimension(a_levels) :: a_ver_z
  real(kind=rk), dimension(o_nyn) :: o_lat_y_blue,o_lat_y_red
  real(kind=rk), dimension(o_nxn) :: o_lon_x_blue,o_lon_x_red
  real(kind=rk), dimension(o_levels) :: o_ver_z
  character (len = *), parameter :: A_LAT_NAME_BLUE = "latitude theta grid"
  character (len = *), parameter :: A_LON_NAME_BLUE = "longitude theta&
       & grid"
  character (len = *), parameter :: A_LAT_NAME_RED = "latitude u grid"
  character (len = *), parameter :: A_LON_NAME_RED = "longitude u grid"
  character (len = *), parameter :: A_VER_NAME = "atmosphere_hybrid_sigma_pressure_coordinate"
  character (len = *), parameter :: TIME_NAME = 'Timestep'
  integer :: a_lat_y_blue_dimid, a_lon_x_blue_dimid
  integer :: a_lat_y_red_dimid, a_lon_x_red_dimid,a_ver_z_dimid
  integer :: timestep_dimid

  !ocean coordinate variables
  character (len = *), parameter :: o_lat_name_blue = "latitude theta &
       &grid ocean"
  character (len = *), parameter :: o_LON_NAME_BLUE = "longitude theta&
       & grid ocean"
  character (len = *), parameter :: o_LAT_NAME_RED = "latitude u grid ocean"
  character (len = *), parameter :: o_LON_NAME_RED = "longitude u grid&
       & ocean"
  character (len = *), parameter :: o_VER_NAME = "depth"

  integer :: o_lat_y_blue_dimid, o_lon_x_blue_dimid
  integer :: o_lat_y_red_dimid, o_lon_x_red_dimid,o_ver_z_dimid

  character (len = *), parameter :: o_theta_NAME="sea_water_temperature"
  character (len = *), parameter :: o_sal_NAME="sea_water_practical_salinity"
  character (len = *), parameter :: o_u_NAME="eastward_sea_water_velocity"
  character (len = *), parameter :: o_V_NAME="northward_sea_water_velocity"

  character (len = *), parameter :: a_pstar_name="surface_air_pressure"
  character (len = *), parameter :: A_U_NAME="eastward_wind"
  integer,allocatable :: dimids(:)

  character (len = *), parameter :: A_V_NAME="northward_wind"
  character (len = *), parameter :: a_theta_name="air_potential_temperature"
  character (len = *), parameter :: a_q_name="specific_humidity"
  character (len = *), parameter :: HUM_UNITS = "kg kg^-1"
  character (len = *), parameter :: SAL_UNITS = "(PSU-35)/1000"

  ! It's good practice for each variable to carry a "units" attribute.
  character (len = *), parameter :: UNITS = "units"
  character (len = *), parameter :: PRES_UNITS = "Pa"
  character (len = *), parameter :: TEMP_UNITS = "K"
  character (len = *), parameter :: TEMP_UNITS_OCEAN = "degrees_Celcius"
  character (len = *), parameter :: LAT_UNITS = "degrees_north"
  character (len = *), parameter :: LON_UNITS = "degrees_east"
  character (len = *), parameter :: ATVER_UNITS = "1"
  character (len = *), parameter :: VER_UNITS = "metres"
  character (len = *), parameter :: VELO_UNITS_A= "m/s"
  character (len = *), parameter :: VELO_UNITS_O= "cm/s"
  character (len = *), parameter :: TIME_UNITS= "days since 1859-12-1"

  character (len = *), parameter :: POSITIVE = "positive"
  character (len = *), parameter :: DOWN = "down"
  character (len = *), parameter :: BOUNDS = "time_bounds"
  character (len = *), parameter :: CALENDAR = "360_day"

  character (len = *), parameter :: LONG_NAME = "long_name"
  character (len = *), parameter :: STD_NAME = "standard_name"

  integer :: i
  
  NaN = -1.0d20

  a_lon_x_blue = (/ (  real(i-1,rk)        *3.75_rk , i = 1,a_nxn) /)
  a_lon_x_red  = (/ ( (real(i-1,rk)+0.5_rk)*3.75_rk , i = 1,a_nxn) /) 
  a_lat_y_blue = (/ ( real(i,rk)*90.0_rk/36.0_rk , i = 36,-36,-1) /)
  a_lat_y_red = (/ ( real(i,rk)*90.0_rk/36.0_rk + 5.0_rk/4.0_rk , i = 36,-36,-1) /)

  a_ver_z(19:1:-1) = (/4.605881_rk,14.797166_rk,29.594332_rk,56.854245_rk,99.246778_rk&
       &,149.501254_rk,199.626815_rk,249.701774_rk,299.751624_rk,354.697739_rk&
       &,422.103115_rk,504.521846_rk,599.503247_rk,699.574362_rk,792.228485_rk&
       &,869.832302_rk,930.416785_rk,974.955913_rk,996.998925_rk/)

  o_lon_x_blue = (/ (real(i-1,rk)*1.25_rk          , i = 1,o_nxn) /)
  o_lon_x_red  = (/ (real(i-1,rk)*1.25_rk+0.625_rk , i = 1,o_nxn) /)
  o_lat_y_blue = (/ (real(i)*1.25_rk-90.625_rk, i = 1,o_nyn) /)
  o_lat_y_red  = (/ (real(i)*1.25_rk-91.25_rk , i = 1,o_nyn) /)

  o_ver_z = (/5.0_rk,15.0_rk,25.0_rk,35.1_rk,47.85_rk,67.0_rk&
       &,95.75_rk,138.9_rk,203.7_rk,301.0_rk&
       &,447.05_rk,666.3_rk,995.55_rk,1500.85_rk,2116.15_rk,2731.4_rk&
       &,3346.65_rk,3961.9_rk&
       &,4577.15_rk,5192.45_rk /)

!  write(*,'(A,96(f0.3,x))'),'a_lon_x_blue = ',a_lon_x_blue
!  write(*,'(A,96(f0.3,x))'),'a_lon_x_red = ',a_lon_x_red
!  write(*,'(A,73(f0.3,x))'),'a_lat_y_blue = ',a_lat_y_blue
!  write(*,'(A,73(f0.3,x))'),'a_lat_y_red = ',a_lat_y_red
!  write(*,'(A,19(f0.6,x))'),'a_ver_z = ',a_ver_z
!  write(*,'(A,20(f0.3,x))'),'o_ver_z = ',o_ver_z

  ! Create the file. 
  call check( nf90_create(filename, nf90_clobber, ncid) )

  ! Define the dimensions.
  call check( nf90_def_dim(ncid, A_LAT_NAME_BLUE, a_nyn   , a_lat_y_blue_dimid) )
  call check( nf90_def_dim(ncid, A_LON_NAME_BLUE, a_nxn   , a_lon_x_blue_dimid) )
  call check( nf90_def_dim(ncid, A_LAT_NAME_RED , a_nyn   , a_lat_y_red_dimid) )
  call check( nf90_def_dim(ncid, A_LON_NAME_RED , a_nxn   , a_lon_x_red_dimid) )
  call check( nf90_def_dim(ncid, A_VER_NAME     , a_levels, a_ver_z_dimid) )
 
  call check( nf90_def_dim(ncid, O_LAT_NAME_BLUE, o_nyn   ,&
       & o_lat_y_blue_dimid) )
  call check( nf90_def_dim(ncid, O_LON_NAME_BLUE, o_nxn   ,&
       & o_lon_x_blue_dimid) )
  call check( nf90_def_dim(ncid, O_LAT_NAME_RED , o_nyn   ,&
       & o_lat_y_red_dimid) )
  call check( nf90_def_dim(ncid, O_LON_NAME_RED , o_nxn   ,&
       & o_lon_x_red_dimid) )
  call check( nf90_def_dim(ncid, O_VER_NAME     , o_levels,&
       & o_ver_z_dimid) )

  print*,'after dimensions of coordinates are specified'

  ! Define the coordinate variables. They will hold the coordinate
  ! information, that is, the latitudes and longitudes. A varid is
  ! returned for each.
  call check( nf90_def_var(ncid, A_LAT_NAME_BLUE, NF90_DOUBLE, a_lat_y_blue_dimid,&
       & a_lat_y_blue_varid) )
  call check( nf90_def_var(ncid, A_LON_NAME_BLUE, NF90_DOUBLE, a_lon_x_blue_dimid,&
       & a_lon_x_blue_varid) )
  call check( nf90_def_var(ncid, A_LAT_NAME_RED, NF90_DOUBLE, a_lat_y_red_dimid,&
       & a_lat_y_red_varid) )
  call check( nf90_def_var(ncid, A_LON_NAME_RED, NF90_DOUBLE, a_lon_x_red_dimid,&
       & a_lon_x_red_varid) )
  call check( nf90_def_var(ncid, A_VER_NAME, NF90_DOUBLE, a_ver_z_dimid,&
       & a_ver_z_varid) )
  print*,'after atmosphere dimid and varid '
  call check( nf90_def_var(ncid, O_LAT_NAME_BLUE, NF90_DOUBLE,&
       & o_lat_y_blue_dimid,&
       & o_lat_y_blue_varid) )
  call check( nf90_def_var(ncid, O_LON_NAME_BLUE, NF90_DOUBLE,&
       & o_lon_x_blue_dimid,&
       & o_lon_x_blue_varid) )
  call check( nf90_def_var(ncid, O_LAT_NAME_RED, NF90_DOUBLE,&
       & o_lat_y_red_dimid,&
       & o_lat_y_red_varid) )
  call check( nf90_def_var(ncid, O_LON_NAME_RED, NF90_DOUBLE,&
       & o_lon_x_red_dimid,&
       & o_lon_x_red_varid) )
  call check( nf90_def_var(ncid, O_VER_NAME, NF90_DOUBLE,&
       & o_ver_z_dimid,&
       & o_ver_z_varid) )
  print*,'after ocean dimid and varid'
  ! Assign units attributes to coordinate var data. This attaches a
  ! text attribute to each of the coordinate variables, containing the
  ! units.
  call check( nf90_put_att(ncid, a_lat_y_blue_varid, UNITS, LAT_UNITS) )
  call check( nf90_put_att(ncid, a_lon_x_blue_varid, UNITS, LON_UNITS) )
  call check( nf90_put_att(ncid, a_lat_y_red_varid, UNITS,&
       & LAT_UNITS) )
  call check( nf90_put_att(ncid, a_lon_x_red_varid, UNITS,&
       & LON_UNITS) )
  call check( nf90_put_att(ncid, a_ver_z_varid, UNITS, ATVER_UNITS) )
  call check( nf90_put_att(ncid, a_ver_z_varid, POSITIVE, DOWN) )
  

  call check( nf90_put_att(ncid, o_lat_y_blue_varid, UNITS,&
       & LAT_UNITS) )
  call check( nf90_put_att(ncid, o_lon_x_blue_varid, UNITS,&
       & LON_UNITS) )
  call check( nf90_put_att(ncid, o_lat_y_red_varid, UNITS,&
       & LAT_UNITS) )
  call check( nf90_put_att(ncid, o_lon_x_red_varid, UNITS,&
       & LON_UNITS) )
  call check( nf90_put_att(ncid, o_ver_z_varid, UNITS, VER_UNITS) )
  call check( nf90_put_att(ncid, o_ver_z_varid, POSITIVE, DOWN) )
  
  print*,'after units of coordinated'
  !define the time dimension as unlimited length
  call check( nf90_def_dim(ncid, TIME_NAME, nf90_unlimited,&
       & timestep_dimid) )
  call check( nf90_def_var(ncid, TIME_NAME, NF90_int, timestep_dimid&
       &,timestep_varid) )
  call check( nf90_put_att(ncid, timestep_varid, UNITS, TIME_UNITS) )

  print*,'after time dimensions defined'

  ! Define the netCDF variables. The dimids array is used to pass
  !  the
  ! dimids of the dimensions of the netCDF variables.
  allocate(dimids(3))
  dimids = (/ a_lon_x_blue_dimid, a_lat_y_blue_dimid ,timestep_dimid/)
  call check( nf90_def_var(ncid, a_pstar_name, NF90_DOUBLE, dimids,&
       & a_pstar_varid) )
  print*,'after pstar defined'
  deallocate(dimids)
  allocate(dimids(4))
  dimids = (/ a_lon_x_red_dimid, a_lat_y_red_dimid, a_ver_z_dimid ,timestep_dimid/)
  call check( nf90_def_var(ncid, A_U_NAME, NF90_DOUBLE, dimids,&
       & a_u_varid) )
  call check( nf90_def_var(ncid, A_V_NAME, NF90_DOUBLE, dimids,&
       & a_v_varid) )
  dimids = (/ a_lon_x_blue_dimid,a_lat_y_blue_dimid,a_ver_z_dimid ,timestep_dimid/)
  call check( nf90_def_var(ncid, A_theta_NAME, NF90_DOUBLE, dimids,&
       & a_theta_varid) )
  call check( nf90_def_var(ncid, A_q_NAME, NF90_DOUBLE, dimids,&
       & a_q_varid) )
  print*,'after atmos defined'
  dimids = (/ o_lon_x_blue_dimid, o_lat_y_blue_dimid,o_ver_z_dimid ,timestep_dimid/)
  call check( nf90_def_var(ncid, o_theta_NAME, NF90_DOUBLE, dimids,&
       & o_theta_varid) )
  call check( nf90_def_var(ncid, o_sal_NAME, NF90_DOUBLE, dimids,&
       & o_sal_varid) )
  dimids = (/ o_lon_x_red_dimid, o_lat_y_red_dimid, o_ver_z_dimid ,timestep_dimid/)
  call check( nf90_def_var(ncid, o_U_NAME, NF90_DOUBLE, dimids,&
       & o_u_varid) )
  call check( nf90_def_var(ncid, o_V_NAME, NF90_DOUBLE, dimids,&
       & o_v_varid) )
  print*,'after ocean defined'
  deallocate(dimids)

  ! Assign units attributes to the pressure and temperature netCDF
  ! variables.
  call check( nf90_put_att(ncid, a_pstar_varid, UNITS, PRES_UNITS) )
  call check( nf90_put_att(ncid, a_pstar_varid, LONG_NAME, "P* Surface &
       &pressure") )
  call check( nf90_put_att(ncid, a_pstar_varid, STD_NAME, a_pstar_name))
!  call check( nf90_put_att(ncid, a_pstar_varid,"valid_min", 0.0_rk) )
  call check( nf90_put_att(ncid, a_u_varid    , UNITS, VELO_UNITS_A) )
  call check( nf90_put_att(ncid, a_u_varid    , LONG_NAME, "U Eastward Air Velocity") )
  call check( nf90_put_att(ncid, a_u_varid, STD_NAME, a_u_name))

  call check( nf90_put_att(ncid, a_v_varid    , UNITS, VELO_UNITS_A) )
  call check( nf90_put_att(ncid, a_v_varid    , LONG_NAME, "V Northward Air Velocity") )
  call check( nf90_put_att(ncid, a_v_varid, STD_NAME, a_v_name))

  call check( nf90_put_att(ncid, a_theta_varid, UNITS, TEMP_UNITS) )
  call check( nf90_put_att(ncid, a_theta_varid, LONG_NAME, "Theta Air Potentia&
       &l Temperature") )
  call check( nf90_put_att(ncid, a_theta_varid, STD_NAME, a_theta_name))
  
  call check( nf90_put_att(ncid, a_q_varid    , UNITS, HUM_UNITS) )
  call check( nf90_put_att(ncid, a_q_varid    , LONG_NAME, "Q Air Humidity") )
  call check( nf90_put_att(ncid, a_q_varid, STD_NAME, a_q_name))
  
!  call check( nf90_put_att(ncid, a_q_varid  , "valid_range", &
!       &(/0.0_rk,1.0_rk/)) )

  call check( nf90_put_att(ncid, o_u_varid    , UNITS, VELO_UNITS_O) )
  call check( nf90_put_att(ncid, o_u_varid    , LONG_NAME, "U Eastward Sea Wat&
       &er Velocity") )
  call check( nf90_put_att(ncid, o_u_varid, STD_NAME, o_u_name))
  
  call check( nf90_put_att(ncid, o_u_varid    , "missing_value", NaN) )
  call check( nf90_put_att(ncid, o_u_varid    , "_FillValue", NaN) )
  call check( nf90_put_att(ncid, o_v_varid    , UNITS, VELO_UNITS_O) )
  call check( nf90_put_att(ncid, o_v_varid    , LONG_NAME, "V Northward Sea Wa&
       &ter Velocity") )
  call check( nf90_put_att(ncid, o_v_varid, STD_NAME, o_v_name))


  call check( nf90_put_att(ncid, o_v_varid    , "missing_value", NaN) )
  call check( nf90_put_att(ncid, o_v_varid    , "_FillValue", NaN) )
  call check( nf90_put_att(ncid, o_theta_varid, UNITS,&
       & TEMP_UNITS_OCEAN) )
  call check( nf90_put_att(ncid, o_theta_varid, LONG_NAME, "Theta Sea Water Temperature") )
  call check( nf90_put_att(ncid, o_theta_varid, STD_NAME, o_theta_name))


  call check( nf90_put_att(ncid, o_theta_varid    , "missing_value", NaN)&
       & )
  call check( nf90_put_att(ncid, o_theta_varid    , "_FillValue", NaN) )
  call check( nf90_put_att(ncid, o_sal_varid  , UNITS, SAL_UNITS) )
  call check( nf90_put_att(ncid, o_sal_varid  , LONG_NAME, "S Sea Water Practi&
       &cal Salinity") )
  call check( nf90_put_att(ncid, o_sal_varid, STD_NAME, o_sal_name))

!  call check( nf90_put_att(ncid, o_sal_varid  , "valid_range", &
!       &(/0.0_rk,1.0_rk/)) )
  call check( nf90_put_att(ncid, o_sal_varid    , "missing_value", NaN)&
       & )
  call check( nf90_put_att(ncid, o_sal_varid    , "_FillValue", NaN) )
  print*,'after units of variables'
  ! End define mode.
  call check( nf90_enddef(ncid) )

  call check( nf90_put_var(ncid, a_lat_y_blue_varid, a_lat_y_blue) )
  call check( nf90_put_var(ncid, a_lon_x_blue_varid, a_lon_x_blue) )
  call check( nf90_put_var(ncid, a_lat_y_red_varid, a_lat_y_red) )
  call check( nf90_put_var(ncid, a_lon_x_red_varid, a_lon_x_red) )
  call check( nf90_put_var(ncid, a_ver_z_varid, a_ver_z) )

  call check( nf90_put_var(ncid, o_lat_y_blue_varid, o_lat_y_blue) )
  call check( nf90_put_var(ncid, o_lon_x_blue_varid, o_lon_x_blue) )
  call check( nf90_put_var(ncid, o_lat_y_red_varid, o_lat_y_red) )
  call check( nf90_put_var(ncid, o_lon_x_red_varid, o_lon_x_red) )
  call check( nf90_put_var(ncid, o_ver_z_varid, o_ver_z) )


end subroutine open_netcdf

subroutine close_netcdf(time)
  use dump_netcdf
  use netcdf
  implicit none
  integer, intent(in) :: time
  integer :: i
  
  print*,'before adding time variables'
  call check( nf90_put_var(ncid,timestep_varid,(/ (i ,i = 0,time-1) /)) )
  print*,'after adding time variable'
  ! Close the file.
  call check( nf90_close(ncid) )  

end subroutine close_netcdf


subroutine check(status)
  use netcdf, only : nf90_noerr,nf90_strerror
  implicit none
  integer, intent ( in) :: status

  if(status /= nf90_noerr) then 
     print *, trim(nf90_strerror(status))
     stop "Stopped"
  end if
end subroutine check

!!$subroutine load_bathimitry(q_depth,p_depth)
!!$  integer, parameter :: o_nxn=290,o_nyn=144      
!!$  integer, dimension(o_nxn,o_nyn),intent(out) :: q_depth,p_depth
!!$  integer, dimension(o_nyn*o_nxn) :: bog
!!$  integer, dimension(o_nxn,o_nyn) :: bath
!!$  real(kind=kind(1.0)) :: aa1,bb1,cc1,dd1
!!$  character(7) :: a1,b1,c1,d1
!!$  integer :: i,j,line
!!$  open(4,file='bath.txt',action='read',status='old')
!!$  i = 0 
!!$  do line = 1,o_nyn*o_nxn/4
!!$     read(4,'(3(A7,f7.0,4x),A7,f7.0)') a1,aa1,b1,bb1,c1,cc1,d1,dd1
!!$     !print*,a,aa1,b,bb1,c,cc1,d,dd1
!!$     bog(i+1) = nint(aa1)
!!$     bog(i+2) = nint(bb1)
!!$     bog(i+3) = nint(cc1)
!!$     bog(i+4) = nint(dd1)
!!$     i = i + 4
!!$  end do
!!$  close(4)
!!$  line = 0
!!$  do j = 1,o_nyn
!!$     do i = 1,o_nxn
!!$        line = line + 1
!!$        bath(i,j) = bog(line) 
!!$     end do
!!$  end do
!!$
!!$  DO j = 1,o_nyn
!!$     DO i = 1,o_nxn
!!$        q_depth(I,J)=0
!!$        p_depth(I,J)=bath(i,j)
!!$     ENDDO
!!$  ENDDO
!!$
!!$
!!$  !  2ND, COMPUTE NUMBER OF VERTICAL LEVELS AT EACH U,V POINT
!!$  DO j=1,o_nyn-1
!!$     DO i=1,o_nxn-1
!!$        q_depth(i,j)=MIN(p_depth(i,j),&
!!$             &          p_depth(i+1,j),p_depth(i,j+1),&
!!$             &          p_depth(i+1,j+1))
!!$     ENDDO
!!$  ENDDO
!!$
!!$  q_depth(o_nxn,:) = q_depth(2,:)
!!$
!!$end subroutine load_bathimitry

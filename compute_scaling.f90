!!! Time-stamp: <2014-06-27 12:51:48 pbrowne>

!program to run the particle filter on the model HadCM3.
!this shall hopefully have minimal changes specific to the model.
!Simon Wilson and Philip Browne 2013
!----------------------------------------------------------------

program compute_scaling_please
  use comms
  use pf_control
  use sizes
  use Qdata
  implicit none
  include 'mpif.h'
  integer :: i,j,k
  integer :: mpi_err,particle,tag
  INTEGER, DIMENSION(:), ALLOCATABLE  :: requests
  INTEGER, DIMENSION(:,:), ALLOCATABLE  :: mpi_statuses
  logical :: mpi_flag
  logical, dimension(:), ALLOCATABLE :: received
  integer :: mpi_status(MPI_STATUS_SIZE)
  real(kind=kind(1.0d0)) :: start_t,end_t
  real(kind=kind(1.0d0)), dimension(9) :: scales
  integer, parameter :: rk = kind(1.0d0)
  real(kind=rk), dimension(:,:), allocatable :: fpsi
  real(kind=rk), dimension(:,:), allocatable :: Qkgain,normaln,betan
  real(kind=rk) :: dnrm2,inc,be,rat,scal
  integer :: ii
  integer, dimension(9) :: st,sp


  st = (/   1,  7009,140161,273313,406465,539617, 997128,1454639,1884535/)
  sp = (/7008,140160,273312,406464,539616,997127,1454638,1884534,2314430/)


  write(6,'(A)') 'PF: Starting PF code'
  call flush(6)
  call initialise_mpi
  print*,'PF: setting controls'
  call set_pf_controls
  print*,'PF: configuring model'
  call configure_model
  print*,'allocating pf'
  call allocate_pf

  allocate(fpsi(state_dim,pf%count))
  allocate(Qkgain(state_dim,pf%count))
  allocate(normaln(state_dim,pf%count))
  allocate(betan(state_dim,pf%count))
  

!  print*,'stopping' !for testing
!  stop              !for testing
  call random_seed_mpi(pfrank)
  write(6,*) 'PF: starting to receive from model'
! 1st call to model to get psi
!  do k =1,pf%count
!     call receive_from_model(pf%psi(:,k),pf%particles(k))

     !lets add some random noise to the initial conditions
!     call perturb_particle(pf%psi(:,k))

!  enddo
  print*,'launching the receives'
 ! print*,pf%count
 ! print*,pf%particles
 ! print*,allocated(requests)
  tag = 1        
  allocate(requests(pf%count))
  allocate(mpi_statuses(mpi_status_size,pf%count))
  allocate(received(pf%count))

  !HADCM3 MODEL SPECIFIC...
  !let us spin the model up for one day, or 72 timesteps
  start_t = mpi_wtime() 
  do j = 1,72
     do k = 1,pf%count
        particle = pf%particles(k)
        call mpi_recv(pf%psi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     end do
     do k = 1,pf%count
        particle = pf%particles(k)
        call mpi_send(pf%psi(:,k), state_dim , MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM, mpi_err)
     end do
  end do
  end_t = mpi_wtime()
  write(6,*) 'Time for initial day run = ',end_t-start_t,' seconds.'
  pf%time=mpi_wtime()

!  if(.not. pf%gen_Q) then

  DO k = 1,pf%count
     particle = pf%particles(k)
     print*,'receiving  from ',particle
     CALL MPI_IRECV(pf%psi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM,requests(k), mpi_err)
  end DO
  print*,'receives launched'
  k = 0
  received = .false.
  do
     k = mod(k,pf%count)+1
!     print*,k
     if(.not. received(k)) then
        particle = pf%particles(k)
!        print*,particle ,'not received so testing it'
        call MPI_TEST(requests(k), mpi_flag, mpi_statuses(:,k), mpi_err)
        
        if(mpi_flag) then
!           PRINT*,'Particle filter ',pfrank,'has received initial state_v&
!                &ector over mpi from ensemble member ',particle
           received(k) = .true.
!           call perturb_particle(pf%psi(:,k))
        end if
     end if
     if(all(received)) exit
  end do
  write(6,*) 'PF: All models received in pf couple' 
  call flush(6)

  scales = 1.0d0
  Qdiag = Qdiag/4000000000.0_rk

  Qdiag(st(1):sp(1)) = Qdiag(st(1):sp(1))*1.0D0
  Qdiag(st(2):sp(2)) = Qdiag(st(2):sp(2))*1.0D0
  Qdiag(st(3):sp(3)) = Qdiag(st(3):sp(3))*1.0D0
  Qdiag(st(4):sp(4)) = Qdiag(st(4):sp(4))*1.0d0
  Qdiag(st(5):sp(5)) = Qdiag(st(5):sp(5))*1.0d0
  Qdiag(st(6):sp(6)) = Qdiag(st(6):sp(6))*1.0d0
  Qdiag(st(7):sp(7)) = Qdiag(st(7):sp(7))*1.0d0
  Qdiag(st(8):sp(8)) = Qdiag(st(8):sp(8))*1.0d5
  Qdiag(st(9):sp(9)) = Qdiag(st(9):sp(9))*1.0d5


!  if(pf%gen_data) call save_truth(pf%psi(:,1))
!  call output_from_pf
!  if(pf%gen_data) call save_truth(pf%psi(:,1))
  start_t = mpi_wtime()

  do j=1,pf%time_obs
     write(6,*) 'PF: observation counter = ',j
     do i = 1,48
        pf%timestep = pf%timestep + 1
        do k =1,pf%count
           particle = pf%particles(k)
           tag = 1
           call mpi_send(pf%psi(:,k),state_dim,MPI_DOUBLE_PRECISION&
                &,particle-1,tag,CPL_MPI_COMM,mpi_err)
        end do
        DO k = 1,pf%count
           particle = pf%particles(k)
           tag = 1
           CALL MPI_RECV(fpsi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
                particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
        END DO
        
        call NormalRandomNumbers2D(0.0D0,1.0D0,state_dim,pf%count,normaln)
        
!        Qkgain(:,1)=Qdiag
        call Qhalf(pf%count,normaln,betan)
!        print*,'change in Qdiag = ',sqrt(sum( (Qkgain(:,1)-Qdiag)**2 ))


        Qkgain = 0.0_rk

        print*,'SIZES:',sqrt(sum((pf%psi-fpsi)**2)),sqrt(sum((pf%psi)**2))&
             &,sqrt(sum((fpsi)**2)),sqrt(sum((normaln)**2))&
             &,sqrt(sum((betan)**2))


        !$omp parallel do private(particle)
        DO k = 1,pf%count
           particle = pf%particles(k)
           do ii = 1,9
              inc = dnrm2(sp(ii)-st(ii)+1,pf%psi(st(ii):sp(ii),k)&
                   &-fpsi(st(ii):sp(ii),k),1)
              be = dnrm2(sp(ii)-st(ii)+1,betan(st(ii):sp(ii),k),1)
              rat = be/inc
              print*,sqrt(sum( betan(st(ii):sp(ii),k)**2 )),sqrt(sum( (pf&
                   &%psi(st(ii):sp(ii),k)-fpsi(st(ii):sp(ii),k))**2)),Qdiag(st(ii))
              scales(ii) = rat
              !        Qdiag(st(i):sp(i)) = Qdiag(st(i):sp(i))*scal     
           end do

           call update_state(pf%psi(:,k),fpsi(:,k),Qkgain(:,k),betan(:,k))
!           call check_scaling(pf%psi(:,k),fpsi(:,k),betan(:,k),scales)
           pf%psi(:,k) = fpsi(:,k)
        end DO
        !$omp end parallel do
     end do
     
     do i = 1,24
        pf%timestep = pf%timestep + 1
        do k =1,pf%count
           particle = pf%particles(k)
           tag = 1
           call mpi_send(pf%psi(:,k),state_dim,MPI_DOUBLE_PRECISION&
                &,particle-1,tag,CPL_MPI_COMM,mpi_err)
        end do
        DO k = 1,pf%count
           particle = pf%particles(k)
           tag = 1
           CALL MPI_RECV(fpsi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
                particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
        END DO
        
        call NormalRandomNumbers2D(0.0D0,1.0D0,state_dim,pf%count,normaln)
        
        call Qhalf(pf%count,normaln,betan)
        Qkgain = 0.0_rk
                print*,'SIZES:',sqrt(sum((pf%psi-fpsi)**2)),sqrt(sum((pf%psi)**2))&
             &,sqrt(sum((fpsi)**2)),sqrt(sum((normaln)**2))&
             &,sqrt(sum((betan)**2))

        !$omp parallel do private(particle)
        DO k = 1,pf%count
           particle = pf%particles(k)
           
           do ii = 1,9
              inc = dnrm2(sp(ii)-st(ii)+1,pf%psi(st(ii):sp(ii),k)&
                   &-fpsi(st(ii):sp(ii),k),1)
              be = dnrm2(sp(ii)-st(ii)+1,betan(st(ii):sp(ii),k),1)
              rat = be/inc
              print*,sqrt(sum( betan(st(ii):sp(ii),k)**2 )),sqrt(sum( (pf&
                   &%psi(st(ii):sp(ii),k)-fpsi(st(ii):sp(ii),k))**2))
              scales(ii) = rat
              !        Qdiag(st(i):sp(i)) = Qdiag(st(i):sp(i))*scal     
           end do


           call update_state(pf%psi(:,k),fpsi(:,k),Qkgain(:,k),betan(:,k))
!           call check_scaling(pf%psi(:,k),fpsi(:,k),betan(:,k),scales)
           pf%psi(:,k) = fpsi(:,k)
        end DO
        !$omp end parallel do
     end do
     
  enddo
  write(6,*) 'PF: finished the loop - now to tidy up'
  end_t = mpi_wtime()
  call flush(6)


  tag = 1        
  DO k = 1,pf%count
     particle = pf%particles(k)
     CALL MPI_ISEND(pf%psi(:,k), state_dim , MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM, requests(k), mpi_err)
     PRINT*,'Particle filter ',pfrank,'has sent final state_vector over mpi &
          &to ensemble member ',particle
  END DO
  CALL MPI_WAITALL(pf%count,requests,mpi_statuses, mpi_err)


!  else

!     call genQ

!  end if


  
  call deallocate_data

  write(6,*) 'PF: finished deallocate_data - off to mpi_finalize'
  call flush(6)


  call MPI_Finalize(mpi_err)
  deallocate(requests)
  deallocate(mpi_statuses)
  deallocate(received)
  write(*,*) 'Program couple_pf terminated successfully.'
  write(*,*) 'Time taken in running the model = ',end_t-start_t
  write(*,*) 'Which works out at ',(end_t-start_t)/real(pf%time_obs),'seconds per day (72 timesteps)'


end program compute_scaling_please



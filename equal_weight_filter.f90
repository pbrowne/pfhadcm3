!!! Time-stamp: <2014-07-31 10:45:20 pbrowne>

subroutine equal_weight_filter
  use pf_control
  use sizes
  use random
  use comms
  implicit none
  include 'mpif.h'
  integer, parameter :: rk = kind(1.0D0)
  real(kind=rk), dimension(pf%count) :: a,b,alpha,c
  real(kind=rk), dimension(pf%nens) :: csorted
  real(kind=rk) :: cmax
  integer :: particle,i,tag,mpi_err
  real(kind=rk), dimension(obs_dim) :: y     !y, the observations
  real(kind=rk), dimension(obs_dim,pf%count) :: Hfpsi            !H(f(psi^(n-1)))
  real(kind=rk), dimension(obs_dim,pf%count) :: y_Hfpsin1  !y-H(f(psi^(n-1)))
  real(kind=rk), dimension(state_dim,pf%count) :: fpsi          !f(psi^(n-1))
  real(kind=rk), dimension(state_dim) :: psimean
  real(kind=rk), dimension(state_dim,pf%count) :: kgain         !QH^T(HQH^T+R)^(-1)(y-H(f(psi^(n-1))))
  real(kind=rk), dimension(state_dim,pf%count) :: betan         !the mixture random variable
  real(kind=rk), dimension(state_dim,pf%count) :: statev        !temporary state space vector 
  real(kind=rk), dimension(obs_dim,pf%count) :: obsv,obsvv      !temporary  obs  space vector
  real(kind=rk) :: w
  real(kind=rk), dimension(pf%count) :: e                     !e = d_i^t R^(-1) d_i
  real(kind=rk), parameter :: pi = 4.0D0*atan(1.0D0)
  logical, dimension(pf%count) :: uniform
  INTEGER, DIMENSION(MPI_STATUS_SIZE) :: mpi_status
!  real(kind=rk), dimension(pf%count) :: weight_temp
  real(kind=rk) :: ddot
!  print*,'in equal weight filter the weights are:'
!  print*,pf%weight


!I have just commented out this block so that the weights are no longer
  ! normalised here:  
!!$  weight_temp = -huge(1.0d0)
!!$  do i = 1,pf%count
!!$     weight_temp(i) = pf%weight(pf%particles(i))
!!$  end do
!!$!  print*,'temporary weight = :'
!!$!  print*,weight_temp
!!$
!!$  call mpi_allgatherv(weight_temp,pf%count,mpi_double_precision,pf%weight,gblcount&
!!$       &,gbldisp,mpi_double_precision,pf_mpi_comm,mpi_err)
!!$
!!$!  print*,'after allgather, pf%weight = '
!!$!  print*,pf%weight
!!$
!!$  pf%weight = exp(-pf%weight+maxval(pf%weight))
!!$  pf%weight = pf%weight/sum(pf%weight)
!!$  pf%weight = -log(pf%weight)
!!$!  print*,'weights should be normalised:'
!!$!  print*,pf%weight
  if(.not. pf%gen_data) then
     call get_observation_data(y)
 
!     do particle =1,pf%count
!        call send_to_model(pf%psi(:,particle),particle)
!     enddo

     DO i = 1,pf%count
        particle = pf%particles(i)
        tag = 1
        CALL MPI_SEND(pf%psi(:,i), state_dim , MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM, mpi_err)
     END DO

     DO i = 1,pf%count
        particle = pf%particles(i)
        tag = 1
        CALL MPI_RECV(fpsi(:,i), state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     END DO
     

     call H(fpsi,Hfpsi)


     do i = 1,pf%count
        particle = pf%particles(i)
        y_Hfpsin1(:,i) = y - Hfpsi(:,i)
        
        call innerHQHt_plus_R_1(y_Hfpsin1(:,i),w)
        
        c(i) = pf%weight(particle) + 0.5*w
     end do
     
     !print*,'all models received by particle filter'
     !  end do
!     print*,'allgatherv in eq'
!     print*,c
!     print*,csorted
     !print*,gblcount
     !print*,gbldisp
     !print*,pf_mpi_comm
     !print*,pf%count
     !here we can pick somehow the 80% level etc...
     !print*,'launching mpi_allgatherv pfrank =',pfrank
     call mpi_allgatherv(c,pf%count,mpi_double_precision,csorted,gblcount&
          &,gbldisp,mpi_double_precision,pf_mpi_comm,mpi_err)
!     print*,'after allgatherv',mpi_err
!     print*,csorted
     call quicksort_d(csorted,pf%nens)
     cmax = csorted(nint(pf%keep*pf%nens))
     print*,'cmax = ',cmax
  else
     
     DO i = 1,pf%count
        particle = pf%particles(i)
        tag = 1
        CALL MPI_SEND(pf%psi(:,i), state_dim , MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM, mpi_err)
        !PRINT*,'Particle filter ',pfrank,'has sent state_vector over mpi at iteratio&
        !    &n',iter,' to ensemble member ',particle
     END DO
     DO i = 1,pf%count
        particle = pf%particles(i)
        tag = 1
        CALL MPI_RECV(fpsi(:,i), state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     END DO

  end if

!  call mpi_barrier(pf_mpi_comm,mpi_err)
  psimean = 0.0_rk

  if(.not. pf%gen_data) then
     call K(y_Hfpsin1,kgain)
     call H(kgain,obsv)
     call solve_r(obsv,obsvv)
     
     do i = 1,pf%count
        a(i) = 0.5*ddot(obs_dim,obsvv(:,i),1,y_Hfpsin1(:,i),1)
     end do
     
     call innerR_1(y_Hfpsin1,e)
     
     do i = 1,pf%count
        particle = pf%particles(i)
        b(i) = 0.5*e(i) - cmax + pf%weight(particle)
        !note the plus sign in the below equation. See Ades & van Leeuwen 2012.
        alpha(i) = 1.0 + sqrt(1.0 - b(i)/a(i) + 1.0D-6)
        
!        print*,'i a  b alpha'
        print*,pfrank,i,a(i),b(i),e(i),cmax,alpha(i),pf%weight(particle),c(i)
        
        !check that alpha(i) is not NaN by seeing if it is equal to itself
        !A NaN is not equal to anything, even itself...
        if( alpha(i) .ne. alpha(i) .and. c(i) .le. cmax) then
           print*,'CAUTION : alpha computation warning!',i,a(i),b(i),pf%weight(particle)
           c(i) = cmax + 1.0d3
        end if

     end do
     
  else !if(.not. pf%gen_data) 
     kgain = 0.0_rk
     alpha = 0.0_rk
     y_Hfpsin1 = 0.0_rk
     c = -1.0_rk
     cmax = 1.0_rk
  end if !if(.not. pf%gen_data)

  call MixtureRandomNumbers2D(0.0D0,pf%nfac,pf%ufac,pf%efac,state_dim,pf%count,statev,uniform)
  call Qhalf(pf%count,statev,betan)

  do i = 1,pf%count
     if(c(i) .le. cmax) then
        particle = pf%particles(i)
        if(uniform(i)) then
           if(pf%weight(particle) .ne. pf%weight(particle)) then
              print*,'WEIGHT IS NAN BEFORE DOING STUFF'
           end if
           pf%weight(particle) = pf%weight(particle) +&
                (alpha(i)**2.0_rk - 2.0_rk*alpha(i))*a(i) + & 
                0.5_rk*e(i)
           if(pf%weight(particle) .ne. pf%weight(particle)) then
              print*,'DAMN IT ALL! Particle ',particle,' on pfrank ',pfrank
              print*,'now has weight ',pf%weight(particle)
              print*,'this is i = ',i,' with uniform draw'
              print*,'alpha(i) = ',alpha(i)
              print*,'a(i) = ',a(i)
              print*,'e(i) = ',e(i)
              print*,'stoppping now'
              stop
           end if

        else
           print*,'LOOK: DRAWN FROM THE GAUSSIAN IN MIXTURE'
           if(pf%weight(particle) .ne. pf%weight(particle)) then
              print*,'WEIGHT IS NAN BEFORE DOING GAUSSIAN STUFF'
           end if
           pf%weight(particle) = pf%weight(particle) +&
                (alpha(i)**2.0_rk - 2.0_rk*alpha(i))*a(i) + &
                0.5_rk*e(i) &
                + 2**(-real(state_dim,rk)/2.0_rk)*pi**(real(state_dim,rk)&
                &/2.0_rk)*pf%nfac*pf%ufac**(-real(state_dim,rk))*((1.0_rk&
                &-pf%efac)/pf%efac)*exp(0.5_rk*(sum(betan(:,i)*betan(:,i))))

           if(pf%weight(particle) .ne. pf%weight(particle)) then
              print*,'DAMN IT ALL! Particle ',particle,' on pfrank ',pfrank
              print*,'now has weight ',pf%weight(particle)
              print*,'this is i = ',i,' with Gaussian draw'
              print*,'alpha(i) = ',alpha(i)
              print*,'a(i) = ',a(i)
              print*,'e(i) = ',e(i)
              print*,'state_dim = ',state_dim
              print*,'-real(state_dim,rk)/2.0_rk = ',-real(state_dim,rk)&
                   &/2.0_rk
              print*,'2**(-real(state_dim,rk)/2.0_rk) = ',2**(-real(state_dim,rk)/2.0_rk)
              print*,'pi = ',pi
              print*,'pi**(real(state_dim,rk)/2.0_rk) = ',pi**(real(state_dim,rk)&
                &/2.0_rk)
              print*,'pf%nfac = ',pf%nfac
              print*,'pf%ufac = ',pf%ufac
              print*,'pf%efac = ',pf%efac
              print*,'pf%ufac**(-real(state_dim,rk)) = ',pf%ufac**(&
                   &-real(state_dim,rk))
              print*,'((1.0_rk-pf%efac)/pf%efac) = ',((1.0_rk-pf%efac)/pf&
                   &%efac)
              print*,'exp(0.5_rk*(sum(betan(:,i)*betan(:,i)))) = ',exp(0.5_rk&
                   &*(sum(betan(:,i)*betan(:,i))))
              print*,'sum(betan(:,i)*betan(:,i)) = ',sum(betan(:,i)*betan(:,i)) 
              print*,'stoppping now'
              stop
           end if

           PRINT*,'LOOK: ',pfrank,particle,i,pf%weight(particle),sum(betan(:,i)*betan(:,i)),e(i)
        end if !if(uniform)
        
        !now do the following
        !x^n = f(x^(n-1)) + alpha(i) K (y-Hf(x_i^n-1)) + beta
        call update_state(pf%psi(:,i),fpsi(:,i),alpha(i)*kgain,betan)
        psimean = psimean + pf%psi(:,i)
     else
        pf%weight(pf%particles(i)) = huge(1.0D0)
     end if
  end do
  

!=========================================================================

  if(pf%gen_data) then
     write(6,*) 'generating the data'
     call flush(6)
     call H(pf%psi,y)
     !     write(6,*) 'after H'
     !     call flush(6)
     call NormalRandomNumbers1D(0.0D0,1.0D0,obs_dim,obsv)
     call rhalf(1,obsv,obsvv)
     y = y + obsvv(:,1)
     call save_observation_data(y)
     call save_truth(pf%psi(:,1))
     call diagnostics
  else 
     if(pf%use_talagrand) call diagnostics
     !print*,'entering resample step'
     print*,'time until resample = ',mpi_wtime()-pf%time
     call flush(6)
     call resample
  end if !if(pf%gen_data)
  
  
end subroutine equal_weight_filter

program which_variables
  implicit none
  real(kind=kind(1.0)) :: aa1,bb1,cc1,dd1
  character(7) :: a1,b1,c1,d1
  integer :: i,j,line,count,k,n

  Integer, parameter :: a_nxn=96
  integer, parameter :: a_nyn=73
  integer, parameter :: a_levels=19

  Integer, parameter :: o_nxn=290
  integer, parameter :: o_nyn=144
  integer, parameter :: o_levels=20

  integer, dimension(o_nyn*o_nxn) :: bog
  integer, dimension(o_nxn,o_nyn) :: bath
  integer, dimension(o_nxn,o_nyn) :: q_depth,p_depth

  integer, dimension(a_nxn,a_nyn,a_levels) :: a_u_vec,a_v_vec&
       &,a_theta_vec,a_q_vec
  integer, dimension(a_nxn,a_nyn) :: a_pstar_vec
  integer, dimension(o_nxn,o_nyn,o_levels) :: o_u_vec,o_v_vec&
       &,o_theta_vec,o_sal_vec
  integer :: end_au,end_av,end_ap,end_at,end_aq
  integer :: end_ot,end_os,end_ou,end_ov,lat,lon,level
  character :: c
  
  level = 0
  open(4,file='bath.txt',action='read',status='old')
  i = 0
  do line = 1,o_nyn*o_nxn/4
     read(4,'(3(A7,f7.0,4x),A7,f7.0)') a1,aa1,b1,bb1,c1,cc1,d1,dd1
     !print*,a,aa1,b,bb1,c,cc1,d,dd1
     !                
     bog(i+1) = nint(aa1)
     bog(i+2) = nint(bb1)
     bog(i+3) = nint(cc1)
     bog(i+4) = nint(dd1)
     i = i + 4
  end do
  close(4)
  line = 0
  do j = 1,o_nyn
     do i = 1,o_nxn
        line = line + 1
        bath(i,j) = bog(line)
     end do
  end do
  
  DO j = 1,o_nyn
     DO i = 1,o_nxn
        q_depth(I,J)=0
        p_depth(I,J)=bath(i,j)
     ENDDO
  ENDDO
  
  
  !  2ND, COMPUTE NUMBER OF VERTICAL LEVELS AT EACH U,V POINT
  !                  
  DO j=1,o_nyn-1
     DO i=1,o_nxn-1
        q_depth(i,j)=MIN(p_depth(i,j),&
             &          p_depth(i+1,j),p_depth(i,j+1),&
             &          p_depth(i+1,j+1))
     ENDDO
  ENDDO
  
  q_depth(o_nxn,:) = q_depth(2,:)



    !let us calculate the position vectors: put everything in its
  ! place:
  count = 0
  !first is PSTAR (2d)
  do j = 1,a_nyn
     do i = 1,a_nxn
        count = count + 1
        a_pstar_vec(i,j) = count
     end do
  end do
!  print*,'a_pstar finishes on the ',count,' element'
  end_ap = count
  !second is  U
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_u_vec(i,j,k) = count
        end do
     end do
  end do
!  print*,'a_u finishes on the ',count,' element'
  end_au = count
  !third is V
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_v_vec(i,j,k) = count
        end do
     end do
  end do
!  print*,'a_v finishes on the ',count,' element'
  end_av = count
  !fouth is THETA
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_theta_vec(i,j,k) = count
        end do
     end do
  end do
!  print*,'a_theta finishes on the ',count,' element'
  end_at = count
  !fifth is Q
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_q_vec(i,j,k) = count
        end do
     end do
  end do
!  print*,'a_q finishes on the ',count,' element'
  end_aq = count
  !now we are onto the ocean:
  !sixth is THETA
!  print*,'first entry of SST is then:', count+1
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_theta_vec(i,j,k) = count
           else
              o_theta_vec(i,j,k) = 0
           end if
        end do
     end do
!        if(k .eq. 1) print*,'final entry of SST is then:',count
  end do
!  print*,'o_theta finished on the ',count,' element'
  end_ot = count
  !seventh is SALINITY
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_sal_vec(i,j,k) = count
           else
              o_sal_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
!  print*,'o_sal finished on the ',count,' element'
  end_os = count
  !eighth is U
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_u_vec(i,j,k) = count
           else
              o_u_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
!  print*,'o_u finished on the ',count,' element'
  end_ou = count
  !ninth is V
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_v_vec(i,j,k) = count
           else
              o_v_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
!  print*,'o_v finished on the ',count,' element'
  end_ov = count
  !FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT
!  print*,'FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT'





  
!do
!   write(6,'(A)',advance='no') 'Please enter the variable number (0 quits): '
read*,n
if(n .le. 0) goto 5

if(n .le. end_ap) then
!   print*,n,' is in atmosphere pstar'
   lon = mod(n,a_nxn)
   if(lon .eq. 0) lon = a_nxn
   lat = ((n - lon)/a_nxn) + 1
   write(6,'(2(A,i0))') 'Atmos Pstar: lon = ',lon,' lat = ',lat
else if(n .le. end_au) then
!   print*,n,' is in atmosphere U'
   n = n - a_nyn*a_nxn
   lon = mod(n,a_nxn)
   if(lon .eq. 0) lon = a_nxn
   lat = ((n - lon)/a_nxn) + 1
   lat = mod(lat,a_nyn)
   if(lat .eq. 0) lat = a_nyn
   level = (n - (lat-1)*a_nxn - lon)/(a_nxn*a_nyn) + 1
   write(6,'(3(A,i0))') 'Atmos U: lon = ',lon,' lat = ',lat,' level = ',level
else if(n .le. end_av) then
!   print*,n,' is in atmosphere V'
   n = n - a_nyn*a_nxn - a_levels*a_nyn*a_nxn
   lon = mod(n,a_nxn)
   if(lon .eq. 0) lon = a_nxn
   lat = ((n - lon)/a_nxn) + 1
   lat = mod(lat,a_nyn)
   if(lat .eq. 0) lat = a_nyn
   level = (n - (lat-1)*a_nxn - lon)/(a_nxn*a_nyn) + 1
   write(6,'(3(A,i0))') 'Atmos V: lon = ',lon,' lat = ',lat,' level = ',level
else if(n .le. end_at) then
!   print*,n,' is in atmosphere Theta'
   n = n - a_nyn*a_nxn - 2*a_levels*a_nyn*a_nxn
   lon = mod(n,a_nxn)
   if(lon .eq. 0) lon = a_nxn
   lat = ((n - lon)/a_nxn) + 1
   lat = mod(lat,a_nyn)
   if(lat .eq. 0) lat = a_nyn
   level = (n - (lat-1)*a_nxn - lon)/(a_nxn*a_nyn) + 1
   write(6,'(3(A,i0))') 'Atmos Theta: lon = ',lon,' lat = ',lat,' level = ',level
else if(n .le. end_aq) then
!   print*,n,' is in atmosphere Q'
   n = n - a_nyn*a_nxn - 3*a_levels*a_nyn*a_nxn
   lon = mod(n,a_nxn)
   if(lon .eq. 0) lon = a_nxn
   lat = ((n - lon)/a_nxn) + 1
   lat = mod(lat,a_nyn)
   if(lat .eq. 0) lat = a_nyn
   level = (n - (lat-1)*a_nxn - lon)/(a_nxn*a_nyn) + 1
   write(6,'(3(A,i0))') 'Atmos Q: lon = ',lon,' lat = ',lat,' level = ',level
else if(n .le. end_ot) then
!   print*,n,' is in ocean Theta'
   do k = 1,o_levels
      do j = 1,o_nyn
         do i = 1,o_nxn
            if(o_theta_vec(i,j,k) .eq. n) then
               lon = i
               lat = j
               level = k
               write(6,'(3(A,i0))') 'Ocean Theta: lon = ',lon,' lat = ',lat,' level = ',level
               goto 4
            end if
         end do
      end do
   end do
else if(n .le. end_os) then
!   print*,n,' is on ocean Salinity'
   do k = 1,o_levels
      do j = 1,o_nyn
         do i = 1,o_nxn
            if(o_sal_vec(i,j,k) .eq. n) then
               lon = i
               lat = j
               level = k
               write(6,'(3(A,i0))') 'Ocean sal: lon = ',lon,' lat = ',lat,' level = ',level
               goto 4
            end if
         end do
      end do
   end do
else if(n .le. end_ou) then
!   print*,n,' is in ocean U'
   do k = 1,o_levels
      do j = 1,o_nyn
         do i = 1,o_nxn
            if(o_u_vec(i,j,k) .eq. n) then
               lon = i
               lat = j
               level = k
               write(6,'(3(A,i0))') 'Ocean U: lon = ',lon,' lat = ',lat,' level = ',level
               goto 4
            end if
         end do
      end do
   end do
else if(n .le. end_ov) then
!   print*,n,' is in ocean V'
   do k = 1,o_levels
      do j = 1,o_nyn
         do i = 1,o_nxn
            if(o_v_vec(i,j,k) .eq. n) then
               lon = i
               lat = j
               level = k
               write(6,'(3(A,i0))') 'Ocean V: lon = ',lon,' lat = ',lat,' level = ',level
               goto 4
            end if
         end do
      end do
   end do
else
!   print*,n,' is too large to be in the state vector'
   goto 5
end if



4 continue


!print*,'longitude = ',lon
!print*,'latitude = ',lat
!print*,'level = ',level
5 continue
!end do



     

end program which_variables


program test_matrix
use Qdata
use Rdata
use sizes
use hqht_plus_r
use pf_control

implicit none
integer, parameter :: rk = kind(1.0D0)
integer :: mpi_err,i
include 'mpif.h'
real(kind=rk), allocatable, dimension(:) :: a,b,c,d,e


write(6,'(A)') 'Starting test_matrix code'
call flush(6)
call mpi_init(mpi_err)
print*,'PF: setting controls'
call set_pf_controls
print*,'PF: configuring model'
call configure_model



allocate(a(obs_dim),b(obs_dim),c(obs_dim),d(obs_dim))

do i = 1,obs_dim
   a(i) = real(i,rk)
   c(i) = real(i,rk)
end do

call R(1,a,b)

call Rhalf(1,c,d)
call Rhalf(1,d,c)

print*,'TEST THAT R = Rhalf*Rhalf'
print*,'looking at |Rx-RhalfRhalfx|_2^2'
print*,sum((b-c)**2)

deallocate(a,b,c,d)

allocate(a(state_dim),b(state_dim),c(state_dim),d(state_dim))
do i = 1,state_dim
   a(i) = real(i,rk)
   c(i) = real(i,rk)
end do

call Q(1,a,b)

call Qhalf(1,c,d)
call Qhalf(1,d,c)

print*,'TEST THAT Q = Qhalf*Qhalf'
print*,'looking at |Qx-QhalfQhalfx|_2^2'
print*,sum((b-c)**2)

deallocate(a,b,c,d)


allocate(a(obs_dim),b(obs_dim),c(obs_dim))

pf%count = 1
do i = 1,obs_dim
   a(i) = real(i,rk)
end do

call R(1,a,b)
call solve_r(b,c)
print*,'TEST THAT R^-1 R = I'
print*,'looking at |R^-1 R x - x|_2^2'
print*,sum((a-c)**2)
print*,'#################################'



b = 0.0_rk
do i = 1,HQHTRne
   if(HQHTRrow(i) .ne. HQHTRcol(i)) then
      b(HQHTRrow(i)) = b(HQHTRrow(i)) + HQHTRval(i)*a(HQHTRcol(i))
      b(HQHTRcol(i)) = b(HQHTRcol(i)) + HQHTRval(i)*a(HQHTRrow(i))
   else
      b(HQHTRrow(i)) = b(HQHTRrow(i)) + HQHTRval(i)*a(HQHTRcol(i))
   end if
end do

call R(1,a,c)
allocate(d(state_dim),e(state_dim))
call HT(a,d)
call Q(1,d,e)
call H(e,a)

c = c + a
print*,'TEST THAT HQHT+R == (HQHT + R)'
print*,'looking at |difference in HQHTR data and Q,H,R|_2^2'
print*,sum((b-c)**2)
print*,'#################################'











do i = 1,obs_dim
   a(i) = real(i,rk)
end do










!allocate(d(state_dim),e(state_dim))
call HT(a,d)
call Q(1,d,e)
call H(e,c)

call R(1,a,b)

c = c+b

call solve_hqht_plus_r(c,b)
print*,'TEST THAT [HQHT+r]^-1[HQHT+r] = I'
print*,'looking at |[HQHT+R]^-1[HQHT+R]a-a|'
print*,sum((a-b)**2)










call mpi_finalize(mpi_err)
end program test_matrix

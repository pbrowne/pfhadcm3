!!! Time-stamp: <2014-10-30 16:47:16 pbrowne>

program genQ_detrended
  use pf_control
  use sizes
  use hadcm3_config
  use comms
  use Qdata
  use hqht_plus_r
  implicit none
  include 'mpif.h'
  integer, parameter :: rk=(kind(1.0d0))

  !real(kind=rk), dimension(a_nxn,a_nyn,a_levels) :: a_u,a_v,a_theta,a_q
  !real(kind=rk), dimension(a_nxn,a_nyn) :: a_pstar
  !real(kind=rk), dimension(o_nxn,o_nyn,o_levels) :: o_u,o_v,o_theta,o_sal
  integer, dimension(a_nxn,a_nyn,a_levels) :: a_u_vec,a_v_vec,a_theta_vec,a_q_vec
  integer, dimension(a_nxn,a_nyn) :: a_pstar_vec
  integer, dimension(o_nxn,o_nyn,o_levels) :: o_u_vec,o_v_vec,o_theta_vec,o_sal_vec
  integer :: i,j,k,count,radius,nnz
  integer, parameter :: n = 426815360
  integer, allocatable, dimension(:) :: row,col
  real(kind=rk), allocatable, dimension(:) :: val
  integer :: ne,day,days
!  real(kind=rk), dimension(state_dim) :: x 
  real(kind=rk), dimension(:), allocatable :: x
  real(kind=rk), dimension(:,:), allocatable :: y
  real(kind=rk) :: start_t,end_t
  character(12) :: savename
  character(len=11) :: filename
  integer,parameter :: mean_rad=7

  obs_dim=27370
  state_dim = 2314430
  radius = 2

  allocate(x(state_dim))


  allocate(pf%mean(state_dim),row(n),col(n),val(n))
  print*,'Going to generate Q ~ the model error covariance matrix'
  start_t = mpi_wtime()
  call load_bathimitry
  end_t = mpi_wtime()
  print*,'Load bathimitry took ',end_t - start_t,' seconds'

  start_t = end_t
  !let us calculate the position vectors: put everything in its place:
  count = 0
  !first is PSTAR (2d)
  do j = 1,a_nyn
     do i = 1,a_nxn
        count = count + 1
        a_pstar_vec(i,j) = count
     end do
  end do
  print*,'a_pstar finishes on the ',count,' element'
  !second is  U
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_u_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_u finishes on the ',count,' element'
  !third is V
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_v_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_v finishes on the ',count,' element'
  !fouth is THETA
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_theta_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_theta finishes on the ',count,' element'
  !fifth is Q
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_q_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_q finishes on the ',count,' element'
  !now we are onto the ocean:
  !sixth is THETA
  print*,'first entry of SST is then:', count+1
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_theta_vec(i,j,k) = count
           else
              o_theta_vec(i,j,k) = 0
           end if
        end do
     end do
     if(k .eq. 1) print*,'final entry of SST is then:',count
  end do
  print*,'o_theta finished on the ',count,' element'
  !seventh is SALINITY
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_sal_vec(i,j,k) = count
           else
              o_sal_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_sal finished on the ',count,' element'
  !eighth is U
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_u_vec(i,j,k) = count
           else
              o_u_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_u finished on the ',count,' element'
  !ninth is V
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_v_vec(i,j,k) = count
           else
              o_v_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_v finished on the ',count,' element'
  !FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT
  print*,'FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT'
  end_t = mpi_wtime()
  print*,'and it took ',end_t-start_t,' seconds'
  !  stop
  !initialise the value thingy
  val = 0.0_rk

  !initialise the mean
  pf%mean = 0.0_rk

  !loop for 5 years:
  days = 360*5

!!$  allocate(y(days,state_dim))
!!$
!!$  do day = 1,days
!!$     start_t = mpi_wtime()
!!$     print*,'day = ',day
!!$     !update by a day, or 72 iterations:
!!$
!!$     write(savename,'(A,i5.5)') 'sample/',day
!!$     open(72,file=savename,action='read',form='unformatted')
!!$     read(72) x
!!$     close(72)
!!$
!!$     if(day-mean_rad .gt. 1) then
!!$        if(day+mean_rad .le. days) then
!!$           !all fine, no bounds issues
!!$           do j = day-mean_rad,day+mean_rad
!!$              y(j,:) = y(j,:) + x
!!$           end do
!!$        else !these go over the right hand limit
!!$           ! so go up to right hand limit
!!$           do j = day-mean_rad,days
!!$              y(j,:) = y(j,:) + x
!!$           end do
!!$           ! now loop round from start
!!$           do j = 1,day+mean_rad-days
!!$              y(j,:) = y(j,:) + x
!!$           end do
!!$        end if
!!$     else
!!$        !these go over the left hand limit
!!$        do j = 1,day+mean_rad
!!$           y(j,:) = y(j,:) + x
!!$        end do
!!$        ! loop around from end
!!$        do j = day-mean_rad+days,days
!!$           y(j,:) = y(j,:) + x
!!$        end do
!!$     end if
!!$
!!$  end do !end of the daily loop
!!$
!!$  !now divide by number of samples taken:
!!$  y = y/real(2*mean_rad+1,rk)
!!$  
!!$  pf%mean = 0.0_rk
!!$  do day = 1,days
!!$     pf%mean = pf%mean + y(day,:)
!!$
!!$     write(savename,'(A,i5.5)') 'detrnd/',day
!!$     open(72,file=savename,action='write',form='unformatted')
!!$     write(72) y(day,:)
!!$     close(72)
!!$  end do
!!$
!!$  deallocate(y)
!!$
!!$
!!$  !now subtract off the mean components:
!!$  pf%mean = pf%mean/real(days,rk)
!!$
!!$  write(savename,'(A,i5.5)') 'detrnd/',0
!!$  open(72,file=savename,action='write',form='unformatted')
!!$  write(72) pf%mean
!!$  close(72)

  write(savename,'(A,i5.5)') 'detrnd/',0
  open(72,file=savename,action='read',form='unformatted')
  read(72) pf%mean
  close(72)


  do day = 1,days
     write(savename,'(A,i5.5)') 'detrnd/',day
     open(72,file=savename,action='read',form='unformatted')
     read(72) x
     close(72)
     x = (x - pf%mean)/real(days-1,rk)
     include 'genQ_order.f90'
  end do



!  val = -1.0_rk*val
!  x = sqrt(real(days,rk)/real(days-1,rk))*pf%mean
!  include 'genQ_order.f90'
!  val = -1.0_rk*val

!  do i = 1,ne
!     if(row(i) .eq. col(i)) then
!        if(val(i) .lt. 0.0_rk) then
!           print*,'other negative detected: ',i,row(i),col(i),val(i)
!        end if
!     end if
!  end do
  


  print*,'MAXIMUM COVARIANCE VALUE = ',maxval(val(1:ne))
  print*,'MIMINUM COVARIANCE VALUE = ',minval(val(1:ne))
  print*,'MINIMUM ABSOLUTE C VALUE = ',minval(abs(val(1:ne)))




  call killQ
  


  allocate(Qdiag(state_dim))
  do i = 1,ne
     if(row(i) .eq. col(i)) then
        if(val(i) .le. 1.0d-16) then
           !val(i) = 1.0d-16
           Qdiag(row(i)) = -1.0d0
        else
           Qdiag(row(i)) = sqrt(val(i))
        end if
     end if
  end do

  !turn the covariance into a correlation
  do i = 1,ne
     if(Qdiag(row(i)) .gt. -0.5d0 .and. Qdiag(col(i)) .gt. -0.5d0) then
        val(i) = val(i)/(Qdiag(row(i))*Qdiag(col(i)))
     else
        val(i) = 0.0d0
     end if
  end do

  do i = 1,state_dim
     if(Qdiag(i) .le. 0.0d0) then
        Qdiag(i) = 1.0d-16
     end if
  end do


  i = minloc(val(1:ne),1)
  print*,'minimum value of correlation matrix is now: ',val(i)
  print*,'minimum value of correlation matrix at index ',i
  print*,'minimum value of correlation matrix at row ',row(i)
  print*,'minimum value of correlation matrix at col ',col(i)
  print*,'Diag value of ',row(i),' is ',Qdiag(row(i))
  print*,'Diag value of ',col(i),' is ',Qdiag(col(i))
  i = maxloc(val(1:ne),1)
  print*,'maximum value of correlation matrix is now: ',val(i)
  print*,'maximum value of correlation matrix at index ',i
  print*,'maximum value of correlation matrix at row ',row(i)
  print*,'maximum value of correlation matrix at col ',col(i)
  print*,'Diag value of ',row(i),' is ',Qdiag(row(i))
  print*,'Diag value of ',col(i),' is ',Qdiag(col(i))

 
  print*,'FIRST 100 VALUES OF VAL:'
  print*,'VALUE            ROW       COLUMN       i'
  do i = 1,100
     print*,val(i),row(i),col(i),i
  end do

  print*,' i            ROW           COLUMN           VAL     Qdiag(row)   Qdiag(col)'
  do i = 1,ne
     if(val(i) .gt. 1.1d0 .or. val(i) .lt. -1.1d0) then
        print*,i,row(i),col(i),val(i),Qdiag(row(i)),Qdiag(col(i))
     end if
  
     !put in final hardcore check to make sure we are between -1 and 1
     val(i) = min(max(-1.0d0,val(i)),1.0d0)
  end do


  



  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) nnz = nnz + 1
  end do

  allocate(Qval(nnz),Qrow(nnz),Qcol(nnz))
  Qne = nnz

  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) then !write(2)
        ! val(i)
        nnz = nnz + 1
        Qval(nnz) = val(i)
     end if
  end do
  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) then !write(2)
        ! row(i)
        nnz = nnz + 1
        Qrow(nnz) = row(i)
     end if
  end do
  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) then !write(2)
        ! col(i)
        nnz = nnz + 1
        Qcol(nnz) = col(i)
     end if
  end do
  !close(2)

  print*,'originally calculated ',ne,' covariances'
  print*,'reduced this to ',nnz,' covariances with a cut off of 1.0d-13'

  print*,'finished generating Q'
  !put it into the data structure
 
  deallocate(pf%mean,row,col,val)

!  call killQ

!  call calc_HQHTR

  open(2,file='Qdata.dat',action='write',status='replace',form='unformatted')

  write(2) state_dim
  write(2) Qne
  print*,'Qne = ',Qne


  do i = 1,state_dim
     write(2) Qdiag(i)
  end do
  print*,'qdiag finished writing'
  print*,'minval Qdiag = ',minval(Qdiag)
  do i = 1,Qne
     write(2) Qval(i)
  end do
  print*,'qval finished writing'
  do i = 1,Qne
     write(2) Qrow(i)
  end do
  print*,'qrow finished writing'
  do i = 1,Qne
     write(2) Qcol(i)
  end do
  print*,'qcol finished writing'
  close(2)

  print*,'FINISHED STORING Q IN Qdata.dat'

  print*,'saving Qdiag as a netcdf file'





  filename = 'Qdiag_sd.nc'
  call open_netcdf(filename)
  call put_netcdf(1,Qdiag)
  call close_netcdf(1)


  


  
  
end program genQ_detrended

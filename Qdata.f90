!!! Time-stamp: <2014-09-19 13:55:08 pbrowne>

module Qdata
use blas_sparse
use rsb
implicit none
integer :: Qn,Qne
integer, allocatable, dimension(:) :: Qrow,Qcol
real(kind=kind(1.0D0)), allocatable, dimension(:) :: Qval,Qdiag
real(kind=kind(1.0d0)) :: Qscale
integer :: Qrsb
contains
  subroutine loadQ
    use sizes
    use pf_control
!    USE blas_sparse
!    use rsb
    integer, parameter :: rk = kind(1.0d0)
    integer :: i
    integer :: res,istat
    TYPE(C_PTR),PARAMETER :: IO = C_NULL_PTR
    
    open(2,file='Qdata.dat',action='read',form='unformatted')
    
    read(2) state_dim
    print*,'state_dim =',state_dim
    Qn = state_dim
    read(2) Qne
    print*,'Qne = ',Qne
    allocate(Qrow(Qne),Qcol(Qne),Qval(Qne))
    allocate(Qdiag(Qn))
    print*,'allocation of Qrow,Qcola dn Qval done boyee'

    do i = 1,Qn
       read(2) Qdiag(i)
    end do
    print*,'qdiag finished loading'
!    Qdiag = 1.0d0 !!!! for testing only
!    print*,'sum(Qdiag**2) = ',sum(Qdiag**2)
    do i = 1,Qne
       read(2) Qval(i)
    end do
    print*,'qval finished loading'
!    Qval = 1.0d0 !!!!for testing only
    do i = 1,Qne
       read(2) Qrow(i)
    end do
    print*,'qrow finished loading'
    do i = 1,Qne
       read(2) Qcol(i)
    end do
    print*,'qcol finished loading'
    close(2)
    
    print*,'loaded Q'


    !SCALE THE DIAGONAL HERE:...........
    Qdiag = Qdiag*pf%Qscale
!    Qdiag = Qdiag/4000000000.0_rk
    !Qdiag = Qdiag/40000000.0_rk

    
    !Qdiag(st(1):sp(1)) = Qdiag(st(1):sp(1))*1.0D0
    !Qdiag(st(2):sp(2)) = Qdiag(st(2):sp(2))*1.0D0
    !Qdiag(st(3):sp(3)) = Qdiag(st(3):sp(3))*1.0D0
    !Qdiag(st(4):sp(4)) = Qdiag(st(4):sp(4))*1.0d0
    !Qdiag(st(5):sp(5)) = Qdiag(st(5):sp(5))*1.0d0
    !Qdiag(st(6):sp(6)) = Qdiag(st(6):sp(6))*1.0d0
    !Qdiag(st(7):sp(7)) = Qdiag(st(7):sp(7))*1.0d0
    !Qdiag(st(8):sp(8)) = Qdiag(st(8):sp(8))*1.0d5
    !Qdiag(st(9):sp(9)) = Qdiag(st(9):sp(9))*1.0d5

!this one
!    Qdiag(1454639:2314430) = Qdiag(1454639:2314430)*1.0d5

    !Qscale = pf%Qscale
    !Qval = Qval/sqrt(Qscale)
    !Qdiag = Qdiag/sqrt(Qscale)


!    Qval = Qval/40000000.0_rk
!    Qdiag = Qdiag/sqrt(40000000.0_rk)
    print*,'QVAL = QVAL**1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    print*,'minval(Qval) = ',minval(Qval)
    print*,'maxval(Qval) = ',maxval(Qval)
    Qval = Qval**1.0_rk

  res = rsb_lib_init(IO)
  CALL duscr_begin(state_dim,state_dim,Qrsb,res)
  CALL ussp(Qrsb,blas_upper_symmetric,istat)
  CALL ussp(Qrsb,blas_rsb_spmv_autotuning_on,istat)
  CALL duscr_insert_entries(Qrsb,Qn,(/ (1.0_rk, i=1,Qn) /),(/ (i, i=1,Qn) /),(/ (i, i=1,Qn) /),istat)
  CALL duscr_insert_entries(Qrsb,Qne,Qval,Qrow,Qcol,istat)
  CALL duscr_end(Qrsb,istat)
  
!  deallocate(Qrow,Qcol,Qdiag,Qval)

  end subroutine loadQ

  subroutine killQ
    if(allocated(Qrow)) deallocate(Qrow)
    if(allocated(Qcol)) deallocate(Qcol)
    if(allocated(Qval)) deallocate(Qval)
    if(allocated(Qdiag)) deallocate(Qdiag)
  end subroutine killQ

end module Qdata




subroutine powerC(eig)
  use Qdata
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer :: n,ldv

  integer, parameter :: nev=4,ncv = 61!31

  !real(kind=rk), dimension(n,ncv) :: v
  real(kind=rk), allocatable, dimension(:,:) :: v
  !real(kind=rk), dimension(ncv*(ncv+8)) :: workl
  real(kind=rk), ALLOCATABLE, dimension(:) :: workl
  !real(kind=rk), dimension(3*n) :: workd
  real(kind=rk), allocatable, dimension(:) :: workd
  !real(kind=rk), dimension(nev) :: d
  real(kind=rk), allocatable, dimension(:) :: d
  !real(kind=rk), dimension(n) :: resid
  real(kind=rk), allocatable, dimension(:) :: resid

  !logical, dimension(ncv) :: select
  logical, allocatable, dimension(:) :: select
  integer, dimension(11) :: iparam,ipntr
  character :: bmat
  character(2) :: which
  integer :: ido,lworkl,info,ierr,nconv,maxitr,mode,ishfts,iter,Qnev,i
  logical::  rvec
  real(kind=rk) :: tol,sigma
  real(kind=rk), intent(out) :: eig

  n = state_dim
  ldv = n

  allocate(v(n,ncv))
  allocate(workl(ncv*(ncv+8)))
  allocate(workd(3*n))
  allocate(d(nev))
  allocate(resid(n))
  allocate(select(ncv))

  bmat = 'I'
  which = 'BE'

  lworkl = ncv*(ncv+8)
  tol = 0.0d0
  info = 0
  ido = 0

  ishfts = 1
  maxitr = 300
  mode   = 1

  iparam(1) = ishfts 
  iparam(3) = maxitr 
  iparam(7) = mode 
  iter = 0

  do

     call dsaupd ( ido, bmat, n, which, nev, tol, resid, ncv, v, ldv, iparam, ipntr, workd, workl, lworkl, info )
     iter = iter + 1
     print*,iter
     call flush(6)
     if (ido .eq. -1 .or. ido .eq. 1) then
        call C(workd(ipntr(1)), workd(ipntr(2)))
     else if( info .lt. 0 ) then

        print *, ' '
        print *, ' Error with _saupd, info = ', info
        print *, ' Check documentation in _saupd '
        print *, ' '
        exit
     else

        rvec = .true.

        call dseupd ( rvec, 'All', select, d, v, ldv, sigma, &
             &        bmat, n, which, nev, tol, resid, ncv, v, ldv,& 
             &        iparam, ipntr, workd, workl, lworkl, ierr )

        if ( ierr .ne. 0) then

           print *, ' '
           print *, ' Error with _seupd, info = ', ierr
           print *, ' Check the documentation of _seupd. '
           print *, ' '
           exit
        else

           nconv =  iparam(5)

        end if
        if ( info .eq. 1) then
           print *, ' '
           print *, ' Maximum number of iterations reached.'
           print *, ' '
        else if ( info .eq. 3) then
           print *, ' ' 
           print *, ' No shifts could be applied during implicit Arnoldi update, try increasing NCV.'
           print *, ' '
        end if

        print *, ' '
        print *, ' _SDRV1 '
        print *, ' ====== '
        print *, ' '
        print *, ' Size of the matrix is ', n
        print *, ' The number of Ritz values requested is ', nev
        print *, ' The number of Arnoldi vectors generated',&
             &            ' (NCV) is ', ncv
        print *, ' What portion of the spectrum: ', which
        print *, ' The number of converged Ritz values is ', &
             &              nconv 
        print *, ' The number of Implicit Arnoldi update',&
             &            ' iterations taken is ', iparam(3)
        print *, ' The number of OP*x is ', iparam(9)
        print *, ' The convergence criterion is ', tol
        print *, ' '
        print*,'eigenvalues that are calculated are:'
        print*,d(1:nev)
        write(*,'(4(es10.4,x))') d
        
        Qnev = nev
        do i = 1,nev
           if(d(i) .le. 0.0d0) then
              Qnev = Qnev - 1
           else
              exit
           end if
        end do



        !      open(2,file='Qevd.dat',action='write',status='replace',form='unformatted&
        !           &')
        !      write(2) Qnev
        !      write(2) d(nev-Qnev+1:nev)
        !      write(2) v(:,nev-Qnev+1:nev)
        !      close(2)
        !      print*,'Data dumped to Qevd.dat'





        exit
     end if
  end do

  eig = d(1)      
end subroutine powerC

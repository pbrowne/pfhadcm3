program gen_q_images
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  real(kind=rk) :: X
  real(kind=kind(1.0)) :: aa1,bb1,cc1,dd1
  character(7) :: a1,b1,c1,d1
  integer :: i,j,line,count,k
  
  Integer, parameter :: a_nxn=96
  integer, parameter :: a_nyn=73
  integer, parameter :: a_levels=19

  Integer, parameter :: o_nxn=290
  integer, parameter :: o_nyn=144
  integer, parameter :: o_levels=20

  integer, dimension(o_nyn*o_nxn) :: bog
  integer, dimension(o_nxn,o_nyn) :: bath
  integer, dimension(o_nxn,o_nyn) :: q_depth,p_depth

  integer, dimension(a_nxn,a_nyn,a_levels) :: a_u_vec,a_v_vec&
       &,a_theta_vec,a_q_vec
  integer, dimension(a_nxn,a_nyn) :: a_pstar_vec
  integer, dimension(o_nxn,o_nyn,o_levels) :: o_u_vec,o_v_vec&
       &,o_theta_vec,o_sal_vec
  integer :: end_au,end_av,end_ap,end_at,end_aq
  integer :: end_ot,end_os,end_ou,end_ov,level
!  character :: c

  
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_pstar_theta
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_pstar_sal
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_pstar_u
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_pstar_v

  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_u_theta
  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_u_sal
  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_u_u
  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_u_v

  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_v_theta
  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_v_sal
  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_v_u
  real(kind=rk), dimension(a_nxn,a_nyn-1) :: Q_v_v

  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_theta_theta
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_theta_sal
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_theta_u
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_theta_v

  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_q_theta
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_q_sal
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_q_u
  real(kind=rk), dimension(a_nxn,a_nyn) :: Q_q_v

  integer, dimension(2314430,4) :: vec

  integer :: ii1,ii2,jj1,jj2
  integer :: n,m
  integer :: mtarget1,mtarget2,mtarget3,mtarget4
  integer :: mtarget5,mtarget6,mtarget7,mtarget8

  integer :: Qne
  integer :: Qn
  integer, allocatable, dimension(:) :: Qrow
  integer, allocatable, dimension(:) :: Qcol
  integer, allocatable, dimension(:) :: Qdiag
  real(kind=rk), allocatable, dimension(:) :: Qval


  level = 0
  open(4,file='bath.txt',action='read',status='old')
  i = 0
  do line = 1,o_nyn*o_nxn/4
     read(4,'(3(A7,f7.0,4x),A7,f7.0)') a1,aa1,b1,bb1,c1,cc1,d1,dd1
     !print*,a,aa1,b,bb1,c,cc1,d,dd1
     !                
     bog(i+1) = nint(aa1)
     bog(i+2) = nint(bb1)
     bog(i+3) = nint(cc1)
     bog(i+4) = nint(dd1)
     i = i + 4
  end do
  close(4)
  line = 0
  do j = 1,o_nyn
     do i = 1,o_nxn
        line = line + 1
        bath(i,j) = bog(line)
     end do
  end do
  
  DO j = 1,o_nyn
     DO i = 1,o_nxn
        q_depth(I,J)=0
        p_depth(I,J)=bath(i,j)
     ENDDO
  ENDDO
  
  
  !  2ND, COMPUTE NUMBER OF VERTICAL LEVELS AT EACH U,V POINT
  !                  
  DO j=1,o_nyn-1
     DO i=1,o_nxn-1
        q_depth(i,j)=MIN(p_depth(i,j),&
             &          p_depth(i+1,j),p_depth(i,j+1),&
             &          p_depth(i+1,j+1))
     ENDDO
  ENDDO
  
  q_depth(o_nxn,:) = q_depth(2,:)



    !let us calculate the position vectors: put everything in its
  ! place:
  count = 0
  !first is PSTAR (2d)
  k = 1
  do j = 1,a_nyn
     do i = 1,a_nxn
        count = count + 1
        a_pstar_vec(i,j) = count
        vec(count,1) = 1
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
     end do
  end do
  print*,'a_pstar finishes on the ',count,' element'
  end_ap = count
  !second is  U
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_u_vec(i,j,k) = count
        vec(count,1) = 2
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
        end do
     end do
  end do
  print*,'a_u finishes on the ',count,' element'
  end_au = count
  !third is V
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_v_vec(i,j,k) = count
        vec(count,1) = 3
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
        end do
     end do
  end do
  print*,'a_v finishes on the ',count,' element'
  end_av = count
  !fouth is THETA
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_theta_vec(i,j,k) = count
        vec(count,1) = 4
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
        end do
     end do
  end do
  print*,'a_theta finishes on the ',count,' element'
  end_at = count
  !fifth is Q
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_q_vec(i,j,k) = count
        vec(count,1) = 5
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
        end do
     end do
  end do
  print*,'a_q finishes on the ',count,' element'
  end_aq = count
  !now we are onto the ocean:
  !sixth is THETA
  print*,'first entry of SST is then:', count+1
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_theta_vec(i,j,k) = count
        vec(count,1) = 6
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
           else
              o_theta_vec(i,j,k) = 0
           end if
        end do
     end do
        if(k .eq. 1) print*,'final entry of SST is then:',count
  end do
  print*,'o_theta finished on the ',count,' element'
  end_ot = count
  !seventh is SALINITY
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_sal_vec(i,j,k) = count
        vec(count,1) = 7
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
           else
              o_sal_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_sal finished on the ',count,' element'
  end_os = count
  !eighth is U
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_u_vec(i,j,k) = count
        vec(count,1) = 8
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
           else
              o_u_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_u finished on the ',count,' element'
  end_ou = count
  !ninth is V
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_v_vec(i,j,k) = count
        vec(count,1) = 9
        vec(count,2) = k
        vec(count,3) = j
        vec(count,4) = i
           else
              o_v_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_v finished on the ',count,' element'
  end_ov = count
  !FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT
  print*,'FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT'



    open(2,file='Qdata.dat',action='read',form='unformatted')

    read(2) Qn
    read(2) Qne
    print*,'Qne = ',Qne
    allocate(Qrow(Qne),Qcol(Qne),Qval(Qne))
    allocate(Qdiag(Qn))
    print*,'allocation of Qrow,Qcola dn Qval done boyee'

    do i = 1,Qn
       read(2) Qdiag(i)
    end do
    print*,'qdiag finished loading'
    do i = 1,Qne
       read(2) Qval(i)
    end do
    print*,'qval finished loading'
    do i = 1,Qne
       read(2) Qrow(i)
    end do
    print*,'qrow finished loading'
    do i = 1,Qne
       read(2) Qcol(i)
    end do
    print*,'qcol finished loading'
    close(2)









  

  do count = 1,Qne
     n = Qrow(count)
     m = Qcol(count)
     if(vec(n,2) .eq. 1 .and. vec(m,2) .eq. 1) then !we are on the first level
        if(vec(n,1) .eq. 1) then !looking at pstar
                                 !on theta grid in Atmosphere
           j = vec(n,3)
           i = vec(n,4)
           
           !start with O_theta and O_sal
           jj1 = floor(146.5 - 2.0*real(j))
           jj2 = ceiling(146.5 - 2.0*real(j))
           ii1  = 3*i - 2

           X = 0.0_rk
           if(jj1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_theta_vec(ii1,jj1,1)
              mtarget3 = O_sal_vec(ii1,jj1,1)
           end if
           if(jj2 .le. 144) then
              X = X + 1.0_rk
              mtarget2 = O_theta_vec(ii1,jj2,1)
              mtarget4 = O_sal_vec(ii1,jj2,1)
           end if
           
           if(m .eq. mtarget1 .or. m .eq. mtarget2) then
              Q_pstar_theta(i,j) = Q_pstar_theta(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4) then
              Q_pstar_sal(i,j) = Q_pstar_sal(i,j) + Qval(count)/X
           end if

           !now look at O_U and O_V
           jj1 = 146 - 2*j
           if(jj1 .gt. 0) then
           ii1 = floor(3.0*real(i) - 2.5)
           ii2 = ceiling(3.0*real(i) - 2.5)
           X = 0.0_rk
           if(ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_U_vec(ii1,jj1,1)
              mtarget3 = O_v_vec(ii1,jj1,1)
           end if
           if(ii2 .le. 290) then
              X= X + 1.0_rk
              mtarget1 = O_U_vec(ii2,jj1,1)
              mtarget3 = O_v_vec(ii2,jj1,1)
           end if
           if(m .eq. mtarget1 .or. m .eq. mtarget2) then
              Q_pstar_U(i,j) = Q_pstar_U(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4) then
              Q_pstar_V(i,j) = Q_pstar_V(i,j) + Qval(count)/X
           end if
        end if
        elseif(vec(n,1) .eq. 2) then !looking at A_u
           j = vec(n,3)
           i = vec(n,4)
           if( j .le. 72) then
           
           !start with O_theta and O_sal
           jj1 = floor(145.5 - 2.0*real(j))
           jj2 = ceiling(145.5 - 2.0*real(j))
           ii1 = floor(3.0*real(i) - 0.5)
           ii2 = ceiling(3.0*real(i) - 0.5)

           X = 0.0_rk
           if(jj1 .ge. 1 .and. ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_theta_vec(ii1,jj1,1)
              mtarget3 = O_sal_vec(ii1,jj1,1)
           end if
           if(jj2 .le. 144 .and. ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget2 = O_theta_vec(ii1,jj2,1)
              mtarget4 = O_sal_vec(ii1,jj2,1)
           end if
           if(jj1 .ge. 1 .and. ii2 .le. 290) then
              X = X + 1.0_rk
              mtarget5 = O_theta_vec(ii2,jj1,1)
              mtarget7 = O_sal_vec(ii2,jj1,1)
           end if
           if(jj2 .le. 144 .and. ii2 .le. 290) then
              X = X + 1.0_rk
              mtarget6 = O_theta_vec(ii2,jj2,1)
              mtarget8 = O_sal_vec(ii2,jj2,1)
           end if

           
           if(m .eq. mtarget1 .or. m .eq. mtarget2 .or. m .eq.&
                & mtarget5 .or. m .eq. mtarget6) then
              Q_U_theta(i,j) = Q_U_theta(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4 .or. m .eq.&
                & mtarget7 .or. m .eq. mtarget8) then
              Q_U_sal(i,j) = Q_U_sal(i,j) + Qval(count)/X
           end if

           !now look at O_U and O_V
           jj1 = 145 - 2*j
           ii1 = 3*i - 1
           X = 1.0_rk
           mtarget1 = O_U_vec(ii1,jj1,1)
           mtarget3 = O_V_vec(ii1,jj1,1)

           if(m .eq. mtarget1) then
              Q_U_U(i,j) = Q_U_U(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3) then
              Q_U_V(i,j) = Q_U_V(i,j) + Qval(count)/X
           end if
        end if
        elseif(vec(n,1) .eq. 3) then !looking at A_v
           j = vec(n,3)
           i = vec(n,4)
           if(j .le. 72) then
           !start with O_theta and O_sal
           jj1 = floor(145.5 - 2.0*real(j))
           jj2 = ceiling(145.5 - 2.0*real(j))
           ii1 = floor(3.0*real(i) - 0.5)
           ii2 = ceiling(3.0*real(i) - 0.5)

           X = 0.0_rk
           if(jj1 .ge. 1 .and. ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_theta_vec(ii1,jj1,1)
              mtarget3 = O_sal_vec(ii1,jj1,1)
           end if
           if(jj2 .le. 144 .and. ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget2 = O_theta_vec(ii1,jj2,1)
              mtarget4 = O_sal_vec(ii1,jj2,1)
           end if
           if(jj1 .ge. 1 .and. ii2 .le. 290) then
              X = X + 1.0_rk
              mtarget5 = O_theta_vec(ii2,jj1,1)
              mtarget7 = O_sal_vec(ii2,jj1,1)
           end if
           if(jj2 .le. 144 .and. ii2 .le. 290) then
              X = X + 1.0_rk
              mtarget6 = O_theta_vec(ii2,jj2,1)
              mtarget8 = O_sal_vec(ii2,jj2,1)
           end if

           
           if(m .eq. mtarget1 .or. m .eq. mtarget2 .or. m .eq.&
                & mtarget5 .or. m .eq. mtarget6) then
              Q_V_theta(i,j) = Q_V_theta(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4 .or. m .eq.&
                & mtarget7 .or. m .eq. mtarget8) then
              Q_V_sal(i,j) = Q_V_sal(i,j) + Qval(count)/X
           end if

           !now look at O_U and O_V
           jj1 = 145 - 2*j
           ii1 = 3*i - 1
           X = 1.0_rk
           mtarget1 = O_U_vec(ii1,jj1,1)
           mtarget3 = O_V_vec(ii1,jj1,1)

           if(m .eq. mtarget1) then
              Q_V_U(i,j) = Q_V_U(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3) then
              Q_V_V(i,j) = Q_V_V(i,j) + Qval(count)/X
           end if
        end if
        elseif(vec(n,1) .eq. 4) then !looking at A_theta
           j = vec(n,3)
           i = vec(n,4)
           
           !start with O_theta and O_sal
           jj1 = floor(146.5 - 2.0*real(j))
           jj2 = ceiling(146.5 - 2.0*real(j))
           ii1  = 3*i - 2

           X = 0.0_rk
           if(jj1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_theta_vec(ii1,jj1,1)
              mtarget3 = O_sal_vec(ii1,jj1,1)
           end if
           if(jj2 .le. 144) then
              X = X + 1.0_rk
              mtarget2 = O_theta_vec(ii1,jj2,1)
              mtarget4 = O_sal_vec(ii1,jj2,1)
           end if
           
           if(m .eq. mtarget1 .or. m .eq. mtarget2) then
              Q_theta_theta(i,j) = Q_theta_theta(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4) then
              Q_theta_sal(i,j) = Q_theta_sal(i,j) + Qval(count)/X
           end if

           !now look at O_U and O_V
           jj1 = 146 - 2*j
           if(jj1 .gt. 0) then
           ii1 = floor(3.0*real(i) - 2.5)
           ii2 = ceiling(3.0*real(i) - 2.5)
           X = 0.0_rk
           if(ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_U_vec(ii1,jj1,1)
              mtarget3 = O_v_vec(ii1,jj1,1)
           end if
           if(ii2 .le. 290) then
              X= X + 1.0_rk
              mtarget1 = O_U_vec(ii2,jj1,1)
              mtarget3 = O_v_vec(ii2,jj1,1)
           end if
           if(m .eq. mtarget1 .or. m .eq. mtarget2) then
              Q_theta_U(i,j) = Q_theta_U(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4) then
              Q_theta_V(i,j) = Q_theta_V(i,j) + Qval(count)/X
           end if
           end if
        elseif(vec(n,1) .eq. 5) then !looking at A_q 
           j = vec(n,3)
           i = vec(n,4)
           
           !start with O_theta and O_sal
           jj1 = floor(146.5 - 2.0*real(j))
           jj2 = ceiling(146.5 - 2.0*real(j))
           ii1  = 3*i - 2

           X = 0.0_rk
           if(jj1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_theta_vec(ii1,jj1,1)
              mtarget3 = O_sal_vec(ii1,jj1,1)
           end if
           if(jj2 .le. 144) then
              X = X + 1.0_rk
              mtarget2 = O_theta_vec(ii1,jj2,1)
              mtarget4 = O_sal_vec(ii1,jj2,1)
           end if
           
           if(m .eq. mtarget1 .or. m .eq. mtarget2) then
              Q_q_theta(i,j) = Q_q_theta(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4) then
              Q_q_sal(i,j) = Q_q_sal(i,j) + Qval(count)/X
           end if

           !now look at O_U and O_V
           jj1 = 146 - 2*j
           if(jj1 .gt. 0) then
           ii1 = floor(3.0*real(i) - 2.5)
           ii2 = ceiling(3.0*real(i) - 2.5)
           X = 0.0_rk
           if(ii1 .ge. 1) then
              X = X + 1.0_rk
              mtarget1 = O_U_vec(ii1,jj1,1)
              mtarget3 = O_v_vec(ii1,jj1,1)
           end if
           if(ii2 .le. 290) then
              X= X + 1.0_rk
              mtarget1 = O_U_vec(ii2,jj1,1)
              mtarget3 = O_v_vec(ii2,jj1,1)
           end if
           if(m .eq. mtarget1 .or. m .eq. mtarget2) then
              Q_q_U(i,j) = Q_q_U(i,j) + Qval(count)/X
           elseif(m .eq. mtarget3 .or. m .eq. mtarget4) then
              Q_q_V(i,j) = Q_q_V(i,j) + Qval(count)/X
           end if
           end if
        end if !end of splitting by atmosphere variable
     end if !end of first level check
  end do


open(11,file='q_pstar_theta',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_pstar_theta(i,j)
   end do
end do
close(11)
open(11,file='q_pstar_sal',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_pstar_sal(i,j)
   end do
end do
close(11)
open(11,file='q_pstar_u',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_pstar_u(i,j)
   end do
end do
close(11)
open(11,file='q_pstar_v',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_pstar_v(i,j)
   end do
end do
close(11)


open(11,file='q_u_theta',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_u_theta(i,j)
   end do
end do
close(11)
open(11,file='q_u_sal',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_u_sal(i,j)
   end do
end do
close(11)
open(11,file='q_u_u',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_u_u(i,j)
   end do
end do
close(11)
open(11,file='q_u_v',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_u_v(i,j)
   end do
end do
close(11)

open(11,file='q_v_theta',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_v_theta(i,j)
   end do
end do
close(11)
open(11,file='q_v_sal',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_v_sal(i,j)
   end do
end do
close(11)
open(11,file='q_v_u',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_v_u(i,j)
   end do
end do
close(11)
open(11,file='q_v_v',action='write',status='replace')
do j = 1,a_nyn-1
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_v_v(i,j)
   end do
end do
close(11)

open(11,file='q_theta_theta',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_theta_theta(i,j)
   end do
end do
close(11)
open(11,file='q_theta_sal',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_theta_sal(i,j)
   end do
end do
close(11)
open(11,file='q_theta_u',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_theta_u(i,j)
   end do
end do
close(11)
open(11,file='q_theta_v',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_theta_v(i,j)
   end do
end do
close(11)

open(11,file='q_q_theta',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_q_theta(i,j)
   end do
end do
close(11)
open(11,file='q_q_sal',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_q_sal(i,j)
   end do
end do
close(11)
open(11,file='q_q_u',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_q_u(i,j)
   end do
end do
close(11)
open(11,file='q_q_v',action='write',status='replace')
do j = 1,a_nyn
   do i = 1,a_nxn
      write(11,'(es24.16)') Q_q_v(i,j)
   end do
end do
close(11)



end program gen_q_images


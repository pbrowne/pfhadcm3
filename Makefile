### Time-stamp: <2015-07-15 13:55:53 pbrowne>

#module swap PrgEnv-cray PrgEnv-pgi; module load xt-libnagfl
all: pf_couple create_ens matrix_functions detrend_Q

#detect the programming environment and set the compiler options accordingly
ProEnv = $(shell `eval `/opt/modules/3.2.6.7/bin/modulecmd bash list`` 2>&1 | grep 'PrgEnv' | cut -d - -f2 | cut -d / -f1)

ifeq ($(ProEnv),cray)
FCOPTS = -s real64
ARPACKLIB = arpack_cp
METISLIB = metis_cp
else
ifeq ($(ProEnv),intel)
FCOPTS = #-fast -Mipa=fast,inline -mp -Mprefetch -O3
ARPACKLIB = arpack_ip
METISLIB = metis_ip
else
ifeq ($(ProEnv),gnu)
FCOPTS = -fimplicit-none -Wall -fbounds-check -fopenmp -O2 -fbacktrace
#FCOPTS = -fimplicit-none -O3 -fopenmp
ARPACKLIB = arpack_gp
METISLIB = metis_gp
else
$(error Programming environment not correct. Detected as: $(ProEnv))
endif
endif
endif



FC=ftn
#FCOPTS=  -r8 -O2 -Kieee -fastsse
#FCOPTS= -r8 -I ../PrimitiveEquationPF 
LOAD=mpif90
LOADOPTS=
EA20DIR=/home/n02/n02/pbrowne/hsl/ea20/hsl_ea20-1.0.0/src/
ARPACKDIR = /home/n02/n02/pbrowne/arpack/ARPACK/
#OBJS= pf_couple.o hadcm3_config.o nudge_data.o equal_weight_filter.o quicksort.o comms.o gen_rand.o random_d.o extra.o proposal_filter.o pf_control.o data_io.o model_specific.o operator_wrappers.o
OBJS= common90.o ddeps90.o hsl_ma87d.o extra.o hadcm3_config.o histogram.o pf_couple.o Qdata.o Rdata.o hsl_ea20d_new.o equal_weight_filter.o comms.o gen_rand.o random_d.o proposal_filter.o pf_control.o data_io.o model_specific.o operator_wrappers.o quicksort.o resample.o diagnostics.o perturb_particle.o genQ.o convert_netcdf.o sparsekit.o stochastic_model.o calcHQHTR.o sir_filter.o phalf.o lambertw.o equivalent_weights_filter_zhu.o


MA87_LOC = /home/n02/n02/pbrowne/hsl/ma87/hsl_ma87-2.1.0/src/
METISDIR = /home/n02/n02/pbrowne/metis/metis-4.0.3/
#METISLIB = metis
LIB_LIST = -L$(METISDIR) -l$(METISLIB) -lnetcdf -L$(ARPACKDIR) -l$(ARPACKLIB)

SPBLASCOMP=`/home/n02/n02/pbrowne/librsb/$(ProEnv)/bin/librsb-config --I_opts`
SPBLASLINK=`/home/n02/n02/pbrowne/librsb/$(ProEnv)/bin/librsb-config --static --ldflags --extra_libs`


common90.o: $(MA87_LOC)common90.f90
	$(FC) $(FCOPTS) -c $(MA87_LOC)common90.f90

ddeps90.o: $(MA87_LOC)ddeps90.f90
	$(FC) $(FCOPTS) -c $(MA87_LOC)ddeps90.f90

hsl_ma87d.o: $(MA87_LOC)hsl_ma87d.f90 ddeps90.o common90.o
	$(FC) $(FCOPTS) -c $(MA87_LOC)hsl_ma87d.f90

convert_netcdf.o: convert_netcdf.f90
	$(FC) $(FCOPTS) -c convert_netcdf.f90

getdata.o: getdata.f90
	$(FC) $(FCOPTS) -c getdata.f90

histogram.o: histogram.f90
	$(FC) $(FCOPTS) -c histogram.f90

getmask.o: getmask.f90
	$(FC) $(FCOPTS) -c getmask.f90

calcHQHTR.o: calcHQHTR.f90
	$(FC) $(FCOPTS) -c calcHQHTR.f90 $(SPBLASCOMP)

genQ.o: genQ.f90
	$(FC) $(FCOPTS) -c genQ.f90 $(SPBLASCOMP)

genQ_detrended.o: genQ_detrended.f90
	$(FC) $(FCOPTS) -c genQ_detrended.f90 $(SPBLASCOMP)

Qdata.o: Qdata.f90 extra.o
	$(FC) $(FCOPTS) -c Qdata.f90 $(SPBLASCOMP)

Qevd.o: Qevd.f90 extra.o
	$(FC) $(FCOPTS) -c Qevd.f90

phalf.o: phalf.f90
	$(FC) $(FCOPTS) -c phalf.f90

lambertw.o: lambertw.f90
	$(FC) $(FCOPTS) -c lambertw.f90

Rdata.o: Rdata.f90
	$(FC) $(FCOPTS) -c Rdata.f90

inflate.o: inflate.f90
	$(FC) $(FCOPTS) -c inflate.f90

hsl_ea20d_new.o: $(EA20DIR)hsl_ea20d_new.f90
	$(FC) $(FCOPTS) -c $(EA20DIR)hsl_ea20d_new.f90

hadcm3_config.o: hadcm3_config.f90 
	$(FC) $(FCOPTS) -c hadcm3_config.f90 

model_specific.o: model_specific.f90 extra.o hadcm3_config.o
	$(FC) $(FCOPTS) -c model_specific.f90 $(SPBLASCOMP)

operator_wrappers.o: operator_wrappers.f90 pf_control.o extra.o
	$(FC) $(FCOPTS) -c operator_wrappers.f90 

pf_control.o: pf_control.f90 extra.o
	$(FC) $(FCOPTS) -c pf_control.f90

perturb_particle.o: perturb_particle.f90 extra.o convert_netcdf.o
	$(FC) $(FCOPTS) -c perturb_particle.f90

sparsekit.o: sparsekit.f90
	$(FC) $(FCOPTS) -c sparsekit.f90

cake:
	cake

data_io.o: data_io.f90 pf_control.o extra.o
	$(FC) $(FCOPTS) -c data_io.f90

proposal_filter.o: proposal_filter.f90 random_d.o
	$(FC) $(FCOPTS) -c proposal_filter.f90

stochastic_model.o: stochastic_model.f90 random_d.o
	$(FC) $(FCOPTS) -c stochastic_model.f90

extra.o: extra.f90
	$(FC) $(FCOPTS) -c extra.f90 

comms.o: comms.f90 extra.o pf_control.o
	$(FC) $(FCOPTS) -c comms.f90 

equal_weight_filter.o: equal_weight_filter.f90 random_d.o
	$(FC) $(FCOPTS) -c equal_weight_filter.f90 

equivalent_weights_filter_zhu.o: equivalent_weights_filter_zhu.f90 random_d.o
	$(FC) $(FCOPTS) -c equivalent_weights_filter_zhu.f90 


sir_filter.o: sir_filter.f90 random_d.o
	$(FC) $(FCOPTS) -c sir_filter.f90 


random_d.o: random_d.f90
	$(FC) $(FCOPTS) -c random_d.f90 

resample.o: resample.f90 pf_control.o random_d.o
	$(FC) $(FCOPTS) -c resample.f90

diagnostics.o: diagnostics.f90 pf_control.o extra.o
	$(FC) $(FCOPTS) -c diagnostics.f90

test_model_specific.o: test_model_specific.f90 extra.o
	$(FC) $(FCOPTS) -c test_model_specific.f90

make_histograms.o: make_histograms.f90 histogram.o
	$(FC) $(FCOPTS) -c make_histograms.f90

make_pdfs.o: make_pdfs.f90 histogram.o
	$(FC) $(FCOPTS) -c make_pdfs.f90

quicksort.o: quicksort.f90
	$(FC) $(FCOPTS) -c quicksort.f90

statistics.o: statistics.f90 extra.o
	$(FC) $(FCOPTS) -c statistics.f90 

pf_couple.o: pf_couple.f90 comms.o pf_control.o
	$(FC) $(FCOPTS) -c pf_couple.f90 

matrix_functions.o: matrix_functions.f90 comms.o pf_control.o
	$(FC) $(FCOPTS) -c matrix_functions.f90 


create_ens.o: create_ens.f90 comms.o pf_control.o
	$(FC) $(FCOPTS) -c create_ens.f90 


compute_scaling.o: compute_scaling.f90 comms.o pf_control.o
	$(FC) $(FCOPTS) -c compute_scaling.f90 

test_matrix.o: test_matrix.f90
	$(FC) $(FCOPTS) -c test_matrix.f90

gen_rand.o: gen_rand.f90 random_d.o
	$(FC) $(FCOPTS) -c gen_rand.f90

plot_Q.o: plot_Q.f90
	$(FC) $(FCOPTS) -c plot_Q.f90

obs_checker.o: obs_checker.f90
	$(FC) $(FCOPTS) -c obs_checker.f90

check_histograms.o: check_histograms.f90
	$(FC) $(FCOPTS) -c check_histograms.f90


pf_couple: $(OBJS) 
	$(FC) $(FCOPTS) $(LOADOPTS) -o pf_couple $(OBJS) $(LIB_LIST) $(SPBLASLINK)


OBJS2 = $(shell echo $(OBJS) | sed 's/pf_couple/getdata/g') 

getdata: $(OBJS2)
	$(FC) $(FCOPTS) $(LOADOPTS) -o getdata $(OBJS2) $(LIB_LIST)

OBJS3 = $(shell echo $(OBJS) | sed 's/pf_couple/getmask/g')

getmask: $(OBJS3)
	$(FC) $(FCOPTS) $(LOADOPTS) -o getmask $(OBJS3) $(LIB_LIST)

OBJS4 = $(shell echo $(OBJS) | sed 's/pf_couple/test_matrix/g')

test_matrix: $(OBJS4)
	$(FC) $(FCOPTS) $(LOADOPTS) -o test_matrix $(OBJS4) $(LIB_LIST)

OBJS5 = $(shell echo $(OBJS) | sed 's/pf_couple/compute_scaling/g')

compute_scaling: $(OBJS5)
	$(FC) $(FCOPTS) $(LOADOPTS) -o compute_scaling $(OBJS5) $(LIB_LIST) $(SPBLASLINK)

OBJS6 = $(shell echo $(OBJS) | sed 's/pf_couple/obs_checker/g')

obs_checker: $(OBJS6)
	$(FC) $(FCOPTS) $(LOADOPTS) -o check_the_obs $(OBJS6) $(LIB_LIST) $(SPBLASLINK)

OBJS7 = $(shell echo $(OBJS) | sed 's/pf_couple/create_ens/g')
create_ens: $(OBJS7)
	$(FC) $(FCOPTS) $(LOADOPTS) -o create_ens $(OBJS7) $(LIB_LIST) $(SPBLASLINK)

OBJS8 = $(shell echo $(OBJS) | sed 's/pf_couple/matrix_functions/g')
matrix_functions: $(OBJS8)
	$(FC) $(FCOPTS) $(LOADOPTS) -o matrix_functions $(OBJS8) $(LIB_LIST) $(SPBLASLINK)

OBJS9 = $(shell echo $(OBJS) | sed 's/pf_couple/genQ_detrended/g')
detrend_Q: $(OBJS9)
	$(FC) $(FCOPTS) $(LOADOPTS) -o genQ_detrended $(OBJS9) $(LIB_LIST) $(SPBLASLINK)
	$(FC) $(FCOPTS) -o viewQcorrs gen_q_images.f90



histogram: histogram.o make_histograms.o quicksort.o convert_netcdf.o pf_control.o extra.o hadcm3_config.o
	$(FC) $(FCOPTS) -o compute_histograms histogram.o make_histograms.o quicksort.o convert_netcdf.o pf_control.o extra.o hadcm3_config.o

pdfs: histogram.o pf_control.o extra.o make_pdfs.o
	$(FC) $(FCOPTS) -o compute_pdfs histogram.o pf_control.o extra.o make_pdfs.o

test_model_specific: test_model_specific.o extra.o model_specific.o
	$(FC) $(FCOPTS) $(LOADOPTS) -o test_model_specific test_model_specific.o extra.o model_specific.o $(LIB_LIST)

check_histograms: check_histograms.o histogram.o quicksort.o
	$(FC) $(FCOPTS) -o check_histograms check_histograms.o histogram.o quicksort.o

plot_Q: plot_Q.o convert_netcdf.o pf_control.o extra.o histogram.o hadcm3_config.o
	$(FC) $(FCOPTS) $(LOADOPTS) -o plot_Q plot_Q.o convert_netcdf.o pf_control.o extra.o histogram.o hadcm3_config.o $(LIB_LIST)


clean:
	rm -f *.o *.oo *.mod pf_couple


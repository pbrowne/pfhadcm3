program obs_checker
use pf_control
use sizes

integer :: i,j
real(kind=kind(1.0d0)), dimension(27370) :: y

obs_dim = 27370

state_dim = 2314430

pf%time_bwn_obs = 72
do i = 72,720,72
   pf%timestep = i
   print*,'################ ',pf%timestep
   
   call get_observation_data(y)

   print*,'minval(y) = ',minval(y)
   print*,'maxval(y) = ',maxval(y)
   
   do j = 1,10
      print*,y(j)
   end do

   do j = 1,27370
      if(y(j) .ne. y(j)) then
         print*,j,y(j)
      end if
   end do
end do


end program obs_checker

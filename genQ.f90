!!! Time-stamp: <2014-09-17 10:57:49 pbrowne>

subroutine genQ
  use sizes
  use pf_control
  use hadcm3_config
  use comms
  use Qdata
  use hqht_plus_r
  implicit none
  include 'mpif.h'
  integer, parameter :: rk=(kind(1.0d0))
  !real(kind=rk), dimension(a_nxn,a_nyn,a_levels) :: a_u,a_v,a_theta,a_q
  !real(kind=rk), dimension(a_nxn,a_nyn) :: a_pstar
  !real(kind=rk), dimension(o_nxn,o_nyn,o_levels) :: o_u,o_v,o_theta,o_sal
  integer, dimension(a_nxn,a_nyn,a_levels) :: a_u_vec,a_v_vec,a_theta_vec,a_q_vec
  integer, dimension(a_nxn,a_nyn) :: a_pstar_vec
  integer, dimension(o_nxn,o_nyn,o_levels) :: o_u_vec,o_v_vec,o_theta_vec,o_sal_vec
  integer :: i,j,k,count,radius,nnz
  integer, parameter :: n = 426815360
  integer, allocatable, dimension(:) :: row,col
  real(kind=rk), allocatable, dimension(:) :: val
  integer :: ne,iter,day,tag,mpi_err,days
  integer :: mpi_status(MPI_STATUS_SIZE)
  real(kind=rk), dimension(state_dim) :: x 
  real(kind=rk) :: start_t,end_t
  character(12) :: savename

  allocate(pf%mean(state_dim),row(n),col(n),val(n))
  print*,'Going to generate Q ~ the model error covariance matrix'
  start_t = mpi_wtime()
  call load_bathimitry
  end_t = mpi_wtime()
  print*,'Load bathimitry took ',end_t - start_t,' seconds'

  start_t = end_t
  !let us calculate the position vectors: put everything in its place:
  count = 0
  !first is PSTAR (2d)
  do j = 1,a_nyn
     do i = 1,a_nxn
        count = count + 1
        a_pstar_vec(i,j) = count
     end do
  end do
  print*,'a_pstar finishes on the ',count,' element'
  !second is  U
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_u_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_u finishes on the ',count,' element'
  !third is V
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_v_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_v finishes on the ',count,' element'
  !fouth is THETA
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_theta_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_theta finishes on the ',count,' element'
  !fifth is Q
  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           count = count + 1
           a_q_vec(i,j,k) = count
        end do
     end do
  end do
  print*,'a_q finishes on the ',count,' element'
  !now we are onto the ocean:
  !sixth is THETA
  print*,'first entry of SST is then:', count+1
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_theta_vec(i,j,k) = count
           else
              o_theta_vec(i,j,k) = 0
           end if
        end do
     end do
     if(k .eq. 1) print*,'final entry of SST is then:',count
  end do
  print*,'o_theta finished on the ',count,' element'
  !seventh is SALINITY
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. p_depth(i,j)) then
              count = count + 1
              o_sal_vec(i,j,k) = count
           else
              o_sal_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_sal finished on the ',count,' element'
  !eighth is U
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_u_vec(i,j,k) = count
           else
              o_u_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_u finished on the ',count,' element'
  !ninth is V
  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn
           if(k .le. q_depth(i,j)) then
              count = count + 1
              o_v_vec(i,j,k) = count
           else
              o_v_vec(i,j,k) = 0
           end if
        end do
     end do
  end do
  print*,'o_v finished on the ',count,' element'
  !FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT
  print*,'FINISHED COMPUTING THE VECTOR POSITIONS OF EACH COMPONENT'
  end_t = mpi_wtime()
  print*,'and it took ',end_t-start_t,' seconds'
  !  stop
  !initialise the value thingy
  val = 0.0_rk

  !initialise the mean
  pf%mean = 0.0_rk
  tag = 1
  print*,'first recv...'
!  print*,state_dim,MPI_DOUBLE_PRECISION, &
!       0, tag, CPL_MPI_COMM,mpi_status, mpi_err
  call mpi_recv(pf%psi(:,1), state_dim, MPI_DOUBLE_PRECISION, &
       0, tag, CPL_MPI_COMM,mpi_status, mpi_err)
  print*,'.............recvd'

  !loop for 5 years:
  days = pf%time_obs
  do day = 1,days
     start_t = mpi_wtime()
     print*,'day = ',day
     !update by a day, or 72 iterations:
     do iter = 1,72
!        print*,state_dim , MPI_DOUBLE_PRECISION, &
!             0, tag, CPL_MPI_COMM, mpi_err
        call mpi_send(pf%psi(:,1), state_dim , MPI_DOUBLE_PRECISION, &
             0, tag, CPL_MPI_COMM, mpi_err)
!        print*,state_dim , MPI_DOUBLE_PRECISION, &
!             0, tag, CPL_MPI_COMM, mpi_err
!                print*,'iter = ',iter
!                print*,mpi_status
        call mpi_recv(pf%psi(:,1), state_dim, MPI_DOUBLE_PRECISION, &
             0, tag, CPL_MPI_COMM,mpi_status, mpi_err)
!        print*,state_dim , MPI_DOUBLE_PRECISION, &
!             0, tag, CPL_MPI_COMM, mpi_status,mpi_err
!        print*,'after recv'
     end do
     end_t = mpi_wtime()
     print*,'72 model runs on ',day,' took ',end_t-start_t,' seconds'
     !update the mean:
     pf%mean = pf%mean + pf%psi(:,1)

     x = pf%psi(:,1)

     write(savename,'(A,i5.5)') 'sample/',day
     open(72,file=savename,action='write',form='unformatted',status='replace')
     write(72) x
     close(72)

     radius = 2
     start_t = mpi_wtime()
!     print*,'going into genQ_order'
!     include 'genQ_order.f90'
!     print*,'finished genQ_order at the end of the day with ne = ',ne
     end_t = mpi_wtime()
     print*,'generating Q on ',day,' took ',end_t-start_t,' seconds.'


  end do !end of the daily loop



  !now we should divide the value by N-1 = 360*5-1...
!  val = val/real(days-1,rk)


  !now subtract off the mean components:
  pf%mean = pf%mean/real(days,rk)

  do day = 1,days
     write(savename,'(A,i5.5)') 'sample/',day
     open(72,file=savename,action='read',form='unformatted')
     read(72) x
     close(72)
     x = (x - pf%mean)/real(days-1,rk)
     include 'genQ_order.f90'
  end do



!  val = -1.0_rk*val
!  x = sqrt(real(days,rk)/real(days-1,rk))*pf%mean
!  include 'genQ_order.f90'
!  val = -1.0_rk*val

!  do i = 1,ne
!     if(row(i) .eq. col(i)) then
!        if(val(i) .lt. 0.0_rk) then
!           print*,'other negative detected: ',i,row(i),col(i),val(i)
!        end if
!     end if
!  end do
  


  print*,'MAXIMUM COVARIANCE VALUE = ',maxval(val(1:ne))
  print*,'MIMINUM COVARIANCE VALUE = ',minval(val(1:ne))
  print*,'MINIMUM ABSOLUTE C VALUE = ',minval(abs(val(1:ne)))







  !do the final mpi send to end model cleanly
  call mpi_send(pf%psi(:,1), state_dim , MPI_DOUBLE_PRECISION, &
       0, tag, CPL_MPI_COMM, mpi_err)


  !let us check the diagonal terms:
  !  print*,'checking diagonal entries'
  !  do i = 1,ne
  !     if(col(i) .eq. row(i)) then
  !        if(abs(val(i)) .lt. 1.0D-13) then
  !           print*,'diagonal term at i = ',i,' in row ',row(i),' is ',val(i)
  !        end if
  !     end if
  !  end do
  !  print*,'diagonal entries checked'



  !now let us scale Q
!  val = val/1.0D4




  call killQ
  


  allocate(Qdiag(state_dim))
  do i = 1,ne
     if(row(i) .eq. col(i)) then
        if(val(i) .le. 1.0d-16) then
           !val(i) = 1.0d-16
           Qdiag(row(i)) = -1.0d0
        else
           Qdiag(row(i)) = sqrt(val(i))
        end if
     end if
  end do

  !turn the covariance into a correlation
  do i = 1,ne
     if(Qdiag(row(i)) .gt. -0.5d0 .and. Qdiag(col(i)) .gt. -0.5d0) then
        val(i) = val(i)/(Qdiag(row(i))*Qdiag(col(i)))
     else
        val(i) = 0.0d0
     end if
  end do

  do i = 1,state_dim
     if(Qdiag(i) .le. 0.0d0) then
        Qdiag(i) = 1.0d-16
     end if
  end do


  i = minloc(val(1:ne),1)
  print*,'minimum value of correlation matrix is now: ',val(i)
  print*,'minimum value of correlation matrix at index ',i
  print*,'minimum value of correlation matrix at row ',row(i)
  print*,'minimum value of correlation matrix at col ',col(i)
  print*,'Diag value of ',row(i),' is ',Qdiag(row(i))
  print*,'Diag value of ',col(i),' is ',Qdiag(col(i))
  i = maxloc(val(1:ne),1)
  print*,'maximum value of correlation matrix is now: ',val(i)
  print*,'maximum value of correlation matrix at index ',i
  print*,'maximum value of correlation matrix at row ',row(i)
  print*,'maximum value of correlation matrix at col ',col(i)
  print*,'Diag value of ',row(i),' is ',Qdiag(row(i))
  print*,'Diag value of ',col(i),' is ',Qdiag(col(i))

 
  print*,'FIRST 100 VALUES OF VAL:'
  print*,'VALUE            ROW       COLUMN       i'
  do i = 1,100
     print*,val(i),row(i),col(i),i
  end do

  print*,' i            ROW           COLUMN           VAL     Qdiag(row)   Qdiag(col)'
  do i = 1,ne
     if(val(i) .gt. 1.1d0 .or. val(i) .lt. -1.1d0) then
        print*,i,row(i),col(i),val(i),Qdiag(row(i)),Qdiag(col(i))
     end if
  
     !put in final hardcore check to make sure we are between -1 and 1
     val(i) = min(max(-1.0d0,val(i)),1.0d0)
  end do


  



  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) nnz = nnz + 1
  end do

  allocate(Qval(nnz),Qrow(nnz),Qcol(nnz))
  Qne = nnz

  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) then !write(2)
        ! val(i)
        nnz = nnz + 1
        Qval(nnz) = val(i)
     end if
  end do
  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) then !write(2)
        ! row(i)
        nnz = nnz + 1
        Qrow(nnz) = row(i)
     end if
  end do
  nnz = 0
  do i = 1,ne
     if(row(i) .ne. col(i) .and. abs(val(i)) .gt. 1.0D-13) then !write(2)
        ! col(i)
        nnz = nnz + 1
        Qcol(nnz) = col(i)
     end if
  end do
  !close(2)
  print*,'finished generating Q'
  !put it into the data structure
 
  deallocate(pf%mean,row,col,val)

!  call killQ

!  call calc_HQHTR

  open(2,file='Qdata.dat',action='write',status='replace',form='unformatted')

  write(2) state_dim
  write(2) Qne
  print*,'Qne = ',Qne


  do i = 1,state_dim
     write(2) Qdiag(i)
  end do
  print*,'qdiag finished writing'
  print*,'minval Qdiag = ',minval(Qdiag)
  do i = 1,Qne
     write(2) Qval(i)
  end do
  print*,'qval finished writing'
  do i = 1,Qne
     write(2) Qrow(i)
  end do
  print*,'qrow finished writing'
  do i = 1,Qne
     write(2) Qcol(i)
  end do
  print*,'qcol finished writing'
  close(2)

  print*,'FINISHED STORING Q IN Qdata.dat'


end subroutine genQ


subroutine genQ_at2d_at2d(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn), intent(in) :: field1,field2
  integer :: i,j,ii,jj

  do j = 1,a_nyn
     do i = 1,a_nxn
        do jj = max(1,j-radius),min(a_nyn,j+radius)
           do ii = max(1,i-radius),min(a_nxn,i+radius)
              if(field1(i,j) .le. field2(ii,jj)) then
                 ne = ne + 1
                 row(ne) = field1(i,j)
                 col(ne) = field2(ii,jj)
                 val(ne) = val(ne)+x(row(ne))*x(col(ne))
              end if
           end do
           if(i .le. radius) then
              do ii = a_nxn-radius+i,a_nxn
                 if(field1(i,j) .le. field2(ii,jj)) then
                    ne = ne + 1
                    row(ne) = field1(i,j)
                    col(ne) = field2(ii,jj)
                    val(ne) = val(ne)+x(row(ne))*x(col(ne))
                 end if
              end do
           end if
           if(i+radius .gt. a_nxn) then
              do ii = 1,i+radius-a_nxn
                 if(field1(i,j) .le. field2(ii,jj)) then
                    ne = ne + 1
                    row(ne) = field1(i,j)
                    col(ne) = field2(ii,jj)
                    val(ne) = val(ne)+x(row(ne))*x(col(ne))
                 end if
              end do
           end if
        end do
     end do
  end do
end subroutine genQ_at2d_at2d


subroutine genQ_at2d_atq(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn), intent(in) :: field1
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field2
  integer :: i,j,ii,jj,kk

  do j = 1,a_nyn
     do i = 1,a_nxn
        do kk = 1,1+radius
           do jj = max(1,j-radius),min(a_nyn,j+radius)
              do ii = max(1,i-radius),min(a_nxn,i+radius)
                 if(field1(i,j) .le. field2(ii,jj,kk)) then
                    ne = ne + 1
                    row(ne) = field1(i,j)
                    col(ne) = field2(ii,jj,kk)
                    val(ne) = val(ne)+x(row(ne))*x(col(ne))
                 end if
              end do

              if(i .le. radius) then
                 do ii = a_nxn-radius+i,a_nxn
                    if(field1(i,j) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do
              end if
              if(i+radius .gt. a_nxn) then
                 do ii = 1,i+radius-a_nxn
                    if(field1(i,j) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do
              end if

           end do
        end do
     end do
  end do
end subroutine genQ_at2d_atq

subroutine genQ_at2d_atu(ne,row,col,val,n,radius,field1,field2,x)
  !field1 is offset of field2

  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn), intent(in) :: field1
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field2
  integer :: i,j,ii,jj,kk

  do j = 1,a_nyn
     do i = 1,a_nxn
        do kk = 1,1+radius
           !jj is now offset: so go from j-radius
           !up to j+radius-1
           !stop at a_nyn-1 as there are only that many in field2
           do jj = max(1,j-radius),min(a_nyn-1,j+radius-1)
              !ii is offset so go from i-radius to i+radius-1
              !stop at a_nxn as there are enough in that direction
              do ii = max(1,i-radius),min(a_nxn,i+radius-1)
                 if(field1(i,j) .le. field2(ii,jj,kk)) then
                    ne = ne + 1
                    row(ne) = field1(i,j)
                    col(ne) = field2(ii,jj,kk)
                    val(ne) = val(ne)+x(row(ne))*x(col(ne))
                 end if
              end do

              if(i .le. radius) then
                 do ii = a_nxn-radius+i,a_nxn
                    if(field1(i,j) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do
              end if
              if(i+radius-1 .gt. a_nxn) then
                 do ii = 1,i+radius-a_nxn-1
                    if(field1(i,j) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do
              end if

           end do
        end do
     end do
  end do
end subroutine genQ_at2d_atu

subroutine genQ_atq_atq(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,a_levels
     do j = 1,a_nyn
        do i = 1,a_nxn
           do kk = max(1,k-radius),min(a_levels,k+radius)
              do jj = max(1,j-radius),min(a_nyn,j+radius)
                 do ii = max(1,i-radius),min(a_nxn,i+radius)
                    if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j,k)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do

                 if(i .lt. radius) then
                    do ii = a_nxn+1-radius+i,a_nxn
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end do
                 end if
                 if(i+radius .gt. a_nxn) then
                    do ii = 1,i+radius-a_nxn-1
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
  end do
end subroutine genQ_atq_atq



subroutine genQ_atu_atu(ne,row,col,val,n,radius,field1,field2,x)
  !both fields on u grid
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,a_levels
     do j = 1,a_nyn-1
        do i = 1,a_nxn
           do kk = max(1,k-radius),min(a_levels,k+radius)
              do jj = max(1,j-radius),min(a_nyn-1,j+radius)
                 do ii = max(1,i-radius),min(a_nxn,i+radius)
                    if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j,k)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do

                 if(i .le. radius) then
                    do ii = a_nxn-radius+i,a_nxn
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end do
                 end if
                 if(i+radius .gt. a_nxn) then
                    do ii = 1,i+radius-a_nxn
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
     !now do the top polar values
     !these are blank, so lets set them to be -1 each time we pass...
     j = a_nyn
     do i = 1,a_nxn
        if(field1(i,j,k) .eq. field2(i,j,k)) then
           ne = ne + 1
           row(ne) = field1(i,j,k)
           col(ne) = field2(i,j,k)
           val(ne) = -1.0D-16
        end if
     end do
  end do
end subroutine genQ_atu_atu

subroutine genQ_atu_atq(ne,row,col,val,n,radius,field1,field2,x)
  !both fields on u grid
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,a_levels
     !first entry is on u grid so only a_nyn-1 points in that direction
     do j = 1,a_nyn-1
        do i = 1,a_nxn
           do kk = max(1,k-radius),min(a_levels,k+radius)
              !jj are offset so plus 1 on the lower bound
              do jj = max(1,j-radius+1),min(a_nyn,j+radius)
                 !ii are offset so plus 1 on the lower bound
                 do ii = max(1,i-radius+1),min(a_nxn,i+radius)
                    if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j,k)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end do

                 !use lt here because of offset
                 if(i .lt. radius) then
                    do ii = a_nxn+1-radius+i,a_nxn
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end do
                 end if
                 if(i+radius .gt. a_nxn) then
                    do ii = 1,i+radius-a_nxn
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
     !as we are using different fields, dont worry about the
     !diagonal term being included here
  end do
end subroutine genQ_atu_atq

subroutine genQ_ocq_ocq(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn-2
           do kk = max(1,k-radius),min(o_levels,k+radius)
              do jj = max(1,j-radius),min(o_nyn,j+radius)
                 do ii = max(1,i-radius),min(o_nxn-2,i+radius)
                    if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                         & .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .lt. radius) then
                    do ii = o_nxn-2+1-radius+i,o_nxn-2
                       if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                            & .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if
                 if(i+radius .gt. o_nxn-2) then
                    do ii = 1,i+radius-(o_nxn-2)-1
                       if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                            & .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
        do i = o_nxn-1,o_nxn
           if(field1(i,j,k) .gt. 0) then
              if(field1(i,j,k) .eq. field2(i,j,k)) then
                 ne = ne + 1
                 row(ne) = field1(i,j,k)
                 col(ne) = field2(i,j,k)
                 val(ne) = val(ne)+x(row(ne))*x(col(ne))
              end if
           end if
        end do
     end do
  end do
end subroutine genQ_ocq_ocq

subroutine genQ_ocu_ocu(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,o_levels
     do j = 1,o_nyn-1
        do i = 1,o_nxn-2
           do kk = max(1,k-radius),min(o_levels,k+radius)
              do jj = max(1,j-radius),min(o_nyn-1,j+radius)
                 do ii = max(1,i-radius),min(o_nxn-2,i+radius)
                    if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                         & .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .lt. radius) then
                    do ii = o_nxn-2+1-radius+i,o_nxn-2
                       if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                            & .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if
                 if(i+radius .gt. o_nxn-2) then
                    do ii = 1,i+radius-(o_nxn-2)-1
                       if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                            & .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
        do i = o_nxn-1,o_nxn
           if(field1(i,j,k) .gt. 0) then
              if(field1(i,j,k) .eq. field2(i,j,k)) then
                 ne = ne + 1
                 row(ne) = field1(i,j,k)
                 col(ne) = field2(i,j,k)
                 val(ne) = -1.0d-16
              end if
           end if
        end do
     end do

     j = o_nyn
     do i = 1,o_nxn
        if(field1(i,j,k) .gt. 0) then
           if(field1(i,j,k) .eq. field2(i,j,k)) then
              ne = ne + 1
              row(ne) = field1(i,j,k)
              col(ne) = field2(i,j,k)
              val(ne) = val(ne)+x(row(ne))*x(col(ne))
           end if
        end if
     end do
  end do

end subroutine genQ_ocu_ocu


subroutine genQ_ocq_ocu(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,o_levels
     do j = 1,o_nyn
        do i = 1,o_nxn-2
           do kk = max(1,k-radius),min(o_levels,k+radius)
              do jj = max(1,j-radius),min(o_nyn,j+radius-1)
                 do ii = max(1,i-radius),min(o_nxn-2,i+radius-1)
                    if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                         & .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .lt. radius) then
                    do ii = o_nxn-2-radius+i,o_nxn-2
                       if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                            & .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if
                 if(i+radius .gt. o_nxn-2) then
                    do ii = 1,i+radius-(o_nxn-2)-1
                       if(field1(i,j,k) .gt. 0 .and. field2(ii,jj,kk)&
                            & .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
  end do
end subroutine genQ_ocq_ocu


subroutine genQ_atq_ocq(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,radius
     do j = 1,a_nyn
        do i = 1,a_nxn
           do kk = 1,(radius+1-k)
              do jj = (o_nyn+1)-min(2*j-1+radius-1,o_nyn),(o_nyn+1)-max(2*j-1-radius,1)                 
                 do ii = max(3*i-2-radius,1),min(3*i-2+radius,o_nxn)
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .eq. 1) then
                    do ii = o_nxn-2+1-radius,o_nxn-2-1
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

                 if(i .eq. a_nxn) then
                    do ii = 1,radius
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
  end do
end subroutine genQ_atq_ocq


subroutine genQ_atq_ocu(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,radius
     do j = 1,a_nyn
        do i = 1,a_nxn
           do kk = 1,(radius+1-k)
              do jj = (o_nyn+1)-min(2*j-2&
                   &+radius,o_nyn),(o_nyn+1)-max(2*j-2-radius,1)

                 do ii = max(3*i-3-radius+1,1),min(3*i-2+radius-1,o_nxn)
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .eq. 1) then
                    do ii = o_nxn-2+1-radius,o_nxn-2-1
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

                 if(i .eq. a_nxn) then
                    do ii = 1,radius
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
  end do
end subroutine genQ_atq_ocu

subroutine genQ_atu_ocq(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,radius
     do j = 1,a_nyn-1
        do i = 1,a_nxn
           do kk = 1,(radius+1-k)
              do jj = (o_nyn+1)-min(2*j&
                   &+radius-1,o_nyn),(o_nyn+1)-max(2*j-1-radius+1,1)

                 do ii = max(3*i-1-radius+1,1),min(3*i+radius-1,o_nxn)
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .eq. 1) then
                    do ii = o_nxn-2+1-radius,o_nxn-2-1
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

                 if(i .eq. a_nxn) then
                    do ii = 1,radius
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
  end do
end subroutine genQ_atu_ocq


subroutine genQ_atu_ocu(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn,a_levels), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,k,ii,jj,kk

  do k = 1,radius
     do j = 1,a_nyn-1
        do i = 1,a_nxn
           do kk = 1,(radius+1-k)
              do jj = (o_nyn+1)-min(2*j-1&
                   &+radius,o_nyn),(o_nyn+1)-max(2*j-1-radius,1)

                 do ii = max(3*i-1-radius,1),min(3*i-1+radius,o_nxn)
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j,k)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do

                 if(i .eq. 1) then
                    do ii = o_nxn-2+1-radius,o_nxn-2-1
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

                 if(i .eq. a_nxn) then
                    do ii = 1,radius
                       if(field2(ii,jj,kk) .gt. 0) then
                          if(field1(i,j,k) .le. field2(ii,jj,kk)) then
                             ne = ne + 1
                             row(ne) = field1(i,j,k)
                             col(ne) = field2(ii,jj,kk)
                             val(ne) = val(ne)+x(row(ne))*x(col(ne))
                          end if
                       end if
                    end do
                 end if

              end do
           end do
        end do
     end do
  end do
end subroutine genQ_atu_ocu


subroutine genQ_at2d_ocq(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,ii,jj,kk

  do j = 1,a_nyn
     do i = 1,a_nxn
        do kk = 1,radius
           do jj = (o_nyn+1)-min(2*j-1+radius-1&
                &,o_nyn),(o_nyn+1)-max(2*j-1-radius,1)

              do ii = max(3*i-2-radius,1),min(3*i-2+radius,o_nxn)
                 if(field2(ii,jj,kk) .gt. 0) then
                    if(field1(i,j) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end if
              end do

              if(i .eq. 1) then
                 do ii = o_nxn-2+1-radius,o_nxn-2-1
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do
              end if

              if(i .eq. a_nxn) then
                 do ii = 1,radius
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do
              end if

           end do
        end do
     end do
  end do

end subroutine genQ_at2d_ocq


subroutine genQ_at2d_ocu(ne,row,col,val,n,radius,field1,field2,x)
  use hadcm3_config
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer, intent(inout) :: ne
  integer, intent(in) :: n,radius
  integer, dimension(n), intent(inout) :: row,col
  real(kind=rk), dimension(n), intent(inout) :: val
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer, dimension(a_nxn,a_nyn), intent(in) :: field1
  integer, dimension(o_nxn,o_nyn,o_levels), intent(in) :: field2
  integer :: i,j,ii,jj,kk

  do j = 1,a_nyn
     do i = 1,a_nxn
        do kk = 1,radius
           do jj = (o_nyn+1)-min(2*j-2+radius&
                &,o_nyn),(o_nyn+1)-max(2*j-2-radius,1)

              do ii = max(3*i-3-radius+1,1),min(3*i-2+radius-1,o_nxn)
                 if(field2(ii,jj,kk) .gt. 0) then
                    if(field1(i,j) .le. field2(ii,jj,kk)) then
                       ne = ne + 1
                       row(ne) = field1(i,j)
                       col(ne) = field2(ii,jj,kk)
                       val(ne) = val(ne)+x(row(ne))*x(col(ne))
                    end if
                 end if
              end do

              if(i .eq. 1) then
                 do ii = o_nxn-2+1-radius,o_nxn-2-1
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do
              end if

              if(i .eq. a_nxn) then
                 do ii = 1,radius
                    if(field2(ii,jj,kk) .gt. 0) then
                       if(field1(i,j) .le. field2(ii,jj,kk)) then
                          ne = ne + 1
                          row(ne) = field1(i,j)
                          col(ne) = field2(ii,jj,kk)
                          val(ne) = val(ne)+x(row(ne))*x(col(ne))
                       end if
                    end if
                 end do
              end if

           end do
        end do
     end do
  end do

end subroutine genQ_at2d_ocu

subroutine calc_HQHTR_old
  use sizes
  use Qdata
  use Rdata
  implicit none
  include 'mpif.h'
  integer, parameter :: rk = kind(1.0d0)
  integer :: i,j,k,Hne,ne,a,cc,d,sizebig
  integer :: nnz,ierr
  logical, allocatable, dimension(:) :: diag
  integer, allocatable, dimension(:) :: hrow,hcol,bigrow,bigcol,bigrowt,bigcolt
  integer, allocatable, dimension(:) :: jao,iao,jc,ic,iw,ir,jaot,iaot
  integer, allocatable, dimension(:) :: Srow,Scol
  real(kind=kind(1.0d0)), allocatable, dimension(:) :: Sval
  real(kind=rk), allocatable, dimension(:) :: hval,bigval,ao,c,bigvalt,aot
  real(kind=rk) :: eig
  real(kind=rk), dimension(obs_dim) :: a1,b1,b2
  real(kind=rk), dimension(state_dim) :: a2,a3
  real(kind=rk) :: scal
  logical :: values
  integer :: ii,ka,kb,jj,nrow,ncol,nzmax,jpos,jcol,len



  print*,'ahoy'

  !start with Q not without separate Qdiag

  !now check to see if all the diagonal entries exist...
  allocate(diag(state_dim))
  diag = .false.
  do j = 1,Qne
     if(Qrow(j) .eq. Qcol(j)) then
        diag(Qrow(j)) = .true.
     end if
  end do

  do j = 1,state_dim
     if(.not. diag(j)) then
        print*,'Diagonal entry ',j,' not present'
     end if
  end do

  print*,'all checked'
  !stop
  
  !compute the largest eigenvalue of Q.
  call power(eig)
  open(3,file='Qeig.dat',action='write',status='replace')
  write(3,*) eig
  close(3)
  !now scale Q by sqrt(eig)
  Qval = Qval/sqrt(eig)


  !now we reduce Q to HQ by picking the appropriate entries

  k = 0
  do j = 1,Qne
     if(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 .and. Qcol(j) .ge.&
          & 539617 .and. Qcol(j) .le. 566986) then
        if(Qrow(j) .eq. Qcol(j)) then
           k = k + 1
        else
           k = k + 2
        end if
     elseif(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 ) then
        k = k + 1
     elseif(Qcol(j) .ge. 539617 .and. Qcol(j) .le. 566986) then
        k = k + 1
     end if
  end do
  Hne = k
  allocate(Hrow(k),Hcol(k),Hval(k))



  k = 0
  d = 0
  do j = 1,Qne
     if(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 .and. Qcol(j) .ge.&
          & 539617 .and. Qcol(j) .le. 566986) then
        k = k + 1
        Hrow(k) = Qrow(j) - 539616
        Hcol(k) = Qcol(j)
        if(Hrow(k) .eq. Hcol(k)) d = d + 1
        Hval(k) = Qval(j)
        if(Qrow(j) .ne. Qcol(j)) then
           k = k + 1
           Hrow(k) = Qcol(j) - 539616
           Hcol(k) = Qrow(j)
           Hval(k) = Qval(j)
        end if
     elseif(Qrow(j) .ge. 539617 .and. Qrow(j) .le. 566986 ) then
        k = k + 1
        Hrow(k) = Qrow(j) - 539616
        Hcol(k) = Qcol(j)! - 539616
        Hval(k) = Qval(j)
     elseif(Qcol(j) .ge. 539617 .and. Qcol(j) .le. 566986) then
        k = k + 1
        Hrow(k) = Qcol(j) - 539616
        Hcol(k) = Qrow(j)
        Hval(k) = Qval(j)
     end if
  end do
  print*,'reduced to HQ k = ',k
  print*,'and contains diagonals ',d
  print*,'minimum row is :',minval(Hrow)
  print*,'maximum row is :',maxval(Hrow)
  print*,'minimum col is :',minval(Hcol)
  print*,'maximum col is :',maxval(Hcol)
  

  Qn = state_dim
  !changing the matrix now:
  allocate(Qdiag(Qn))
  allocate(Srow(Qne-Qn),Scol(Qne-Qn),Sval(Qne-Qn))
  cc = 0
  a = 0
  do i = 1,Qne
     if(Qrow(i) .eq. Qcol(i)) then
        a = a + 1
        Qdiag(a) = Qval(i)
     else
        cc = cc + 1
        Sval(cc) = Qval(i)
        Srow(cc) = Qrow(i)
        Scol(cc) = Qcol(i)
     end if
  end do
  print*,'cc = ',cc


  open(2,file='Qdata.dat',action='write',status='replace',form='unformatted')
  write(2) Qn
  write(2) cc
  do i = 1,Qn
     write(2) Qdiag(i)
  end do
  do i = 1,cc
     write(2) Sval(i)
  end do
  do i = 1,cc
     write(2) Srow(i)
  end do
  do i = 1,cc
     write(2) Scol(i)
  end do
  close(2)
  deallocate(Qval,Qrow,Qcol)
  allocate(Qval(cc),Qrow(cc),Qcol(cc))
  Qval = Sval
  Qrow = Srow
  Qcol = Scol
  Qne = cc




!!$  k=0
!!$  do i = 1,Hne
!!$     if(Hrow(i) .ne. Hcol(i)) then
!!$        if(Hcol(i) .ge. 539617 .and. Hcol(i) .le. 566986) then
!!$           k = k + 1
!!$           bigrow(Hne+k)  = Hcol(i)-539616
!!$           bigrowT(Hne+k) = Hrow(i)
!!$           bigcol(Hne+k)  = Hrow(i)
!!$           bigcolT(Hne+k) = Hcol(i)-539616
!!$           bigval(Hne+k)  = Hval(i)
!!$           bigvalT(Hne+k) = Hval(i)
!!$        end if
!!$     end if
!!$  end do
  
  
  sizebig = k








  allocate(bigrow(sizebig),bigcol(sizebig),bigval(sizebig))
  allocate(bigrowT(sizebig),bigcolT(sizebig),bigvalT(sizebig))
  allocate(ao(sizebig),  jao(sizebig), iao(obs_dim+1))
  allocate(aoT(sizebig),jaoT(sizebig),iaoT(state_dim+1))
  allocate(c(4*(sizebig)),jc(4*(sizebig)),ic(4*(sizebig)),ir(4*(sizebig))&
       &,iw(state_dim))






  !expand Q
  !        
!!$  bigrow(1:Hne)  = Hrow(1:Hne)
!!$  bigrowT(1:Hne) = Hcol(1:Hne)
!!$  bigcol(1:Hne)  = Hcol(1:Hne)
!!$  bigcolT(1:Hne) = Hrow(1:Hne)
!!$  bigval(1:Hne)  = Hval(1:Hne)
!!$  bigvalT(1:Hne) = Hval(1:Hne)
!!$
!!$  k=0
!!$  do i = 1,Hne
!!$     if(Hrow(i) .ne. Hcol(i)) then
!!$        if(Hcol(i) .ge. 539617 .and. Hcol(i) .le. 566986) then
!!$           k = k + 1
!!$           bigrow(Hne+k)  = Hcol(i)-539616
!!$           bigrowT(Hne+k) = Hrow(i)
!!$           bigcol(Hne+k)  = Hrow(i)
!!$           bigcolT(Hne+k) = Hcol(i)-539616
!!$           bigval(Hne+k)  = Hval(i)
!!$           bigvalT(Hne+k) = Hval(i)
!!$        end if
!!$     end if
!!$  end do

  bigrow = Hrow
  bigcol = Hcol
  bigval = Hval
  bigrowT = Hcol
  bigcolT = Hrow
  bigvalT = Hval



  print*,'expanded to nonsymmetric form'
  print*,'minimum row of HQ',minval(bigrow)
  print*,'maximum row of HQ',maxval(bigrow)
  print*,'minimum col of HQ',minval(bigcol)
  print*,'maximum col of HQ',maxval(bigcol)
  print*,'minimum row of (HQ)^T',minval(bigrowT)
  print*,'maximum row of (HQ)^T',maxval(bigrowT)
  print*,'minimum col of (HQ)^T',minval(bigcolT)
  print*,'maximum col of (HQ)^T',maxval(bigcolT)






  print*,minval(bigrow),maxval(bigrow)
  print*,minval(bigcol),maxval(bigcol)

  call coocsr(obs_dim  ,sizebig,bigval ,bigrow ,bigcol ,ao ,jao ,iao)
  print*,'converted first matrix to csr'
  print*,minval(bigrowT),maxval(bigrowT)
  print*,minval(bigcolT),maxval(bigcolT)
  call coocsr(state_dim,sizebig,bigvalT,bigrowT,bigcolT,aoT,jaoT,iaoT)
  print*,'converted to compressed storage'


  print*,'going into amub...'
  !       subroutine amub (nrow,ncol,job,a,ja,ia,b,jb,ib,
  !   *                  c,jc,ic,nzmax,iw,ierr) 

!  call amub(obs_dim,obs_dim,1,ao,jao,iao,ao,jao,iao,c,jc,ic,4*(2*Hne-obs_dim),iw,ierr)
!  call amub(obs_dim,state_dim,1,ao,jao,iao,aoT,jaoT,iaoT,c,jc,ic,4*(sizebig)
  !  ,iw,ierr)

  nrow = obs_dim
  ncol = state_dim
  nzmax = 4*sizebig
  values = .true.
  len = 0
  ic(1) = 1 
  ierr = 0
  !     initialize array iw.
  do j=1, ncol
     iw(j) = 0
  end do

  do ii=1, nrow 
     !     row i 
     do ka=iao(ii), iao(ii+1)-1 
        if (values) scal = ao(ka)
        jj   = jao(ka)
        do kb=iaoT(jj),iaoT(jj+1)-1
           jcol = jaoT(kb)
           jpos = iw(jcol)
           if (jpos .eq. 0) then
              len = len+1
              if (len .gt. nzmax) then
                 ierr = ii
                 print*,'ierr in amub = ',ierr
                 stop
              end if
              jc(len) = jcol
              iw(jcol)= len
              if (values) c(len)  = scal*aoT(kb)
           else
              if (values) c(jpos) = c(jpos) + scal*aoT(kb)
           end if
        end do
     end do
     do  k=ic(ii), len
        iw(jc(k)) = 0
     end do
     ic(ii+1) = len+1
  end do
           
           
           
           
  print*,'amub ierr = ',ierr
  print*,'multiplied matrix'
  call csrcoo(obs_dim,3,4*(sizebig),c,jc,ic,nnz,c,ir,jc,ierr)
  print*,'csrcoo ierr = ',ierr
  print*,'csrcoo nnz = ',nnz
  print*,'converted to coordinate storage'
  ic = ir

  deallocate(Hrow,Hcol,Hval)
  ne = 0
  do i = 1,nnz
     if(ic(i) .le. jc(i)) then
        ne = ne+1
     end if
  end do
  allocate(Hval(ne),Hrow(ne),Hcol(ne))
  k = 0
  do i = 1,nnz
     if(ic(i) .le. jc(i)) then
        k = k + 1
        Hrow(k) = ic(i)
        Hcol(k) = jc(i)
        Hval(k) = c(i)
     end if
  end do
  print*,'reduced to upper triangular form'

  print*,'minimum row in H:',minval(Hrow)
  print*,'maximum row in H:',maxval(Hrow)
  print*,'minimum col in H:',minval(Hcol)
  print*,'maximum col in H:',maxval(Hcol)

!  open(9,file='endofwork',action='write',status='replace')
!  close(9)
!!$
!!$
!!$
!!$   print*,'hoy'
!!$   open(20,file='done',action='write',status='replace')
!!$   close(20)

  open(2,file='HQHTRdata.dat',action='write',status='replace',form='unfor&
       &matted')
  write(2) obs_dim
  write(2) ne
  write(2) Hrow
  write(2) Hcol
  write(2) Hval
  close(2)


!TEST THAT SPARSE MATRIX MULTIPLICATION WORKED:
!CHECK H*Q*Q*HT == HQQHT
  do i = 1,obs_dim
     a1(i) = real(i,rk)
  end do
  call HT(a1,a2)
  call Q(1,a2,a3)
!  call Q(1,a3,a2)
  call H(a3,b1)


  b2 = 0.0_rk
  do i = 1,ne
     if(Hrow(i) .ne. Hcol(i)) then
        b2(Hrow(i)) = b2(Hrow(i)) + Hval(i)*a1(Hcol(i))
        b2(Hcol(i)) = b2(Hcol(i)) + Hval(i)*a1(Hrow(i))
     else
        b2(Hrow(i)) = b2(Hrow(i)) + Hval(i)*a1(Hcol(i))
     end if
  end do

  print*,'testing if sparse matrix multiplication worked:...'
  print*,'H*Q*Q*HT == HQQHT'
  print*,'sum((b1-b2)**2) = ',sum((b1-b2)**2)
  print*,'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
  print*,'#######################################'

  do i = 1,200
     print*,i,b1(i),b2(i)
  end do




  call loadR
  !practice scaling Q here...
  Hval = Hval/1.0D3
  do i = 1,ne
     if(Hrow(i) .eq. Hcol(i)) Hval(i) = Hval(i) + Rval(1)
  end do


  call checkHQHTR_factor(obs_dim,ne,Hrow,Hcol,Hval)







  !end if
  !print*,'pid = ',pid,' calling mpi_finalize'
  !call mpi_finalize(err)
  !print*,'pid ',pid,' has err = ',err
  !if(pid .eq. 0) stop 
end subroutine calc_HQHTR_old




subroutine checkHQHTR_factor(HQHTRn,HQHTRne,HQHTRrow,HQHTRcol,HQHTRval)
  use hsl_mc68_double
  use hsl_mc69_double
  use hsl_ma87_double
  implicit none
  integer, parameter :: wp = kind(0d0)
  integer, intent(in) :: HQHTRn,HQHTRne
  integer, dimension(HQHTRne), intent(in) :: HQHTRrow,HQHTRcol
  integer, dimension(HQHTRn) :: HQHTR_order
  real(kind=wp), dimension(HQHTRne), intent(in) :: HQHTRval
  type (ma87_keep) :: HQHTR_keep
  type (ma87_control) :: HQHTR_control
  type (ma87_info) :: HQHTR_info
  type(mc68_control) :: control68
  type(mc68_info) :: info68

  integer :: lmap, flag
  integer, dimension(:), allocatable  :: ptr, row, map
  real(wp), dimension(:), allocatable :: val

  ! Read the first matrix in coordinate format
  ! Convert to HSL standard format

  allocate(ptr(HQHTRn+1))
  print*,'reconstructing matrix in mc69:'


  call mc69_coord_convert(HSL_MATRIX_REAL_SYM_PSDEF, HQHTRn, HQHTRn,&
       &HQHTRne, HQHTRrow, HQHTRcol, ptr, row, &
       &flag, val_in=HQHTRval, val_out=val, lmap=lmap, map=map)
  print*,'mc69 finished with flag (should read 0): ',flag

  print*,'getting pivot order from mc68'
  call mc68_order(1, HQHTRn, ptr, row, HQHTR_order, control68, info68)
  print*,'pivot order returned from mc68'

  open(2,file='HQHTRdata.dat',action='write',status='old',form='unfor&
       &matted',position='append')
  write(2) HQHTR_order
  close(2)

  print*,'analyse phase starting'
  ! Analyse
  call ma87_analyse(HQHTRn, ptr, row, HQHTR_order, HQHTR_keep,&
       &HQHTR_control, HQHTR_info)
  print*,'analysis finished'
  print*,'factorisation starting:'
  ! Factor
  call ma87_factor(HQHTRn, ptr, row, val, HQHTR_order, HQHTR_keep,HQHTR_control&
       &, HQHTR_info)
  print*,'factorisation complete...'


end subroutine checkHQHTR_factor




subroutine power(eig)
  use Qdata
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  integer :: n,ldv

  integer, parameter :: nev=1,ncv = 31

  !real(kind=rk), dimension(n,ncv) :: v
  real(kind=rk), allocatable, dimension(:,:) :: v
  !real(kind=rk), dimension(ncv*(ncv+8)) :: workl
  real(kind=rk), ALLOCATABLE, dimension(:) :: workl
  !real(kind=rk), dimension(3*n) :: workd
  real(kind=rk), allocatable, dimension(:) :: workd
  !real(kind=rk), dimension(nev) :: d
  real(kind=rk), allocatable, dimension(:) :: d
  !real(kind=rk), dimension(n) :: resid
  real(kind=rk), allocatable, dimension(:) :: resid

  !logical, dimension(ncv) :: select
  logical, allocatable, dimension(:) :: select
  integer, dimension(11) :: iparam,ipntr
  character :: bmat
  character(2) :: which
  integer :: ido,lworkl,info,ierr,nconv,maxitr,mode,ishfts,iter,Qnev,i
  logical::  rvec
  real(kind=rk) :: tol,sigma
  real(kind=rk), intent(out) :: eig

  n = state_dim
  ldv = n

  allocate(v(n,ncv))
  allocate(workl(ncv*(ncv+8)))
  allocate(workd(3*n))
  allocate(d(nev))
  allocate(resid(n))
  allocate(select(ncv))

  bmat = 'I'
  which = 'LA'

  lworkl = ncv*(ncv+8)
  tol = 0.0d0
  info = 0
  ido = 0

  ishfts = 1
  maxitr = 300
  mode   = 1

  iparam(1) = ishfts 
  iparam(3) = maxitr 
  iparam(7) = mode 
  iter = 0

  do

     call dsaupd ( ido, bmat, n, which, nev, tol, resid, ncv, v, ldv, iparam, ipntr, workd, workl, lworkl, info )
     iter = iter + 1
     print*,iter
     call flush(6)
     if (ido .eq. -1 .or. ido .eq. 1) then
        call QgenQ(workd(ipntr(1)), workd(ipntr(2)))
     else if( info .lt. 0 ) then

        print *, ' '
        print *, ' Error with _saupd, info = ', info
        print *, ' Check documentation in _saupd '
        print *, ' '
        exit
     else

        rvec = .true.

        call dseupd ( rvec, 'All', select, d, v, ldv, sigma, &
             &        bmat, n, which, nev, tol, resid, ncv, v, ldv,& 
             &        iparam, ipntr, workd, workl, lworkl, ierr )

        if ( ierr .ne. 0) then

           print *, ' '
           print *, ' Error with _seupd, info = ', ierr
           print *, ' Check the documentation of _seupd. '
           print *, ' '
           exit
        else

           nconv =  iparam(5)

        end if
        if ( info .eq. 1) then
           print *, ' '
           print *, ' Maximum number of iterations reached.'
           print *, ' '
        else if ( info .eq. 3) then
           print *, ' ' 
           print *, ' No shifts could be applied during implicit Arnoldi update, try increasing NCV.'
           print *, ' '
        end if

        print *, ' '
        print *, ' _SDRV1 '
        print *, ' ====== '
        print *, ' '
        print *, ' Size of the matrix is ', n
        print *, ' The number of Ritz values requested is ', nev
        print *, ' The number of Arnoldi vectors generated',&
             &            ' (NCV) is ', ncv
        print *, ' What portion of the spectrum: ', which
        print *, ' The number of converged Ritz values is ', &
             &              nconv 
        print *, ' The number of Implicit Arnoldi update',&
             &            ' iterations taken is ', iparam(3)
        print *, ' The number of OP*x is ', iparam(9)
        print *, ' The convergence criterion is ', tol
        print *, ' '
        print*,'eigenvalues that are calculated are:'
        print*,d(1:nev)

        Qnev = nev
        do i = 1,nev
           if(d(i) .le. 0.0d0) then
              Qnev = Qnev - 1
           else
              exit
           end if
        end do



        !      open(2,file='Qevd.dat',action='write',status='replace',form='unformatted&
        !           &')
        !      write(2) Qnev
        !      write(2) d(nev-Qnev+1:nev)
        !      write(2) v(:,nev-Qnev+1:nev)
        !      close(2)
        !      print*,'Data dumped to Qevd.dat'





        exit
     end if
  end do

  eig = d(1)      
end subroutine power





subroutine QgenQ(x,Qx)
  !subroutine to take a full state vector x and return Qx
  !in state space.
  use sizes
  use Qdata
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  real(kind=rk), dimension(state_dim), intent(in) :: x
  real(kind=rk), dimension(state_dim), intent(out) :: qx

  call genQ_coord_matmul_t(state_dim,Qne,Qrow,Qcol,Qval,x,qx)

end subroutine QGENQ


subroutine genQ_coord_matmul_t(n,ne,row,col,val,x,b)
  integer, parameter :: rk = kind(1.0D+0)
  integer, intent(in) :: n,ne
  integer, intent(in), dimension(ne) :: row,col
  real(kind = rk), dimension(ne), intent(in) :: val
  real(kind = rk), dimension(n), intent(in) :: x
  real(kind = rk), dimension(n), intent(out) :: b
  integer :: k
  b = 0.0_rk
  do k = 1,ne
     if(row(k) .eq. col(k)) then
        b(row(k)) = b(row(k)) + val(k)*x(col(k))
     else
        b(row(k)) = b(row(k)) + val(k)*x(col(k))
        b(col(k)) = b(col(k)) + val(k)*x(row(k))
     end if
  end do
end subroutine genQ_coord_matmul_t

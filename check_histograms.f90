program check_histograms
use histogram_data

implicit none
include 'mpif.h'
integer, parameter :: rk = kind(1.0d0)

integer :: mpi_err,pfrank,npfs!dummy_colour,mpi_err
type :: pf_control_type
   integer :: nens !the number of ensemble members
   real(kind=kind(1.0D0)), allocatable, dimension(:) :: weight !stores the weights of the particles
   integer :: time_obs !the number of observations we will assimilate
   integer :: time_bwn_obs !the number of model timesteps between observations
   real(kind=kind(1.0D0)) :: nudgefac !the nudging factor
   logical :: gen_data,gen_Q,human_readable
   integer :: timestep=0
   real(kind=kind(1.0D0)), allocatable, dimension(:,:) :: psi
   real(kind=kind(1.0D0)), allocatable, dimension(:) :: mean
   real(kind=kind(1.0D0)) :: nfac                !standard deviation of normal distribution in mixture density
   real(kind=kind(1.0D0)) :: ufac                !half width of the uniform distribution in mixture density
   real(kind=kind(1.0D0)) :: efac
   real(kind=kind(1.0D0)) :: keep,time
   real(kind=kind(1.0D0)) :: Qscale
   integer :: couple_root
   logical :: use_talagrand,use_weak,use_mean,use_var,use_traj,use_rmse
   integer, dimension(:,:), allocatable :: talagrand
   integer :: count
   integer,allocatable, dimension(:) :: particles
   character(2) :: type
   character(1) :: init
end type pf_control_type
type(pf_control_type) :: pf 






!    print*,'initialising mpi'
CALL MPI_INIT(mpi_err)
!    print*,'mpi init fucking worked'
CALL MPI_COMM_RANK(MPI_COMM_WORLD,pfrank,mpi_err)
call mpi_comm_size(mpi_comm_world,npfs,mpi_err)


pf%time_bwn_obs=72
pf%time_obs = 3
pf%timestep = 3*72
pf%gen_data = .false.
pf%use_talagrand = .true.
pf%nens = 32
call load_histogram_data
allocate(pf%talagrand(rhn_n,pf%nens+1))
allocate(pf%particles(1))
pf%particles = pfrank+1

call do_histograms(pf,npfs,pfrank)

call mpi_finalize(mpi_err)
end program check_histograms


subroutine do_histograms(pf,npfs,pfrank)
  use histogram_data

  implicit none
  integer, parameter :: rk = kind(1.0d0)
type :: pf_control_type
   integer :: nens !the number of ensemble members
   real(kind=kind(1.0D0)), allocatable, dimension(:) :: weight !stores the weights of the particles
   integer :: time_obs !the number of observations we will assimilate
   integer :: time_bwn_obs !the number of model timesteps between observations
   real(kind=kind(1.0D0)) :: nudgefac !the nudging factor
   logical :: gen_data,gen_Q,human_readable
   integer :: timestep=0
   real(kind=kind(1.0D0)), allocatable, dimension(:,:) :: psi
   real(kind=kind(1.0D0)), allocatable, dimension(:) :: mean
   real(kind=kind(1.0D0)) :: nfac                !standard deviation of normal distribution in mixture density
   real(kind=kind(1.0D0)) :: ufac                !half width of the uniform distribution in mixture density
   real(kind=kind(1.0D0)) :: efac
   real(kind=kind(1.0D0)) :: keep,time
   real(kind=kind(1.0D0)) :: Qscale
   integer :: couple_root
   logical :: use_talagrand,use_weak,use_mean,use_var,use_traj,use_rmse
   integer, dimension(:,:), allocatable :: talagrand
   integer :: count
   integer,allocatable, dimension(:) :: particles
   character(2) :: type
   character(1) :: init
end type pf_control_type
type(pf_control_type), intent(inout) :: pf 
integer, intent(in) :: npfs,pfrank
  real(kind=rk), dimension(rhl_n,pf%nens) :: HHpsi
  real(kind=rk), dimension(pf%nens) :: bin_marker
  real(kind=rk), dimension(rhl_n) :: y
  integer :: particle,time,i,j,mpi_err,lb,length
  logical :: placed
  character(32) :: filename
  integer, dimension(rhn_n,pf%nens+1) :: reduced_talagrand
  include 'mpif.h'
  if(.not. pf%gen_data) then
     if(pf%use_talagrand) then
        
        print*,'rhl_n = ',rhl_n
        !        call H(pf%psi,Hpsi)
        inquire(iolength=length) bin_marker(1)

        if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs) then
           pf%talagrand = 0
           call mpi_barrier(mpi_comm_world,mpi_err)
           
           do time = 1,pf%time_obs
              !              print*,'time = ',time
              if(mod(time,npfs) .eq. pfrank) then
                 !cock cock cock adjusted the below to make it sensible
                 pf%timestep = time*pf%time_bwn_obs
                 print*,'pfrank = ',pfrank,'picking up truth at ',pf%timestep
                 !call get_truth(y)
                 write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',((pf%timestep)/pf&
                      &%time_bwn_obs) ,'truth'
                 print*,'filename = ',filename
                 open(12,file=filename,action='read',status='old',form='unforma&
                      &tted',access='direct',recl=length)
                 !           call H(pf%psi(:,particle),Hpsi)
                 do i = 1,rhl_n
                    print*,'pfrank = ',pfrank,' i = ',i
                    read(12,rec=i) y(i)
                    print*,'pfrank = ',pfrank,' i = ',i,' y(i) = ',y(i)
                 end do
                 close(12)

                 print*,'got obs data at timestep ',pf%timestep
                 do particle = 1,pf%nens
                    write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',pf&
                         &%timestep/pf%time_bwn_obs,'&
                         &particle',particle
                    open(13, file=filename,action='read',status='old',form='un&
                         &for&
                         &matted',access='direct',recl=length)
                    do i = 1,rhl_n
                       read(13,rec=i) HHpsi(i,particle)
                    end do
                    close(13)
                 end do
                              print*,'read HHpsi'
                              print*,'rhn_n = ',rhn_n

                 do j = 1,rhn_n !for each histogram we want to make
                    if( j .eq. 1) then
                       lb = 1
                    else
                       lb = sum(rank_hist_nums(1:j-1)) + 1
                    end if
                    PRINT*,'j = ',j,' lb = ',lb,' ub = ',sum(rank_hist_nums(1:j))
                 do i = lb,sum(rank_hist_nums(1:j))

                    do particle = 1,pf%nens
                       bin_marker(particle) = HHpsi(i,particle)
                    end do

                    call quicksort_d(bin_marker,pf%nens)
                    
                    if(i .eq. sum(rank_hist_nums(1:j))) then
                       print*,pfrank,i,bin_marker,y(i)
                    end if
                    
                                       print*,'quicksorted'
                    placed = .false.
                    do particle  = 1,pf%nens
                       if(y(i) .lt. bin_marker(particle)) then
                          pf%talagrand(j,particle) = pf&
                               &%talagrand(j,particle)&
                               & + 1
                          placed = .true.
                          exit
                       end if
                    end do
                    !                    print*,'did we place?'

                    if(.not. placed) then
                       if(y(i) .ge. bin_marker(pf%nens)) then
                          pf%talagrand(j,pf%nens+1) = pf&
                               &%talagrand(j,pf%nens+1) + 1
                       else
                          stop 'There was an error in the calculation of the p&
                               &lacement &
                               &in the rank histogram. Bums.'
                       end if
                    end if

                 end do
                 end do

              end if !end of mpi splitting by timestep
           end do !end of the timestep
           !           print*,'end of all the timesteps'

           !now let us reduce the information to the master processor:
           call mpi_reduce(pf%talagrand,reduced_talagrand,rhn_n*(pf%nens+1)&
                &,mpi_integer,mpi_sum,0,mpi_comm_world,mpi_err)

           !           print*,'some reduction just happened'

           if(pfrank .eq. 0) then
              do i = 1,rhn_n
                 write(filename,'(A,i0)') 'histogram_',i
                 open(17,file=filename,action='write',status='replace')
                 do j = 1,pf%nens+1
                    write(17,'(i8.8)') reduced_talagrand(i,j)
                 end do
                 close(17)
              end do
              !now output the image
           end if
           pf%timestep = pf%time_obs*pf%time_bwn_obs
        end if !end of if we are the last step in the pf

     end if

     if(pf%use_rmse) then
        

     end if

  else
     inquire(iolength=length) pf%psi(1,1)

!        pf%timestep = -72
     do particle = 1,pf%count
        write(filename,'(A,i6.6,A)') 'hist/timestep',((pf%timestep)/pf&
             &%time_bwn_obs) ,'truth'
        open(12,file=filename,action='write',status='replace',form='unforma&
             &tted',access='direct',recl=length)
        !           call H(pf%psi(:,particle),Hpsi)
        do i = 1,rhl_n
           write(12,rec=i) pf%psi(rank_hist_list(i),particle)
        end do
        close(12)
     end do
  end if


end subroutine do_histograms

!!! Time-stamp: <2014-03-03 16:08:45 pbrowne>

program make_pdfs
  use histogram_data
  use pf_control
  implicit none
  integer, parameter :: state_dim = 2314430
  integer, parameter :: obs_dim = 27370
  integer, parameter :: rk = kind(1.0d0)


!  real(kind=rk), dimension(:,:), allocatable :: HHpsi
  real(kind=rk), dimension(:),allocatable :: x,pdf
  real(kind=rk), dimension(state_dim) :: truth
  integer :: time,i,mpi_err,npfs!,timestep
!  logical :: placed
 ! integer, dimension(:,:), allocatable :: talagrand,reduced_talagrand
  LOGICAL :: file_exists
  integer :: pfrank,nens,time_obs
  character(32) :: filename
  integer :: num
  include 'mpif.h'

!  CALL MPI_INIT (mpi_err)
!  CALL MPI_COMM_RANK(MPI_COMM_WORLD,pfrank,mpi_err)
!  call mpi_comm_size(mpi_comm_world,npfs,mpi_err)

  i = 0
  do
     i = i + 1
     write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',i,'particle',1
     !print*,filename
     INQUIRE(FILE=filename, EXIST=file_exists)
     if(.not. file_exists) then
        print*,filename
        time_obs = i-1
        exit
     end if
  end do
  print*,'time_obs=',time_obs
  pf%time_obs = time_obs - 1
  
  file_exists = .true.
  i = 0
  do 
     i = i + 1
     write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',1,'particle',i
     INQUIRE(FILE=filename, EXIST=file_exists)
     if(.not. file_exists) then
        print*,filename
        nens = i-1
        exit
     end if
  end do
 
  print*,'nens=',nens
  print*,'npfs = ',npfs
  call load_histogram_data

allocate(x(rhl_n))
allocate(pdf(nens))
  
write(*,*) 'Please enter the histogram number you want the pdf of.'
write(*,'(A,i0,A)',advance='no') 'This should be between 1 and ',rhl_n,':  '
read*,num

write(*,*) 'Please enter the time at which you want the pdf of.'
write(*,'(A,i0,A)',advance='no') 'This should be between 1 and ',time_obs,':  '
read*,time


do i = 1,nens
   write(filename,'(A,i6.6,A,i5.5)') 'hist/timestep',time,'particle',i
   open(2,FILE=filename, action='read',form='unformatted')
   read(2) x
   close(2)
   pdf(i) = x(num)
end do

open(2,file='pdfoutput',action='write',status='replace')
do i = 1,nens
   write(2,*) pdf(i)
end do
close(2)

call get_truth(truth,time)
open(2,file='truthoutput',action='write',status='replace')
write(2,*) truth(rank_hist_list(num))
close(2)


!  call mpi_finalize(mpi_err)
  print*,'pfrank = ',pfrank,' mpi_err = ',mpi_err
  
end program make_pdfs

subroutine get_truth(x,timestep)
  
  implicit none
  integer, parameter :: rk = kind(1.0D0)
  integer, parameter :: state_dim = 2314430
  integer, parameter :: obs_dim = 27370
  
  !  real(kind=rk), dimension(state_dim), intent(out) :: hx                                
  real(kind=rk), dimension(state_dim), intent(out) :: x
  INTEGER, INTENT(IN) :: timestep
  integer :: ios,length,rec
  inquire(iolength=length) x
  open(62,file='pf_truth',iostat=ios,action='read',form='unformatted',access='&
       &direct',recl=length)
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_truth to read'
     write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
     stop
  end if
!  do i = 0,pf%timestep
  rec = timestep + 1
  read(62,rec=rec) x
!  end do
  close(62)


  !  call H(1,x,hx)                                                                        

end subroutine get_truth

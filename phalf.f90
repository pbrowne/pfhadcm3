!> subroutine to take a full state vector x and return \f$P^{1/2}x\f$
!> in state space.
!!
!! Given \f$x\f$ compute \f$P^{\frac{1}{2}}x\f$
!!
!! where \f$P = (Q^{-1} +H^TR^{-1}H)^{-1} = Q^{\frac{1}{2}}(I +
!! Q^{\frac{1}{2}}H^TR^{-1}HQ^{\frac{1}{2}})^{-1}Q^{\frac{1}{2}}\f$ 
!!
!! This is required for the Zhu Equal weights particle filter
!! @ref equivalent_weights_filter_zhu
subroutine Phalf(nrhs,x,Px)
  use sizes
  use Qdata
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs !< the number of right hand sides
  real(kind=rk), dimension(state_dim,nrhs), intent(in) :: x !< the
  !!input vector
  real(kind=rk), dimension(state_dim,nrhs), intent(out) :: px !< the
  !!resulting vector where Px \f$= P^{\frac{1}{2}}x\f$

  real(kind=rk), dimension(state_dim,nrhs) :: qhalfx
  integer :: i

  call Qhalf(nrhs,x,qhalfx)

  do i = 1,nrhs
     call mat_sqrt_for_phalf(qhalfx(:,i),px(:,i))
  end do

  
end subroutine Phalf

subroutine mat_sqrt_for_phalf(x,y)
  ! Simple code to illustrate row entry to hsl_ea20
!  use pf_control
  use HSL_EA20_double
  use sizes
  implicit none

  ! Derived types
  type (ea20_control) :: cntl
  type (ea20_info)    :: info
  type (ea20_reverse) :: rev

  ! Parameters

  integer, parameter :: wp = kind(0.0d0)    
  integer :: ido
  real(kind=kind(1.0D0)), dimension(state_dim), intent(out) :: y
  real(kind=kind(1.0D0)), dimension(state_dim), intent(in) :: x
  real(kind=kind(1.0D0)), allocatable  :: w(:,:)
  real(kind=kind(1.0D0))               :: s

  !! set u
  y = x 

  !! set data
  s = -0.5d0

  !! set cntl
  cntl%d     = 3         !! delay
  cntl%tol   = 1.d-2     !! convergece tolerance
  cntl%maxit = 20        !! max number iteration

  cntl%diagnostics_level = 1 !! full error check

  ido = -1

  do while ( ido .ne. 0 .and. info%flag == 0)

     call EA20(state_dim,y,s,ido,w,cntl,info,rev)

     select case ( ido )

     case (0)
        exit

     case (1) !! Matrix-Vector product w_out = A w(:,1)
!!!        call Q(w(:,1),w(:,2))

        call a_for_phalf(1, w(:,1) , w(:,2) )

!        if(maxval(abs(w(:,1)-w(:,2))) .gt. 1.0D-10) then
!           print*,'shaft! ',maxval(abs(w(:,1)-w(:,2))) 
!        end if
        
     case (2) !! Matrix-Vector product w(:,2) = M w(:,1)
        w(:,2) = w(:,1)

     case (3) !! Matrix-Vector product w_out = inv(M) w(:,1)
        w(:,2) = w(:,1)

     end select

  end do


  if (info%flag .ge. 0) then
!     write(*,'(a,i5)') 'error code  = ',info%flag
     !! print the final solution
!     write(*,'(a,i5)') 'number of iterations  = ',info%iter
!     write(*,'(a,1x,1pe14.2)') 'estimated final error = ',info%error
!     write(*,'(a)') '    i       X(i)'
!     write(*,'(i5,1x,1pe14.3)') (i,y(i), i=1,state_dim)

  else
!     write(*,'(i5,1x,1pe14.3)') (i,x(i), i=1,state_dim) 
     print*,'EA20 broke: max x = ',maxval(x),' min x = ',minval(x)
!     write(filename,'(A,i0)') 'data',pf%timestep
!     open(20,file=filename,action='write')
!     do ido = 1,state_dim
!        write(20,*) x(ido)
!     end do
!     close(20)

!     stop
  end if




end subroutine mat_sqrt_for_phalf



subroutine a_for_phalf(nrhs,x,ax)
  use sizes
  use Qdata
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs !< the number of right hand sides
  real(kind=rk), dimension(state_dim,nrhs), intent(in) :: x !< the
  !!input vector
  real(kind=rk), dimension(state_dim,nrhs), intent(out) :: ax 

  real(kind=rk), dimension(state_dim,nrhs) :: qhalfx,htrhqhalfx
  real(kind=rk), dimension(obs_dim,nrhs) :: hqhalfx,rhqhalfx
  

  call Qhalf(nrhs,x,qhalfx)
  
  call H(nrhs,qhalfx,hqhalfx)

  call solve_r(nrhs,hqhalfx,rhqhalfx)

  call HT(nrhs,rhqhalfx,htrhqhalfx)

  call Qhalf(nrhs,htrhqhalfx,qhalfx)
  
  ax = x + qhalfx
end subroutine a_for_phalf

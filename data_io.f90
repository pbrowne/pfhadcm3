!!! Time-stamp: <2014-08-01 11:20:32 pbrowne>

subroutine get_observation_data(y)
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0D0)
  real(kind=rk), dimension(obs_dim), intent(out) :: y
  integer :: obs_number,ios
  character(14) :: filename

  obs_number = ((pf%timestep-1)/pf%time_bwn_obs) + 1

  write(filename,'(A,i6.6)') 'obs/num_',obs_number

  open(67,file=filename,iostat=ios,action='read',status='old',form='unformatted')
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file ',filename
     write(*,*) 'Check it exists. I need a lie down.'
     stop
  end if
  read(67) y
  close(67)
end subroutine get_observation_data

subroutine save_observation_data(y)
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0D0)
  real(kind=rk), dimension(obs_dim), intent(in) :: y
  integer :: obs_number,ios
  character(14) :: filename

  obs_number = ((pf%timestep-1)/pf%time_bwn_obs) + 1

  write(filename,'(A,i6.6)') 'obs/num_',obs_number

  open(67,file=filename,iostat=ios,action='write',status='replace',form='unformatted')
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file ',filename
     write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
     stop
  end if
  write(67) y
  close(67)

  if(pf%human_readable) then
     open(64,file='pf_data',action='write',position='append')
     if(ios .ne. 0)  then
        write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_data'
        write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
        stop
     end if
     write(64,*) pf%timestep,y
     close(64)
  end if
end subroutine save_observation_data

subroutine save_truth(x)
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0D0)
  real(kind=rk), dimension(state_dim), intent(in) :: x
  integer :: ios,length,rec
  if(pf%timestep .eq. 0) then
     print*,'opening pf_truth'
     inquire(iolength=length) x
     open(62,file='pf_truth',iostat=ios,action='write',status='replace',form='&
          &unformatted',access='direct',recl=length)
     if(ios .ne. 0)  then
        write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_truth to write'
        write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
        stop
     end if
  end if
  rec = (pf%timestep/pf%time_bwn_obs) + 1
  write(62,rec=rec) x
  call flush(62)
  if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs) then
     close(62)
     print*,'closing pf_truth'
     print*,'converting pf_truth to netcdf format'
     call convert_pf_truth
     print*,'finished converting pf_truth to netcdf'
  end if
end subroutine save_truth

subroutine get_truth(x)
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0D0)
  !  real(kind=rk), dimension(state_dim), intent(out) :: hx                                
  real(kind=rk), dimension(state_dim), intent(out) :: x
  integer :: ios,length,rec!,i
  inquire(iolength=length) x
  open(62,file='pf_truth',iostat=ios,action='read',form='unformatted',access='&
       &direct',recl=length)
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_truth to read'
     write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
     stop
  end if
!  do i = 0,pf%timestep

  rec = (pf%timestep/pf%time_bwn_obs) + 1
  read(62,rec=rec) x
!  end do
  close(62)


  !  call H(1,x,hx)                                                                        

end subroutine get_truth


subroutine output_from_pf
  use pf_control
  use sizes
  use comms
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  real(kind=rk), dimension(state_dim) :: mean,reduced_mean,var,reduced_var&
       &,truth,mse
  real(kind=rk) :: rmse
  integer :: ios,mpi_err,i,length!,particle,j
  integer, dimension(81) :: oends
  character(9) :: filename
  include 'mpif.h'
  if(pf%timestep .eq. 0) then
     write(filename,'(A,i2.2)') 'pf_out_',pfrank
     open(68,file=filename,iostat=ios,action='write',status='replace')
     if(ios .ne. 0)  then
        write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_out'
        write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
        stop
     end if
  end if
  !  print*,'output: ',pf%timestep,pf%weight
  !  write(68,*) pf%timestep,pf%particles,pf%weight(:)
  write(68,'(i6.6,A)',advance='no') pf%timestep,' '
  do ios = 1,pf%count-1
     write(68,'(i6.6,A,e21.15,A)',advance='no') pf%particles(ios),' ',pf&
          &%weight(pf%particles(ios)),' '
  end do
  write(68,'(i6.6,A,e21.15)',advance='yes') pf%particles(pf%count),' ',pf&
       &%weight(pf%particles(pf%count))
  call flush(68)
  if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs) close(68)

  if(pf%use_mean) then
!!$     if(pf%timestep .eq. 0) then
!!$        open(61,file='pf_mean',iostat=ios,action='write',status='replace')
!!$        if(ios .ne. 0)  then
!!$           write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_mean'
!!$           write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
!!$           stop
!!$        end if
!!$     end if
!!$     mean = 0.0D0
!!$     do particle = 1,pf%nens
!!$        mean(:) = mean(:) + pf%psi(:,particle)*exp(-pf%weight(particle))
!!$     end do
!!$     write(61,*) mean(:)
!!$     call flush(61)
!!$     if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs) close(61)
     if(pf%timestep .eq. 0 .and. pfrank .eq. 0) then
        inquire(iolength=length) reduced_mean
        open(61,file='pf_mean',iostat=ios,action='write',status='replace'&
             &,form='unformatted',access='direct',recl=length)
        if(ios .ne. 0)  then
           write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_mean'
           write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
           stop
        end if
     end if

     if(mod(pf%timestep,pf%time_bwn_obs) .eq. 0) then

        mean = sum(pf%psi,dim=2)
        call mpi_reduce(mean,reduced_mean,state_dim&
             &,mpi_double,mpi_sum,0,pf_mpi_comm,mpi_err)

        reduced_mean = reduced_mean/real(pf%nens,rk)

        if(pfrank .eq. 0) then
           print*,'writing mean at ',pf%timestep
           write(61,rec=(pf%timestep/pf%time_bwn_obs)+1) reduced_mean
           call flush(61)
        end if

        if(pf%use_rmse .and. pfrank .eq. 0) then
           write(filename,'(A,i4.4)') 'rmse/',pf%timestep/pf%time_bwn_obs
           open(55,file=filename,iostat=ios,action='write',status='replace')
           if(ios .ne. 0)  then
              write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file ',filename
              write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
              stop
           end if
           call get_truth(truth)
           !note if want latitude weighted rmse we need to
           !introduce weighting term based on latitude to
           !the line below
           mse = (truth-reduced_mean)**2
           
           !atmospheric variables...
           do i = 0,76
              rmse = (sum(mse(i*7008+1:(i+1)*7008))/real(7008,rk))**0.5_rk
              write(55,'(es24.16)') rmse
           end do

           !ocean ends = 
           oends = (/539616,&
                     566986,594356,621726,649096,676466,703836,&
                     731206,758576,784367,809527,834176,858307,&
                     881944,904925,927026,947784,966561,981602,&
                     991838,997127,&
                     1024497,1051867,1079237,1106607,1133977,1161347,&
                     1188717,1216087,1241878,1267038,1291687,1315818,&
                     1339455,1362436,1384537,1405295,1424072,1439113,&
                     1449349,1454638,&
                     1480669,1506700,1532731,1558762,1584793,1610824,&
                     1636855,1662886,1687372,1711237,1734616,1757465,&
                     1779808,1801473,1822176,1841437,1858517,1871704,&
                     1880336,1884534,&
                     1910565,1936596,1962627,1988658,2014689,2040720,&
                     2066751,2092782,2117268,2141133,2164512,2187361,&
                     2209704,2231369,2252072,2271333,2288413,2301600,&
                     2310232,2314430/)
           do i = 1,80
              rmse = (sum(  mse(oends(i)+1:oends(i+1))  )/real( oends(i+1)&
                   &-oends(i)  ,rk))**0.5_rk
              write(55,'(es24.16)') rmse
           end do
           close(55)
        end if

     end if

     if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs .and. pfrank .eq. 0) then
        close(61)
        print*,'converting pf_mean to netcdf...'
        call convert_pf_mean
        print*,'finished converting pf_mean to netcdf.'
     end if

     if(pf%use_var) then
        if(pf%timestep .eq. 0 .and. pfrank .eq. 0) then
           inquire(iolength=length) reduced_var
           open(60,file='pf_var',iostat=ios,action='write',status='replace'&
                &,form='unformatted',access='direct',recl=length)
           if(ios .ne. 0)  then
              write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_var'
              write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
              stop
           end if
        end if

        if(mod(pf%timestep,pf%time_bwn_obs) .eq. 0) then

           var = sum((pf%psi)**2,dim=2)
           call mpi_reduce(var,reduced_var,state_dim&
                &,mpi_double,mpi_sum,0,pf_mpi_comm,mpi_err)

           reduced_var = reduced_var/real(pf%nens-1,rk)
           reduced_var = reduced_var - (real(pf%nens,rk)/real(pf%nens-1,rk))*reduced_mean**2

           if(pfrank .eq. 0) then
              print*,'writing var at ',pf%timestep
              write(60,rec=(pf%timestep/pf%time_bwn_obs)+1) reduced_var
              call flush(60)
           end if
        end if

        if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs .and. pfrank .eq. 0)&
             & then
           close(60)
           print*,'converting pf_var to netcdf...'
           call convert_pf_var
           print*,'finished convering pf_var to netcdf'
        end if
     end if

  end if

!!$  if(pf%use_weak) then
!!$     if(pf%timestep .eq. 0) then
!!$        open(77,file='pf_weak',iostat=ios,action='write',status='replace&
!!$             &')
!!$     else
!!$        open(77,file='pf_weak',iostat=ios,action='write',status&
!!$             &='old',position='append')
!!$     end if
!!$     if(ios .ne. 0)  then
!!$        write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_&
!!$             &weak'
!!$        write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
!!$        stop
!!$     end if
!!$     
!!$     do particle = 1,pf%nens
!!$        write(77,*) pf%psi(:,particle),exp(-pf%weight(particle))
!!$     end do
!!$     close(77)
!!$     
!!$     if(pf%timestep .eq. pf%time_obs*pf%time_bwn_obs) then
!!$        open(77,file='gnuplot_weak.cfg',iostat=ios,action='write'&
!!$             &,status='replace')
!!$        if(ios .ne. 0)  then
!!$           write(*,*) 'Cannot open file gnuplot_weak'
!!$           write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
!!$           stop
!!$        end if
!!$        write(77,'(A)') 'reset'
!!$        write(77,'(A)') 'set terminal wxt size 600,700'
!!$        write(77,'(A)') 'set macros'
!!$        write(77,'(A)') 'set key off'
!!$        write(77,'(A,f0.5,A)') 'set yrange [0:',4.0D0/pf%nens,']'
!!$        write(77,'(A)') 'Str(k)=sprintf("%d",k)'
!!$        write(77,'(A)') 'S=0'
!!$        write(77,'(A,i0)') 'E=S+',pf%nens-1
!!$        write(77,'(A,i0)') 'n=',pf%nens*pf%time_bwn_obs*pf%time_obs
!!$        write(77,'(A)') 'load "animateweak.lorenz"'
!!$        close(77)
!!$        open(77,file='animateweak.lorenz',iostat=ios,action='write'&
!!$             &,status='replace')
!!$        if(ios .ne. 0)  then
!!$           write(*,*) 'Cannot open file animateweak.lorenz'
!!$           write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
!!$           stop
!!$        end if
!!$        write(77,'(A,i0)') 'set multiplot layout 3,1 title "Lorenz 63 usi&
!!$             &ng equal weight particle filter. Timestep = ".S/',pf%nens
!!$        write(77,'(A)') 'myplotoptions=''every ::''.Str(S).''::''.Str(&
!!$             &E)'
!!$        write(77,'(A)') 'set xrange [-20:20]'
!!$        write(77,'(A)') 'set title "x unobserved"'
!!$        write(77,'(A)') 'set xlabel "x"'
!!$        write(77,'(A)') 'plot "pf_weak" using 1:4 @myplotoptions w imp&
!!$             &ulses'
!!$        write(77,'(A)') 'set xrange [-30:30]'
!!$        write(77,'(A)') 'set title "y unobserved"'
!!$        write(77,'(A)') 'set xlabel "y"'
!!$        write(77,'(A)') 'plot "pf_weak" using 2:4 @myplotoptions w imp&
!!$             &ulses'
!!$
!!$        write(77,'(A)') 'set xrange [0:50]'
!!$        write(77,'(A)') 'set title "z observed"'
!!$        write(77,'(A)') 'set xlabel "z"'
!!$        write(77,'(A)') 'plot "pf_weak" using 3:4 @myplotoptions w imp&
!!$             &ulses'
!!$        
!!$        write(77,'(A)') 'unset multiplot'
!!$        
!!$        write(77,'(A,i0)') 'S=S+',pf%nens
!!$        write(77,'(A,i0)') 'E=E+',pf%nens
!!$        write(77,'(A)') 'if (E < n) reread'
!!$        close(77)
!!$     end if
!!$ end if






!if(pfrank .eq. 0) print*,'leaving output_from_pf'
end subroutine output_from_pf



subroutine save_state(state,filename)
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  real(kind=rk), dimension(state_dim), intent(in) :: state
  character(14), intent(in) :: filename
  integer :: ios
  
  open(16,file=filename,iostat=ios,action='write',status='replace',form='unformatted')
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file ',filename
     write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
     stop
  end if
  write(16) state
  close(16)
end subroutine save_state

subroutine get_state(state,filename)
  use sizes
  implicit none
  integer, parameter :: rk = kind(1.0d0)
  real(kind=rk), dimension(state_dim), intent(out) :: state
  character(14), intent(in) :: filename
  integer :: ios
  
  open(16,file=filename,iostat=ios,action='read',status='old',form='unformatted')
  if(ios .ne. 0)  then
     write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file ',filename
     write(*,*) 'Very strange that I couldnt open it. Im going to stop now.'
     stop
  end if
  read(16) state
  close(16)
end subroutine get_state

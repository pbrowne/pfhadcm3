!!! Time-stamp: <2014-10-14 10:34:42 pbrowne>

!program to run the particle filter on the model HadCM3.
!this shall hopefully have minimal changes specific to the model.
!Simon Wilson and Philip Browne 2013
!----------------------------------------------------------------

program create_ens
  use comms
  use pf_control
  use sizes
  use dump_netcdf
  implicit none
  include 'mpif.h'
  integer :: j,k,year,timestep
  integer :: mpi_err,particle,tag
  INTEGER, DIMENSION(:), ALLOCATABLE  :: requests
  INTEGER, DIMENSION(:,:), ALLOCATABLE  :: mpi_statuses
  logical :: mpi_flag
  logical, dimension(:), ALLOCATABLE :: received
  integer :: mpi_status(MPI_STATUS_SIZE)
  real(kind=kind(1.0d0)) :: start_t,end_t
  character(14) :: filename
  character(11) :: filename2

  write(6,'(A)') 'PF: Starting PF code'
  call flush(6)
  call initialise_mpi
  print*,'PF: setting controls'
  call set_pf_controls
  

  pf%gen_Q = .true.
  print*,'PF: configuring model'
  call configure_model
  call allocate_pf

  call load_bathimitry

  print*,'allocated pf%psi = ',allocated(pf%psi)
  print*,'size pf%psi = ',size(pf%psi)

  tag = 1        
  allocate(requests(pf%count))
  allocate(mpi_statuses(mpi_status_size,pf%count))
  allocate(received(pf%count))

  !HADCM3 MODEL SPECIFIC...
  !let us spin the model up for one day, or 72 timesteps
  start_t = mpi_wtime() 
  do j = 1,72
     do k = 1,pf%count
        particle = pf%particles(k)
        call mpi_recv(pf%psi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     end do
     do k = 1,pf%count
        particle = pf%particles(k)
        call mpi_send(pf%psi(:,k), state_dim , MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM, mpi_err)
     end do
  end do
  end_t = mpi_wtime()
  write(6,*) 'Time for initial day run = ',end_t-start_t,' seconds.'
  pf%time=mpi_wtime()


  DO k = 1,pf%count
     particle = pf%particles(k)
     print*,'receiving  from ',particle
     CALL MPI_IRECV(pf%psi(:,k), state_dim, MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM,requests(k), mpi_err)
  end DO
  print*,'receives launched'
  k = 0
  received = .false.
  do
     k = mod(k,pf%count)+1
!     print*,k
     if(.not. received(k)) then
        particle = pf%particles(k)
!        print*,particle ,'not received so testing it'
        call MPI_TEST(requests(k), mpi_flag, mpi_statuses(:,k), mpi_err)
        
        if(mpi_flag) then
!           PRINT*,'Particle filter ',pfrank,'has received initial state_v&
!                &ector over mpi from ensemble member ',particle
           received(k) = .true.
!           if(.not. pf%gen_data) call perturb_particle(pf%psi(:,k))
!           call perturb_particle(pf%psi(:,k))
        end if
     end if
     if(all(received)) exit
  end do
  write(6,*) 'PF: All models received in pf couple' 
  call flush(6)


  start_t = mpi_wtime()

  year = 0
  write(filename,'(A,i2.2,A)') 'start/',year,'.state'
  call save_state(pf%psi(:,1),filename)
  write(filename2,'(A,i2.2,A)') 'start/',year,'.nc'
  call open_netcdf(filename2)
  call put_netcdf(1,pf%psi(:,1))
  call close_netcdf(1)

  do year = 1,32
     do timestep = 1,72*360
        particle = pf%particles(1)
        tag = 1
        call mpi_send(pf%psi(:,1),state_dim,MPI_DOUBLE_PRECISION&
             &,particle-1,tag,CPL_MPI_COMM,mpi_err)
     
        tag = 1
        CALL MPI_RECV(pf%psi(:,1), state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, CPL_MPI_COMM,mpi_status, mpi_err)
     end do
     write(filename,'(A,i2.2,A)') 'start/',year,'.state'
     call save_state(pf%psi(:,1),filename)
     write(filename2,'(A,i2.2,A)') 'start/',year,'.nc'
     call open_netcdf(filename2)
     call put_netcdf(1,pf%psi(:,1))
     call close_netcdf(1)
  end do

  write(6,*) 'PF: finished the loop - now to tidy up'
  end_t = mpi_wtime()
  call flush(6)


  tag = 1        
  DO k = 1,pf%count
     particle = pf%particles(k)
     CALL MPI_ISEND(pf%psi(:,k), state_dim , MPI_DOUBLE_PRECISION, &
          particle-1, tag, CPL_MPI_COMM, requests(k), mpi_err)
     PRINT*,'Particle filter ',pfrank,'has sent final state_vector over mpi &
          &to ensemble member ',particle
  END DO
  CALL MPI_WAITALL(pf%count,requests,mpi_statuses, mpi_err)


  call deallocate_data

  write(6,*) 'PF: finished deallocate_data - off to mpi_finalize'
  call flush(6)


  call MPI_Finalize(mpi_err)
  deallocate(requests)
  deallocate(mpi_statuses)
  deallocate(received)
  write(*,*) 'Program couple_pf terminated successfully.'
  write(*,*) 'Time taken in running the model = ',end_t-start_t
 


end program create_ens



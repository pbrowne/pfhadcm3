!!! Time-stamp: <2014-09-04 17:05:03 pbrowne>

subroutine configure_model
  use pf_control
  use sizes
  use Qdata
  use Rdata
  use hqht_plus_r
  use hadcm3_config
  use histogram_data
  implicit none
  include 'mpif.h'
  
  real(kind=kind(1.0d0)) :: t1
!this is for Lorenz 63
!  state_dim=3
!  obs_dim = 1

  !this is for hadcm3
  state_dim = 2314430
  obs_dim = 27370

  print*,'#################################'
  print*,'######### SANITY CHECK ##########'
  print*,'#################################'
  print*,'## STATE DIMENSION = ',state_dim
  print*,'##  OBS  DIMENSION = ',obs_dim
  print*,'#################################'
  if(.not. pf%gen_Q) then
     !pf%count = 1 !added just for checking
     call load_bathimitry
     call load_histogram_data
     t1 = mpi_wtime()
     call loadQ
     print*,'load Q     took ',mpi_wtime()-t1,' seconds'
     t1 = mpi_wtime()
     call loadR
     print*,'load R     took ',mpi_wtime()-t1,' seconds'
     t1 = mpi_wtime()
     if(.not. pf%gen_data) call calc_HQHTR
     print*,'load HQHTR took ',mpi_wtime()-t1,' seconds'
     !stop !added to just check the calc
  end if
end subroutine configure_model



subroutine solve_r(y,v)
  !subroutine to take an observation vector y and return v
  !in observation space.
  use sizes
  use pf_control
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  real(kind=rk), dimension(obs_dim,pf%count), intent(in) :: y
  real(kind=rk), dimension(obs_dim,pf%count), intent(out) :: v

  !R = diag(0.1)
  !v = y/0.3_rk
  v = 0.0_rk
  call daxpy(obs_dim*pf%count,1.0D0/0.3_rk,y,1,v,1)

  
end subroutine solve_r

subroutine solve_rhalf(y,v)
  !subroutine to take an observation vector y and return v
  !in observation space.
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  real(kind=rk), dimension(obs_dim,pf%count), intent(in) :: y
  real(kind=rk), dimension(obs_dim,pf%count), intent(out) :: v

  !R^1/2 is diag(sqrt(0.1))
!  v = y/sqrt(0.3_rk)
  v = 0.0_rk
  call daxpy(obs_dim*pf%count,1.0D0/sqrt(0.3_rk),y,1,v,1)



end subroutine solve_rhalf


subroutine solve_hqht_plus_r(y,v)
  !subroutine to take an observation vector y and return v
  !in observation space.
  use hsl_ma87_double
  use hqht_plus_r
  use sizes
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  real(kind=rk), dimension(obs_dim), intent(in) :: y
  real(kind=rk), dimension(obs_dim), intent(out) :: v

  !HQHt = (sqrt(2)*0.01)**2 = 0.0002
  !R = 2
  !thus (HQHt + R) = 2.0002

!  v = y/2.0002_rk
  v = y
  call ma87_solve(v,HQHTR_order, HQHTR_keep, HQHTR_control, HQHTR_info)

!!$use sizes
!!$use hqht_plus_r
!!$implicit none
!!$integer, parameter :: rk=kind(1.0D+0)
!!$real(kind=rk), dimension(obs_dim), intent(in) :: y
!!$real(kind=rk), dimension(obs_dim), intent(out) :: v
!!$include 'mpif.h'
!!$real(kind=kind(1.0d0)), dimension(HQHTRnev) :: s
!!$real(kind=kind(1.d00)) :: start
!!$start = mpi_wtime()
!!$call dgemv('T',obs_dim,HQHTRnev,1.0D0,HQHTRU,obs_dim,y,1,0.0D0,s,1)
!!$s = s / HQHTRD
!!$call dgemv('N',obs_dim,HQHTRnev,1.0D0,HQHTRU,obs_dim,s,1,0.0D0,v,1)
!!$print*,'time spent in solve_hqhtr_plus_r = ',mpi_wtime()-start



end subroutine solve_hqht_plus_r

subroutine Q(nrhs,x,Qx)
  !subroutine to take a full state vector x and return Qx
  !in state space.
  use sizes
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs
  real(kind=rk), dimension(state_dim,nrhs), intent(in) :: x
  real(kind=rk), dimension(state_dim,nrhs), intent(out) :: Qx
  real(kind=rk), dimension(state_dim,nrhs) :: temp

!  print*,'sum(x**2) = ',sum(x**2)
  call Qhalf(nrhs,x,temp)
!  print*,'sum(temp**2) = ',sum(temp**2)
  call QhalfT(nrhs,temp,Qx)
!  print*,'sum(qx**2) = ',sum(qx**2)

end subroutine Q

 
!!$subroutine Qhalf(nrhs,x,Qx)
!!$  !subroutine to take a full state vector x and return Q^{1/2}x
!!$  !in state space.
!!$  use sizes
!!$  use Qdata
!!$  implicit none
!!$  integer, parameter :: rk=kind(1.0D+0)
!!$  integer, intent(in) :: nrhs
!!$  real(kind=rk), dimension(state_dim,nrhs), intent(in) :: x
!!$  real(kind=rk), dimension(state_dim,nrhs), intent(out) :: qx
!!$
!!$!  call coord_matmul_t(state_dim,Qne,Qrow,Qcol,Qval,x,qx)
!!$  call openmp_matmul_t(state_dim,Qne,Qrow,Qcol,Qval,Qdiag,nrhs,x,qx)
!!$
!!$  
!!$end subroutine Qhalf

subroutine Qhalf(nrhs,x,Qx)
  !subroutine to take a full state vector x and return Q^{1/2}x
  !in state space.

  !this will be to compute (correlation matrix)diag(scaled variances)x

  use sizes
  use Qdata
!  use blas_sparse
!  use rsb
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs
  integer :: i,istat
  real(kind=rk), dimension(state_dim,nrhs), intent(in) :: x
  real(kind=rk), dimension(state_dim,nrhs) :: y
  real(kind=rk), dimension(state_dim,nrhs), intent(out) :: qx
!  real(kind=rk), dimension(state_dim,nrhs) :: temp
  real(kind=rk) :: alpha

!  do i = 1,100
!     print*,'Qdiag(',i,') = ',Qdiag(i)
!  end do

!  print*,'allocated Qdiag = ',allocated(Qdiag)
!  print*,'maxval Qdiag = ',maxval(Qdiag)
!  print*,'minval Qdiag = ',minval(Qdiag)

!  print*,'sum(Qdiag**2) = ',sum(Qdiag**2)
!  print*,'sum(x**2) =     ',sum(x**2)

  do i = 1,nrhs
     y(:,i) = x(:,i)*Qdiag
  end do

!  print*,'sum(y**2) = ',sum(y**2)
  
  qx = 0.0_rk
  alpha = 1.0_rk
!  call openmp_matmul_t(state_dim,Qne,Qrow,Qcol,Qval,Qdiag,nrhs,x,qx)
  do i = 1,nrhs
     CALL dusmv(blas_no_trans,alpha,Qrsb,y(:,i),1,qx(:,i),1,istat)
!     print*,i,Qrsb,istat
  end do
!  call openmp_matmul_t(state_dim,Qne,Qrow,Qcol,Qval,Qdiag,nrhs,x,temp)
!  do i = 1,nrhs
!     print*,i,sqrt(sum((qx(:,i)-temp(:,i))**2))
!  end do
!  print*,'qhalf over'

  
end subroutine Qhalf

subroutine QhalfT(nrhs,x,Qx)
  !subroutine to take a full state vector x and return Q^{T/2}x
  !in state space.

  !this will be to compute diag(scaled variances)(correlation matrix)x

  use sizes
  use Qdata
!  use blas_sparse
!  use rsb
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs
  integer :: i,istat
  real(kind=rk), dimension(state_dim,nrhs), intent(in) :: x
  real(kind=rk), dimension(state_dim,nrhs), intent(out) :: qx
!  real(kind=rk), dimension(state_dim,nrhs) :: temp
  real(kind=rk) :: alpha

  qx = 0.0_rk
  alpha = 1.0_rk
!  call openmp_matmul_t(state_dim,Qne,Qrow,Qcol,Qval,Qdiag,nrhs,x,qx)
  do i = 1,nrhs
     CALL dusmv(blas_no_trans,alpha,Qrsb,x(:,i),1,qx(:,i),1,istat)
!     print*,i,Qrsb,istat
  end do

  do i = 1,nrhs
     qx(:,i) = qx(:,i)*Qdiag
  end do

  
end subroutine QhalfT





subroutine R(nrhs,y,Ry)
  !subroutine to take an observation vector x and return Rx
  !in observation space.
  use sizes
  use Rdata
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs
  real(kind=rk), dimension(obs_dim,nrhs), intent(in) :: y
  real(kind=rk), dimension(obs_dim,nrhs), intent(out) :: Ry

!  call openmp_matmul_t(obs_dim,Rne,Rrow,Rcol,Rval,Rdiag,y,Ry)
!  call openmp_matmul_t(obs_dim,Rne,Rrow,Rcol,Rval,Rdiag,nrhs,y,Ry)
  Ry = Rdiag(1)*y


end subroutine R

subroutine Rhalf(nrhs,y,Ry)
  use sizes
  use Rdata
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer, intent(in) :: nrhs
  real(kind=rk), dimension(obs_dim,nrhs), intent(in) :: y
  real(kind=rk), dimension(obs_dim,nrhs), intent(out) :: Ry
  
  Ry = sqrt(Rdiag(1))*y

end subroutine RHALF


subroutine H(x,hx)
  !subroutine to take a full state vector x and return H(x)
  !in observation space.
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  real(kind=rk), dimension(state_dim,pf%count), intent(in) :: x
  real(kind=rk), dimension(obs_dim,pf%count), intent(out) :: hx

!  include 'H_output.f90'
  hx(:,:) = x(539617:566986,:)
  !now convert from celcius to Kelvin
!  hx = hx + 273.15_rk
end subroutine H

subroutine HT(y,x)
  !subroutine to take an observation vector y and return x = H^T(y)
  !in full state space.
  use pf_control
  use sizes
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  real(kind=rk), dimension(state_dim,pf%count), intent(out) :: x
  real(kind=rk), dimension(obs_dim,pf%count), intent(in) :: y

!  x=273.15_rk
  x = 0.0_rk
  x(539617:566986,:) = y(:,:)
!  include 'HT_output.f90'
  !convert from Kelvin to celcius
!  x = x - 273.15_rk
end subroutine HT


!!$subroutine coord_matmul_t(n,ne,row,col,val,x,b)
!!$  integer, parameter :: rk = kind(1.0D+0)
!!$  integer, intent(in) :: n,ne
!!$  integer, intent(in), dimension(ne) :: row,col
!!$  real(kind = rk), dimension(ne), intent(in) :: val
!!$  real(kind = rk), dimension(n), intent(in) :: x
!!$  real(kind = rk), dimension(n), intent(out) :: b
!!$  integer :: k
!!$  b = 0.0_rk
!!$  !$omp parallel do
!!$  do k = 1,ne
!!$     if(row(k) .eq. col(k)) then
!!$!        !$OMP ATOMIC
!!$        b(row(k)) = b(row(k)) + val(k)*x(col(k))
!!$     else
!!$!        !$OMP ATOMIC
!!$        b(row(k)) = b(row(k)) + val(k)*x(col(k))
!!$!        !$OMP ATOMIC
!!$        b(col(k)) = b(col(k)) + val(k)*x(row(k))
!!$     end if
!!$  end do
!!$  !$omp end parallel do
!!$end subroutine coord_matmul_t

subroutine openmp_matmul_t(n,ne,row,col,val,diag,nrhs,x,b)
use omp_lib
integer, parameter :: rk = kind(1.0D+0)
integer, intent(in) :: n,ne,nrhs
integer, intent(in), dimension(ne) :: row,col
real(kind = rk), dimension(ne), intent(in) :: val
real(kind = rk), dimension(n), intent(in) :: diag
real(kind = rk), dimension(n,nrhs), intent(in) :: x
real(kind = rk), dimension(n,nrhs), intent(out) :: b
real(kind = rk), allocatable, dimension(:,:,:) :: temp
integer :: k,i,num_thr,thr,iam
include 'mpif.h'
real(kind=rk) :: t
real(kind=rk), dimension(5) :: ti
logical, parameter :: time = .false.

!print*,'in openmp'
!print*,'n = ',n
!print*,'ne = ',ne
!print*,'nrhs = ',nrhs
if(time) t = mpi_wtime()
b = 0.0_rk
if(time) ti(1) = mpi_wtime()
!first do the diagonal part
!$omp parallel do
do k = 1,n
   b(k,:) = b(k,:) + diag(k)*x(k,:)
end do
!$omp end parallel do
if(time) ti(2) = mpi_wtime()
!print*,'finished computing b'


!now do the off diagonal terms:
num_thr = omp_get_max_threads()
!print*,'num_thr = ',num_thr
allocate(temp(n,nrhs,num_thr))
temp = 0.0D0
if(time) ti(3) = mpi_wtime()
!print*,'before paralelel'
!$omp parallel private(iam)
iam = omp_get_thread_num()+1
!$omp do
do i = 1, ne
!   temp(row(i),:,omp_get_thread_num()+1) = temp(row(i),:&
!        &,omp_get_thread_num()+1) + val(i)*x(col(i),:)
!   temp(col(i),:,omp_get_thread_num()+1) = temp(col(i),:,omp_get_thread_num()&
!        &+1) + val(i)*x(row(i),:)

   temp(row(i),:,iam) = temp(row(i),:,iam) + val(i)*x(col(i),:)
   temp(col(i),:,iam) = temp(col(i),:,iam) + val(i)*x(row(i),:)

end do
!$omp end do
!$omp end parallel
if(time) ti(4) = mpi_wtime()

!print*,'before reduction'

!$omp parallel do private(thr)
do i = 1, n
   do thr = 1, num_thr
      b(i,:) = b(i,:) + temp(i,:,thr)
   end do
end do
!$omp end parallel do 
deallocate(temp)
if(time) then
   ti(5) = mpi_wtime()
   ti(5) = ti(5)-ti(4)
   ti(4) = ti(4)-ti(3)
   ti(3) = ti(3)-ti(2)
   ti(2) = ti(2)-ti(1)
   ti(1) = ti(1)-t
   write(6,'(4(es7.1,x),es7.1)') ti(1),ti(2),ti(3),ti(4),ti(5)
   print*,'###########'
end if
!print*,'finished ompenmp_matmul_y'
end subroutine openmp_matmul_t

subroutine C(x,Qx)
  !subroutine to take a full state vector x and multiply it by the
  ! correlation matrix.



  use sizes
  use Qdata
!  use blas_sparse
!  use rsb
  implicit none
  integer, parameter :: rk=kind(1.0D+0)
  integer :: istat
  real(kind=rk), dimension(state_dim), intent(in) :: x
  real(kind=rk), dimension(state_dim) :: y
  real(kind=rk), dimension(state_dim), intent(out) :: qx
!  real(kind=rk), dimension(state_dim,nrhs) :: temp
  real(kind=rk) :: alpha

  y= x
  
  qx = 0.0_rk
  alpha = 1.0_rk
  CALL dusmv(blas_no_trans,alpha,Qrsb,y,1,qx,1,istat)

  qx = qx/14200509040.039228_rk
  
end subroutine C

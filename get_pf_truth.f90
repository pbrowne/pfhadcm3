program get_pf_truth
implicit none
integer, parameter :: rk = kind(1.0d0)
integer, parameter :: state_dim = 2314430
real(kind=rk), dimension(state_dim) :: x
integer :: ios,length,rec,time,num
read*,time
read*,num
if(num .lt. 1 .or. num .gt. state_dim) then
   stop 'num outside allowable bounds'
end if
inquire(iolength=length) x
open(62,file='pf_truth',iostat=ios,action='read',form='unformatted'&
     &,access='direct',recl=length)
if(ios .ne. 0)  then
   write(*,*) 'PARTICLE FILTER DATA ERROR!!!!! Cannot open file pf_t&
        &ruth to read'
   write(*,*) 'Very strange that I couldnt open it. Im going to stop&
        & now.'
   stop
end if
rec = time + 1
read(62,rec=rec) x
close(62)
write(6,*) x(num)
end program get_pf_truth

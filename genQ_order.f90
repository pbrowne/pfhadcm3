!!! Time-stamp: <2014-10-30 16:46:54 pbrowne>

  ne = 0
  call genQ_at2d_at2d(ne,row,col,val,n,radius,a_pstar_vec,a_pstar_vec,x)
  !print*,'a_pstar a_pstar',ne
  call genQ_at2d_atu(ne,row,col,val,n,radius,a_pstar_vec,a_u_vec    ,x)
  !print*,'a_pstar a_u',ne
  call genQ_at2d_atu(ne,row,col,val,n,radius,a_pstar_vec,a_v_vec    ,x)
  !print*,'a_pstar a_v',ne
  call genQ_at2d_atq(ne,row,col,val,n,radius,a_pstar_vec,a_theta_vec,x)
  !print*,'a_pstar a_theta',ne
  call genQ_at2d_atq(ne,row,col,val,n,radius,a_pstar_vec,a_q_vec    ,x)
  !print*,'a_pstar a_q',ne

  call genQ_at2d_ocq(ne,row,col,val,n,radius,a_pstar_vec,o_theta_vec,x)
  call genQ_at2d_ocq(ne,row,col,val,n,radius,a_pstar_vec,o_sal_vec  ,x)
  call genQ_at2d_ocu(ne,row,col,val,n,radius,a_pstar_vec,o_u_vec    ,x)
  call genQ_at2d_ocu(ne,row,col,val,n,radius,a_pstar_vec,o_v_vec    ,x)

  call genQ_atu_atu(ne,row,col,val,n,radius,a_u_vec    ,a_u_vec    ,x)
  !print*,'a_u a_u',ne
  call genQ_atu_atu(ne,row,col,val,n,radius,a_u_vec    ,a_v_vec    ,x)
  !print*,'a_u a_v',ne
  call genQ_atu_atq(ne,row,col,val,n,radius,a_u_vec    ,a_theta_vec,x)
  !print*,'a_u a_theta',ne
  call genQ_atu_atq(ne,row,col,val,n,radius,a_u_vec    ,a_q_vec    ,x)

  call genQ_atu_ocq(ne,row,col,val,n,radius,a_u_vec    ,o_theta_vec,x)
  call genQ_atu_ocq(ne,row,col,val,n,radius,a_u_vec    ,o_sal_vec  ,x)
  call genQ_atu_ocu(ne,row,col,val,n,radius,a_u_vec    ,o_u_vec    ,x)
  call genQ_atu_ocu(ne,row,col,val,n,radius,a_u_vec    ,o_v_vec    ,x)

  !print*,'a_u a_q',ne
  call genQ_atu_atu(ne,row,col,val,n,radius,a_v_vec    ,a_v_vec    ,x)
  !print*,'a_v a_v',ne
  call genQ_atu_atq(ne,row,col,val,n,radius,a_v_vec    ,a_theta_vec,x)
  !print*,'a_v a_theta',ne
  call genQ_atu_atq(ne,row,col,val,n,radius,a_v_vec    ,a_q_vec    ,x)
  !print*,'a_v a_q',ne

  call genQ_atu_ocq(ne,row,col,val,n,radius,a_v_vec    ,o_theta_vec,x)
  call genQ_atu_ocq(ne,row,col,val,n,radius,a_v_vec    ,o_sal_vec  ,x)
  call genQ_atu_ocu(ne,row,col,val,n,radius,a_v_vec    ,o_u_vec    ,x)
  call genQ_atu_ocu(ne,row,col,val,n,radius,a_v_vec    ,o_v_vec    ,x)

  call genQ_atq_atq(ne,row,col,val,n,radius,a_theta_vec,a_theta_vec,x)
  !print*,'a_theta a_theta',ne
  call genQ_atq_atq(ne,row,col,val,n,radius,a_theta_vec,a_q_vec    ,x)
  !print*,'a_theta a_q',ne

  call genQ_atq_ocq(ne,row,col,val,n,radius,a_theta_vec,o_theta_vec,x)
  call genQ_atq_ocq(ne,row,col,val,n,radius,a_theta_vec,o_sal_vec  ,x)
  call genQ_atq_ocu(ne,row,col,val,n,radius,a_theta_vec,o_u_vec    ,x)
  call genQ_atq_ocu(ne,row,col,val,n,radius,a_theta_vec,o_v_vec    ,x)

  call genQ_atq_atq(ne,row,col,val,n,radius,a_q_vec    ,a_q_vec    ,x)
  !print*,'a_q a_q',ne

  call genQ_atq_ocq(ne,row,col,val,n,radius,a_q_vec    ,o_theta_vec,x)
  call genQ_atq_ocq(ne,row,col,val,n,radius,a_q_vec    ,o_sal_vec  ,x)
  call genQ_atq_ocu(ne,row,col,val,n,radius,a_q_vec    ,o_u_vec    ,x)
  call genQ_atq_ocu(ne,row,col,val,n,radius,a_q_vec    ,o_v_vec    ,x)

  call genQ_ocq_ocq(ne,row,col,val,n,radius,o_theta_vec,o_theta_vec,x)
  !print*,'o_theta o_theta',ne
  call genQ_ocq_ocq(ne,row,col,val,n,radius,o_theta_vec,o_sal_vec  ,x)
  !print*,'o_theta o_sal',ne
  call genQ_ocq_ocu(ne,row,col,val,n,radius,o_theta_vec,o_u_vec    ,x)
  !print*,'o_theta o_u',ne
  call genQ_ocq_ocu(ne,row,col,val,n,radius,o_theta_vec,o_v_vec    ,x)
  !print*,'o_theta o_v',ne
  call genQ_ocq_ocq(ne,row,col,val,n,radius,o_sal_vec  ,o_sal_vec  ,x)
  !print*,'o_sal o_sal',ne
  call genQ_ocq_ocu(ne,row,col,val,n,radius,o_sal_vec  ,o_u_vec    ,x)
  !print*,'o_sal o_u',ne
  call genQ_ocq_ocu(ne,row,col,val,n,radius,o_sal_vec  ,o_v_vec    ,x)
  !print*,'o_sal o_v',ne
  call genQ_ocu_ocu(ne,row,col,val,n,radius,o_u_vec    ,o_u_vec    ,x)
  !print*,'o_u o_u',ne
  call genQ_ocu_ocu(ne,row,col,val,n,radius,o_u_vec    ,o_v_vec    ,x)
  !print*,'o_u o_v',ne
  call genQ_ocu_ocu(ne,row,col,val,n,radius,o_v_vec    ,o_v_vec    ,x)
  !print*,'o_v o_v',ne

